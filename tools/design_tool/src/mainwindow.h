/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QLineEdit>

#include "sampling_strategies/samplingStrategy.h"
#include "emitters/emitter.h"
#include "geometries/geometry.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    void on_actionCircle_triggered();

    void on_actionLine_triggered();

    void on_actionSquare_triggered();

    void on_nextButton_clicked();

    void on_shapeComboBox_currentIndexChanged(const QString &arg1);

    void on_actionClear_triggered();

    void on_previousButton_clicked();

    void on_playButton_clicked();

    void on_nbPtsLineEdit_editingFinished();

    void on_actionEllipse_triggered();

    void on_actionRectangle_triggered();

    void on_actionTriangle_triggered();

    void on_lineEditDynParam1Shape_editingFinished();

    void on_lineEditDynParam2Shape_editingFinished();

    void on_emitStartPushButton_clicked();

    void on_emitStopPushButton_clicked();

    void on_xCenterLineEdit_editingFinished();

    void on_yCenterLineEdit_editingFinished();

    void on_zCenterLineEdit_editingFinished();

    void on_samplingComboBox_currentIndexChanged(const QString &arg1);

    void on_actionStep_sampling_triggered();

    void on_lineEditDynParam1Sampling_editingFinished();

    void on_plainTextEditDynParam1Sampling_textChanged();

    void on_actionCustom_step_triggered();

    void on_actionSave_sampling_strategy_triggered();

    void on_actionLoad_sampling_strategy_triggered();

    void on_actionRandom_sampling_triggered();

    void on_dynSamplingButton_clicked();

    void on_actionSave_geometry_triggered();

    void on_actionLoad_geometry_triggered();

    void on_actionSave_both_triggered();

    void on_actionLaod_both_triggered();

    void on_actionPointList_triggered();

    void on_plainTextEditDynParam1Shape_textChanged();

    void on_actionArc_triggered();

    void on_actionCustom_Time_Step_Sampling_triggered();

    void on_lineEditDynParam3Sampling_editingFinished();

    void on_lineEditSampleRate_editingFinished();

private:
    // *** Fields *** //

    Ui::MainWindow *ui;

    // Emitter
//    Ultrahaptics::TimePointStreaming::Emitter emitter = Ultrahaptics::TimePointStreaming::Emitter("MockDevice:USX;logfile=logTestEmiting.txt");
//    Ultrahaptics::TimePointStreaming::Emitter emitter = Ultrahaptics::TimePointStreaming::Emitter();
//    UserData* user_data;

    Emitter* emitter;
    bool is_emitting = false;
    int sampleRate = 40000;

    bool is_playing = false;

    // Geometry
    Geometries::Geometry* currentShape = nullptr;
    QString geometryName = "...";                                           // To keep track of what kinf of geometry is currently used
    Ultrahaptics::Vector3 center = Ultrahaptics::Vector3(0.f, 0.f, 20.f);   // Center of the shape
    double dynParam1Shape, dynParam2Shape;                  // Will contain the values in the dynamic widgets
    QString dynParamStringShape;                            // Will contain the values in the PlainTextEdit dynamic widget

    // Sampling strategy
    SamplingStrategy* samplingStrategy = nullptr;
    QString samplingName = "Step sampling";     // To keep track of what kinf of geometry is currently used
    int dynParam1Sampling;                     // Will contain the values in the LineEdit dynamic widgets
    double dynParam2Sampling;
    QString dynParamStringSampling;             // Will contain the values in the PlainTextEdit dynamic widgets

    // Play animation
    QTimer* timer = new QTimer();   // For the 'play' animation
    int playIndex = 0;              // For the 'play' animation (useful if user uses next/previous during the animation)

    // Utils
    std::pair<QVector<double>, QVector<double>> modelIntensity; // Values of pression per distance


    // *** Methods *** //


    // Plots
    void plotGeometry();
    void plotIntensities();

    // Dynamic widgets about the shape parametrization
    void setLineEditDynParamShape();                                            // Remove all dynamic widgets for the shape parametrization
    void setLineEditDynParamShape(QString s1, double d1);                       // Set the first dynamic widget + label
    void setLineEditDynParamShape(QString s1, QString s2, double d1, double d2);// Set the first two dynamic widgets + labels
    void setBaseLineEditDynParamShape();                                        // Set the base parameters in the dynamic widgets/labels for the current shape
    void updateAfterGeometryLoad();                                             // Set the widgets after loading a geometry

    // Dynamic widgets about the strategy parametrization
    void setLineEditDynParamSampling();                     // Remove all dynamic widgets for the strategy parametrization
    void setLineEditDynParamSampling(QString s1, int i1);   // Set the first (lineEdit) dynamic widget
    void setLineEditDynParamSampling(QString s1, QString s2, int val1, double val2);        // Set the first two (lineEdit) dynamic widgets
    void setPlainTextEditDynParamSampling(QString s1, QString value, QString tooltip); // Set the first PlainTextEdit widget
    void setBaseLineEditDynParamSampling();                 // Set the base parameters in the dynamic widgets/labels for the current strategy
    void updateAfterSamplingLoad();                         // Set the widgets/labels after loading a strategy

    // Update the backend shape/strategy when something has changed
    void updateGeoMetry();          // To set the dynamic parameters and change geometry
    void updateSampling();          // To set the dynamic parameters and change sampling strategy

    // Play animation
    void playNextStep();            // Next step in the 'play' animation
    void resetTimer();

    // Intensities
    static std::pair<QVector<double>, QVector<double>> recoverIntensitiesModel(std::string path);   // Revover the pression vector from the CSV model
    double computeIntensity(Ultrahaptics::Vector3 reciever, Ultrahaptics::Vector3 emitter);         // Computes the intensity at one point when the other emits according to the model
    QVector<double> computeIntensities(Ultrahaptics::Vector3 reciever);                             // Computes the intensity at every point when one emits

    // Conversions
    std::pair<bool, int> lineEdit2Int(QLineEdit* widget);
    std::pair<bool, double> lineEdit2Double(QLineEdit* widget);

    void reloadEmitter();
    void updateInfoLabels();
};
#endif // MAINWINDOW_H
