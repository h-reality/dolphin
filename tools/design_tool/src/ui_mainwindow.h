/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionCircle;
    QAction *actionSquare;
    QAction *actionLine;
    QAction *actionClear;
    QAction *actionTriangle;
    QAction *actionEllipse;
    QAction *actionRectangle;
    QAction *actionStep_sampling;
    QAction *actionCustom_step;
    QAction *actionSave_sampling_strategy;
    QAction *actionLoad_sampling_strategy;
    QAction *actionRandom_sampling;
    QAction *actionSave_geometry;
    QAction *actionLoad_geometry;
    QAction *actionSave_both;
    QAction *actionLaod_both;
    QAction *actionPointList;
    QAction *actionArc;
    QAction *actionCustom_Time_Step_Sampling;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QPushButton *playButton;
    QCustomPlot *customPlotIntensity;
    QPushButton *nextButton;
    QSpacerItem *horizontalSpacer;
    QLineEdit *xCenterLineEdit;
    QLabel *labelShape;
    QLineEdit *lineEditDynParam1Shape;
    QLabel *centerXLabel;
    QCustomPlot *customPlotIntensity_2;
    QLineEdit *lineEditSampleRate;
    QPushButton *dynSamplingButton;
    QLabel *labelDynParam3Shape;
    QLabel *centerYLabel;
    QLineEdit *zCenterLineEdit;
    QLabel *labelShape_2;
    QPlainTextEdit *plainTextEditDynParam1Sampling;
    QLineEdit *lineEditDynParam1Sampling;
    QLineEdit *yCenterLineEdit;
    QLabel *labelDynParam1Sampling;
    QLabel *labelSampleRate;
    QLineEdit *lineEditDynParam3Sampling;
    QLabel *labelDynParam2Shape;
    QLabel *labelDynParam3Sampling;
    QComboBox *shapeComboBox;
    QLabel *nbPtsLabel;
    QLabel *labelGain;
    QLabel *labelPtFrequency;
    QLineEdit *lineEditGain;
    QComboBox *samplingComboBox;
    QLabel *labelDynParam1Shape;
    QLabel *labelDynParam2Sampling;
    QPushButton *previousButton;
    QCustomPlot *customPlot;
    QLabel *labelSpatialStep;
    QLineEdit *lineEditDynParam2Shape;
    QLabel *centerZLabel;
    QLabel *labelTimeStep;
    QLineEdit *nbPtsLineEdit;
    QPlainTextEdit *plainTextEditDynParam1Shape;
    QLabel *labelFPSpeed;
    QPushButton *emitStopPushButton;
    QPushButton *emitStartPushButton;
    QMenuBar *menubar;
    QMenu *menuFile;
    QStatusBar *statusbar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1412, 690);
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(255, 255, 255, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette.setBrush(QPalette::Active, QPalette::Light, brush1);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush1);
        QBrush brush2(QColor(127, 127, 127, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush2);
        QBrush brush3(QColor(170, 170, 170, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush3);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush1);
        QBrush brush4(QColor(255, 255, 220, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush4);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        QBrush brush5(QColor(0, 0, 0, 128));
        brush5.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush5);
#endif
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush5);
#endif
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush5);
#endif
        MainWindow->setPalette(palette);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        actionCircle = new QAction(MainWindow);
        actionCircle->setObjectName(QString::fromUtf8("actionCircle"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/icons/circle_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCircle->setIcon(icon);
        actionSquare = new QAction(MainWindow);
        actionSquare->setObjectName(QString::fromUtf8("actionSquare"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/icons/square_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSquare->setIcon(icon1);
        actionLine = new QAction(MainWindow);
        actionLine->setObjectName(QString::fromUtf8("actionLine"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/icons/line_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLine->setIcon(icon2);
        actionClear = new QAction(MainWindow);
        actionClear->setObjectName(QString::fromUtf8("actionClear"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/icons/clear_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionClear->setIcon(icon3);
        actionTriangle = new QAction(MainWindow);
        actionTriangle->setObjectName(QString::fromUtf8("actionTriangle"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/icons/Triangle_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionTriangle->setIcon(icon4);
        actionEllipse = new QAction(MainWindow);
        actionEllipse->setObjectName(QString::fromUtf8("actionEllipse"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/icons/ellipse_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEllipse->setIcon(icon5);
        actionRectangle = new QAction(MainWindow);
        actionRectangle->setObjectName(QString::fromUtf8("actionRectangle"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/icons/rectangle_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionRectangle->setIcon(icon6);
        actionStep_sampling = new QAction(MainWindow);
        actionStep_sampling->setObjectName(QString::fromUtf8("actionStep_sampling"));
        actionCustom_step = new QAction(MainWindow);
        actionCustom_step->setObjectName(QString::fromUtf8("actionCustom_step"));
        actionSave_sampling_strategy = new QAction(MainWindow);
        actionSave_sampling_strategy->setObjectName(QString::fromUtf8("actionSave_sampling_strategy"));
        actionLoad_sampling_strategy = new QAction(MainWindow);
        actionLoad_sampling_strategy->setObjectName(QString::fromUtf8("actionLoad_sampling_strategy"));
        actionRandom_sampling = new QAction(MainWindow);
        actionRandom_sampling->setObjectName(QString::fromUtf8("actionRandom_sampling"));
        actionSave_geometry = new QAction(MainWindow);
        actionSave_geometry->setObjectName(QString::fromUtf8("actionSave_geometry"));
        actionLoad_geometry = new QAction(MainWindow);
        actionLoad_geometry->setObjectName(QString::fromUtf8("actionLoad_geometry"));
        actionSave_both = new QAction(MainWindow);
        actionSave_both->setObjectName(QString::fromUtf8("actionSave_both"));
        actionLaod_both = new QAction(MainWindow);
        actionLaod_both->setObjectName(QString::fromUtf8("actionLaod_both"));
        actionPointList = new QAction(MainWindow);
        actionPointList->setObjectName(QString::fromUtf8("actionPointList"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/icons/point_list.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPointList->setIcon(icon7);
        actionArc = new QAction(MainWindow);
        actionArc->setObjectName(QString::fromUtf8("actionArc"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/images/icons/Arc.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionArc->setIcon(icon8);
        actionCustom_Time_Step_Sampling = new QAction(MainWindow);
        actionCustom_Time_Step_Sampling->setObjectName(QString::fromUtf8("actionCustom_Time_Step_Sampling"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        centralwidget->setAutoFillBackground(false);
        centralwidget->setStyleSheet(QString::fromUtf8(""));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        playButton = new QPushButton(centralwidget);
        playButton->setObjectName(QString::fromUtf8("playButton"));

        gridLayout->addWidget(playButton, 4, 6, 1, 1);

        customPlotIntensity = new QCustomPlot(centralwidget);
        customPlotIntensity->setObjectName(QString::fromUtf8("customPlotIntensity"));

        gridLayout->addWidget(customPlotIntensity, 1, 0, 1, 5);

        nextButton = new QPushButton(centralwidget);
        nextButton->setObjectName(QString::fromUtf8("nextButton"));

        gridLayout->addWidget(nextButton, 5, 6, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 4, 12, 1, 1);

        xCenterLineEdit = new QLineEdit(centralwidget);
        xCenterLineEdit->setObjectName(QString::fromUtf8("xCenterLineEdit"));
        xCenterLineEdit->setMaximumSize(QSize(75, 16777215));

        gridLayout->addWidget(xCenterLineEdit, 4, 3, 1, 1);

        labelShape = new QLabel(centralwidget);
        labelShape->setObjectName(QString::fromUtf8("labelShape"));
        labelShape->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelShape, 4, 0, 1, 1);

        lineEditDynParam1Shape = new QLineEdit(centralwidget);
        lineEditDynParam1Shape->setObjectName(QString::fromUtf8("lineEditDynParam1Shape"));
        lineEditDynParam1Shape->setMaximumSize(QSize(75, 16777215));

        gridLayout->addWidget(lineEditDynParam1Shape, 4, 5, 1, 1);

        centerXLabel = new QLabel(centralwidget);
        centerXLabel->setObjectName(QString::fromUtf8("centerXLabel"));
        centerXLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(centerXLabel, 4, 2, 1, 1);

        customPlotIntensity_2 = new QCustomPlot(centralwidget);
        customPlotIntensity_2->setObjectName(QString::fromUtf8("customPlotIntensity_2"));

        gridLayout->addWidget(customPlotIntensity_2, 0, 0, 1, 5);

        lineEditSampleRate = new QLineEdit(centralwidget);
        lineEditSampleRate->setObjectName(QString::fromUtf8("lineEditSampleRate"));
        lineEditSampleRate->setMaximumSize(QSize(75, 16777215));

        gridLayout->addWidget(lineEditSampleRate, 2, 3, 1, 1);

        dynSamplingButton = new QPushButton(centralwidget);
        dynSamplingButton->setObjectName(QString::fromUtf8("dynSamplingButton"));

        gridLayout->addWidget(dynSamplingButton, 3, 9, 1, 1);

        labelDynParam3Shape = new QLabel(centralwidget);
        labelDynParam3Shape->setObjectName(QString::fromUtf8("labelDynParam3Shape"));
        labelDynParam3Shape->setMinimumSize(QSize(80, 0));
        labelDynParam3Shape->setMaximumSize(QSize(100, 16777215));
        labelDynParam3Shape->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelDynParam3Shape, 6, 4, 1, 1);

        centerYLabel = new QLabel(centralwidget);
        centerYLabel->setObjectName(QString::fromUtf8("centerYLabel"));
        centerYLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(centerYLabel, 5, 2, 1, 1);

        zCenterLineEdit = new QLineEdit(centralwidget);
        zCenterLineEdit->setObjectName(QString::fromUtf8("zCenterLineEdit"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(zCenterLineEdit->sizePolicy().hasHeightForWidth());
        zCenterLineEdit->setSizePolicy(sizePolicy);
        zCenterLineEdit->setMaximumSize(QSize(75, 16777215));
        zCenterLineEdit->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(zCenterLineEdit, 6, 3, 1, 1, Qt::AlignTop);

        labelShape_2 = new QLabel(centralwidget);
        labelShape_2->setObjectName(QString::fromUtf8("labelShape_2"));
        labelShape_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelShape_2, 4, 8, 1, 1);

        plainTextEditDynParam1Sampling = new QPlainTextEdit(centralwidget);
        plainTextEditDynParam1Sampling->setObjectName(QString::fromUtf8("plainTextEditDynParam1Sampling"));
        plainTextEditDynParam1Sampling->setMaximumSize(QSize(75, 50));

        gridLayout->addWidget(plainTextEditDynParam1Sampling, 5, 11, 2, 1, Qt::AlignTop);

        lineEditDynParam1Sampling = new QLineEdit(centralwidget);
        lineEditDynParam1Sampling->setObjectName(QString::fromUtf8("lineEditDynParam1Sampling"));
        lineEditDynParam1Sampling->setMaximumSize(QSize(75, 16777215));

        gridLayout->addWidget(lineEditDynParam1Sampling, 4, 11, 1, 1);

        yCenterLineEdit = new QLineEdit(centralwidget);
        yCenterLineEdit->setObjectName(QString::fromUtf8("yCenterLineEdit"));
        yCenterLineEdit->setMaximumSize(QSize(75, 16777215));

        gridLayout->addWidget(yCenterLineEdit, 5, 3, 1, 1);

        labelDynParam1Sampling = new QLabel(centralwidget);
        labelDynParam1Sampling->setObjectName(QString::fromUtf8("labelDynParam1Sampling"));
        labelDynParam1Sampling->setMinimumSize(QSize(80, 0));
        labelDynParam1Sampling->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelDynParam1Sampling, 4, 10, 1, 1);

        labelSampleRate = new QLabel(centralwidget);
        labelSampleRate->setObjectName(QString::fromUtf8("labelSampleRate"));

        gridLayout->addWidget(labelSampleRate, 2, 2, 1, 1);

        lineEditDynParam3Sampling = new QLineEdit(centralwidget);
        lineEditDynParam3Sampling->setObjectName(QString::fromUtf8("lineEditDynParam3Sampling"));
        lineEditDynParam3Sampling->setMaximumSize(QSize(75, 16777215));

        gridLayout->addWidget(lineEditDynParam3Sampling, 3, 11, 1, 1);

        labelDynParam2Shape = new QLabel(centralwidget);
        labelDynParam2Shape->setObjectName(QString::fromUtf8("labelDynParam2Shape"));
        labelDynParam2Shape->setMinimumSize(QSize(80, 0));
        labelDynParam2Shape->setMaximumSize(QSize(100, 16777215));
        labelDynParam2Shape->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelDynParam2Shape, 5, 4, 1, 1);

        labelDynParam3Sampling = new QLabel(centralwidget);
        labelDynParam3Sampling->setObjectName(QString::fromUtf8("labelDynParam3Sampling"));
        labelDynParam3Sampling->setMinimumSize(QSize(80, 0));
        labelDynParam3Sampling->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelDynParam3Sampling, 3, 10, 1, 1);

        shapeComboBox = new QComboBox(centralwidget);
        shapeComboBox->addItem(icon3, QString());
        shapeComboBox->addItem(icon, QString());
        shapeComboBox->addItem(icon5, QString());
        shapeComboBox->addItem(icon1, QString());
        shapeComboBox->addItem(icon6, QString());
        shapeComboBox->addItem(icon4, QString());
        shapeComboBox->addItem(icon2, QString());
        shapeComboBox->addItem(icon7, QString());
        shapeComboBox->addItem(icon8, QString());
        shapeComboBox->setObjectName(QString::fromUtf8("shapeComboBox"));
        shapeComboBox->setMaximumSize(QSize(100, 16777215));

        gridLayout->addWidget(shapeComboBox, 4, 1, 1, 1);

        nbPtsLabel = new QLabel(centralwidget);
        nbPtsLabel->setObjectName(QString::fromUtf8("nbPtsLabel"));
        nbPtsLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(nbPtsLabel, 5, 8, 1, 1);

        labelGain = new QLabel(centralwidget);
        labelGain->setObjectName(QString::fromUtf8("labelGain"));
        labelGain->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelGain, 3, 2, 1, 1);

        labelPtFrequency = new QLabel(centralwidget);
        labelPtFrequency->setObjectName(QString::fromUtf8("labelPtFrequency"));

        gridLayout->addWidget(labelPtFrequency, 3, 1, 1, 1);

        lineEditGain = new QLineEdit(centralwidget);
        lineEditGain->setObjectName(QString::fromUtf8("lineEditGain"));
        lineEditGain->setMaximumSize(QSize(75, 16777215));

        gridLayout->addWidget(lineEditGain, 3, 3, 1, 1);

        samplingComboBox = new QComboBox(centralwidget);
        samplingComboBox->addItem(QString());
        samplingComboBox->addItem(QString());
        samplingComboBox->addItem(QString());
        samplingComboBox->addItem(QString());
        samplingComboBox->setObjectName(QString::fromUtf8("samplingComboBox"));
        samplingComboBox->setMinimumSize(QSize(100, 0));
        samplingComboBox->setMaximumSize(QSize(16777215, 16777215));

        gridLayout->addWidget(samplingComboBox, 4, 9, 1, 1);

        labelDynParam1Shape = new QLabel(centralwidget);
        labelDynParam1Shape->setObjectName(QString::fromUtf8("labelDynParam1Shape"));
        labelDynParam1Shape->setMinimumSize(QSize(80, 0));
        labelDynParam1Shape->setMaximumSize(QSize(100, 16777215));
        labelDynParam1Shape->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelDynParam1Shape, 4, 4, 1, 1);

        labelDynParam2Sampling = new QLabel(centralwidget);
        labelDynParam2Sampling->setObjectName(QString::fromUtf8("labelDynParam2Sampling"));
        labelDynParam2Sampling->setMinimumSize(QSize(80, 0));
        labelDynParam2Sampling->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelDynParam2Sampling, 5, 10, 1, 1);

        previousButton = new QPushButton(centralwidget);
        previousButton->setObjectName(QString::fromUtf8("previousButton"));

        gridLayout->addWidget(previousButton, 6, 6, 1, 1);

        customPlot = new QCustomPlot(centralwidget);
        customPlot->setObjectName(QString::fromUtf8("customPlot"));

        gridLayout->addWidget(customPlot, 0, 5, 2, 8);

        labelSpatialStep = new QLabel(centralwidget);
        labelSpatialStep->setObjectName(QString::fromUtf8("labelSpatialStep"));
        labelSpatialStep->setMaximumSize(QSize(16777215, 25));

        gridLayout->addWidget(labelSpatialStep, 2, 0, 1, 1);

        lineEditDynParam2Shape = new QLineEdit(centralwidget);
        lineEditDynParam2Shape->setObjectName(QString::fromUtf8("lineEditDynParam2Shape"));
        lineEditDynParam2Shape->setMaximumSize(QSize(75, 16777215));

        gridLayout->addWidget(lineEditDynParam2Shape, 5, 5, 1, 1);

        centerZLabel = new QLabel(centralwidget);
        centerZLabel->setObjectName(QString::fromUtf8("centerZLabel"));
        centerZLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(centerZLabel, 6, 2, 1, 1, Qt::AlignTop);

        labelTimeStep = new QLabel(centralwidget);
        labelTimeStep->setObjectName(QString::fromUtf8("labelTimeStep"));
        labelTimeStep->setMaximumSize(QSize(16777215, 25));

        gridLayout->addWidget(labelTimeStep, 3, 0, 1, 1);

        nbPtsLineEdit = new QLineEdit(centralwidget);
        nbPtsLineEdit->setObjectName(QString::fromUtf8("nbPtsLineEdit"));
        nbPtsLineEdit->setMaximumSize(QSize(100, 16777215));

        gridLayout->addWidget(nbPtsLineEdit, 5, 9, 1, 1);

        plainTextEditDynParam1Shape = new QPlainTextEdit(centralwidget);
        plainTextEditDynParam1Shape->setObjectName(QString::fromUtf8("plainTextEditDynParam1Shape"));
        plainTextEditDynParam1Shape->setMaximumSize(QSize(75, 50));

        gridLayout->addWidget(plainTextEditDynParam1Shape, 6, 5, 1, 1);

        labelFPSpeed = new QLabel(centralwidget);
        labelFPSpeed->setObjectName(QString::fromUtf8("labelFPSpeed"));

        gridLayout->addWidget(labelFPSpeed, 2, 1, 1, 1);

        emitStopPushButton = new QPushButton(centralwidget);
        emitStopPushButton->setObjectName(QString::fromUtf8("emitStopPushButton"));

        gridLayout->addWidget(emitStopPushButton, 5, 7, 1, 1);

        emitStartPushButton = new QPushButton(centralwidget);
        emitStartPushButton->setObjectName(QString::fromUtf8("emitStartPushButton"));

        gridLayout->addWidget(emitStartPushButton, 4, 7, 1, 1);


        verticalLayout->addLayout(gridLayout);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1412, 26));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        menubar->addAction(menuFile->menuAction());
        menuFile->addAction(actionSave_sampling_strategy);
        menuFile->addAction(actionLoad_sampling_strategy);
        menuFile->addSeparator();
        menuFile->addAction(actionSave_geometry);
        menuFile->addAction(actionLoad_geometry);
        menuFile->addSeparator();
        menuFile->addAction(actionSave_both);
        menuFile->addAction(actionLaod_both);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Stimulus Design Tool", nullptr));
        actionCircle->setText(QCoreApplication::translate("MainWindow", "Circle", nullptr));
        actionSquare->setText(QCoreApplication::translate("MainWindow", "Square", nullptr));
        actionLine->setText(QCoreApplication::translate("MainWindow", "Line", nullptr));
        actionClear->setText(QCoreApplication::translate("MainWindow", "Clear", nullptr));
        actionTriangle->setText(QCoreApplication::translate("MainWindow", "Triangle", nullptr));
#if QT_CONFIG(tooltip)
        actionTriangle->setToolTip(QCoreApplication::translate("MainWindow", "Triangle", nullptr));
#endif // QT_CONFIG(tooltip)
        actionEllipse->setText(QCoreApplication::translate("MainWindow", "Ellipse", nullptr));
        actionRectangle->setText(QCoreApplication::translate("MainWindow", "Rectangle", nullptr));
#if QT_CONFIG(tooltip)
        actionRectangle->setToolTip(QCoreApplication::translate("MainWindow", "Rectangle", nullptr));
#endif // QT_CONFIG(tooltip)
        actionStep_sampling->setText(QCoreApplication::translate("MainWindow", "Step sampling", nullptr));
#if QT_CONFIG(tooltip)
        actionStep_sampling->setToolTip(QCoreApplication::translate("MainWindow", "Step sampling", nullptr));
#endif // QT_CONFIG(tooltip)
        actionCustom_step->setText(QCoreApplication::translate("MainWindow", "Custom step", nullptr));
#if QT_CONFIG(tooltip)
        actionCustom_step->setToolTip(QCoreApplication::translate("MainWindow", "Custom step", nullptr));
#endif // QT_CONFIG(tooltip)
        actionSave_sampling_strategy->setText(QCoreApplication::translate("MainWindow", "Save sampling strategy", nullptr));
        actionLoad_sampling_strategy->setText(QCoreApplication::translate("MainWindow", "Load sampling strategy", nullptr));
        actionRandom_sampling->setText(QCoreApplication::translate("MainWindow", "Random sampling", nullptr));
#if QT_CONFIG(tooltip)
        actionRandom_sampling->setToolTip(QCoreApplication::translate("MainWindow", "Random sampling", nullptr));
#endif // QT_CONFIG(tooltip)
        actionSave_geometry->setText(QCoreApplication::translate("MainWindow", "Save geometry", nullptr));
        actionLoad_geometry->setText(QCoreApplication::translate("MainWindow", "Load geometry", nullptr));
        actionSave_both->setText(QCoreApplication::translate("MainWindow", "Save both", nullptr));
        actionLaod_both->setText(QCoreApplication::translate("MainWindow", "Load both", nullptr));
        actionPointList->setText(QCoreApplication::translate("MainWindow", "Point List", nullptr));
#if QT_CONFIG(tooltip)
        actionPointList->setToolTip(QCoreApplication::translate("MainWindow", "Point List", nullptr));
#endif // QT_CONFIG(tooltip)
        actionArc->setText(QCoreApplication::translate("MainWindow", "Arc", nullptr));
#if QT_CONFIG(tooltip)
        actionArc->setToolTip(QCoreApplication::translate("MainWindow", "Arc", nullptr));
#endif // QT_CONFIG(tooltip)
        actionCustom_Time_Step_Sampling->setText(QCoreApplication::translate("MainWindow", "Custom Time Step Sampling", nullptr));
#if QT_CONFIG(tooltip)
        actionCustom_Time_Step_Sampling->setToolTip(QCoreApplication::translate("MainWindow", "Custom Time Step Sampling", nullptr));
#endif // QT_CONFIG(tooltip)
        playButton->setText(QCoreApplication::translate("MainWindow", "Play", nullptr));
        nextButton->setText(QCoreApplication::translate("MainWindow", "Next", nullptr));
        xCenterLineEdit->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        labelShape->setText(QCoreApplication::translate("MainWindow", "Shape", nullptr));
        centerXLabel->setText(QCoreApplication::translate("MainWindow", "Center: x", nullptr));
        lineEditSampleRate->setText(QCoreApplication::translate("MainWindow", "40000", nullptr));
        dynSamplingButton->setText(QCoreApplication::translate("MainWindow", "New sample", nullptr));
        labelDynParam3Shape->setText(QString());
        centerYLabel->setText(QCoreApplication::translate("MainWindow", "y", nullptr));
        zCenterLineEdit->setText(QCoreApplication::translate("MainWindow", "20", nullptr));
        labelShape_2->setText(QCoreApplication::translate("MainWindow", "Sampling", nullptr));
#if QT_CONFIG(tooltip)
        plainTextEditDynParam1Sampling->setToolTip(QCoreApplication::translate("MainWindow", "<html><head/><body><p><br/></p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        lineEditDynParam1Sampling->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        yCenterLineEdit->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        labelDynParam1Sampling->setText(QString());
        labelSampleRate->setText(QCoreApplication::translate("MainWindow", "Sample rate:", nullptr));
        lineEditDynParam3Sampling->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        labelDynParam2Shape->setText(QString());
        labelDynParam3Sampling->setText(QString());
        shapeComboBox->setItemText(0, QCoreApplication::translate("MainWindow", "...", nullptr));
        shapeComboBox->setItemText(1, QCoreApplication::translate("MainWindow", "Circle", nullptr));
        shapeComboBox->setItemText(2, QCoreApplication::translate("MainWindow", "Ellipse", nullptr));
        shapeComboBox->setItemText(3, QCoreApplication::translate("MainWindow", "Square", nullptr));
        shapeComboBox->setItemText(4, QCoreApplication::translate("MainWindow", "Rectangle", nullptr));
        shapeComboBox->setItemText(5, QCoreApplication::translate("MainWindow", "Triangle", nullptr));
        shapeComboBox->setItemText(6, QCoreApplication::translate("MainWindow", "Line", nullptr));
        shapeComboBox->setItemText(7, QCoreApplication::translate("MainWindow", "Point list", nullptr));
        shapeComboBox->setItemText(8, QCoreApplication::translate("MainWindow", "Arc", nullptr));

        nbPtsLabel->setText(QCoreApplication::translate("MainWindow", "Number of points", nullptr));
        labelGain->setText(QCoreApplication::translate("MainWindow", "Gain:", nullptr));
        labelPtFrequency->setText(QCoreApplication::translate("MainWindow", "Base frequency:", nullptr));
        lineEditGain->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        samplingComboBox->setItemText(0, QCoreApplication::translate("MainWindow", "Step sampling", nullptr));
        samplingComboBox->setItemText(1, QCoreApplication::translate("MainWindow", "Custom step", nullptr));
        samplingComboBox->setItemText(2, QCoreApplication::translate("MainWindow", "Custom time step", nullptr));
        samplingComboBox->setItemText(3, QCoreApplication::translate("MainWindow", "Random sampling", nullptr));

        labelDynParam1Shape->setText(QString());
        labelDynParam2Sampling->setText(QString());
        previousButton->setText(QCoreApplication::translate("MainWindow", "Previous", nullptr));
        labelSpatialStep->setText(QCoreApplication::translate("MainWindow", "Base step:", nullptr));
        centerZLabel->setText(QCoreApplication::translate("MainWindow", "z", nullptr));
        labelTimeStep->setText(QCoreApplication::translate("MainWindow", "Base Time step:", nullptr));
#if QT_CONFIG(tooltip)
        nbPtsLineEdit->setToolTip(QCoreApplication::translate("MainWindow", "<html><head/><body><p>Must be greater or equal to 0</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        nbPtsLineEdit->setText(QCoreApplication::translate("MainWindow", "50", nullptr));
        labelFPSpeed->setText(QCoreApplication::translate("MainWindow", "Base speed:", nullptr));
        emitStopPushButton->setText(QCoreApplication::translate("MainWindow", "Stop emiting", nullptr));
        emitStartPushButton->setText(QCoreApplication::translate("MainWindow", "Start emiiting", nullptr));
        menuFile->setTitle(QCoreApplication::translate("MainWindow", "File", nullptr));
        toolBar->setWindowTitle(QCoreApplication::translate("MainWindow", "toolBar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
