/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "geometries/ellipse.h"
#include "geometries/rectangles.h"
#include "geometries/triangle.h"
#include "geometries/pointlist.h"
#include "geometries/arc.h"
#include "importExport.h"
#include "emitters/ultraleapTPSEmitter.h"

#include <iostream>
#include <fstream>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , emitter(new UltraleapTPSEmitter())
    , samplingStrategy(new StepSampling(50)) // Base value
{
    ui->setupUi(this);

    std::string path = "data\\subsample_pressure.csv"; // Should be changed on other computers
    modelIntensity = recoverIntensitiesModel(path);
    setLineEditDynParamShape();
    setLineEditDynParamSampling();
    setBaseLineEditDynParamSampling();
    plotGeometry();
    plotIntensities();
}

MainWindow::~MainWindow()
{
    delete ui;
}

//////////////////////////////////
/// Geometry actions
/////////////////////////////////

void MainWindow::on_actionClear_triggered()
{
    currentShape = nullptr;
    plotGeometry();
}

void MainWindow::on_actionCircle_triggered()
{
    double radius = dynParam1Shape;
    currentShape = new Geometries::Circle(center, radius);
}

void MainWindow::on_actionEllipse_triggered()
{
    double a = dynParam1Shape, b = dynParam2Shape;
    currentShape = new Geometries::Ellipse(center, a, b);
}

void MainWindow::on_actionSquare_triggered()
{
    double side = dynParam1Shape;
    currentShape = new Geometries::Square(center, side);
}

void MainWindow::on_actionRectangle_triggered()
{
    double width = dynParam1Shape, height = dynParam2Shape;
    currentShape = new Geometries::Rectangle(center, width, height);
}

void MainWindow::on_actionTriangle_triggered()
{
    double side = dynParam1Shape;
    currentShape = (Geometries::RegularPolygon*)new Geometries::EquilateralTriangle(center, side);
}

void MainWindow::on_actionLine_triggered()
{
    double halfSize = dynParam1Shape / 2.f;
    currentShape = new Geometries::Segment(Ultrahaptics::Vector3(center.x - halfSize, center.y, center.z),
                                           Ultrahaptics::Vector3(center.x + halfSize, center.y, center.z));
}

void MainWindow::on_actionPointList_triggered()
{
    currentShape = new Geometries::PointList(dynParamStringShape.toStdString());
    samplingStrategy->setNbPoints(std::max<int>((int)1, ((Geometries::PointList*)currentShape)->getPoints().size()));
}

void MainWindow::on_actionArc_triggered()
{
    double radius = dynParam1Shape;
    double length = dynParam2Shape;

    currentShape = new Geometries::Arc(radius, length, center);
}

//////////////////////////////////
/// Sampling actions
/////////////////////////////////

void MainWindow::on_actionStep_sampling_triggered()
{
    int step = dynParam1Sampling;
    samplingStrategy = new StepSampling(samplingStrategy->getNbPts(), step);
}

void MainWindow::on_actionCustom_step_triggered()
{
    QString txtToBeParsed = dynParamStringSampling;
    std::pair<QVector<double>, QVector<int>> r = CustomSampling::parseTxt(txtToBeParsed);
    QVector<double> ratios = r.first;
    QVector<int> quantities = r.second;
    samplingStrategy =  new CustomSampling(ratios, quantities, samplingStrategy->getNbPts(), dynParam1Sampling);
}

void MainWindow::on_actionRandom_sampling_triggered()
{
    samplingStrategy = new RandomSampling(samplingStrategy->getNbPts());
}

void MainWindow::on_actionCustom_Time_Step_Sampling_triggered()
{
    QString txtToBeParsed = dynParamStringSampling;
    int direction = dynParam1Sampling;
    double freq = dynParam2Sampling * Ultrahaptics::Units::Hz;
    std::pair<QVector<int>, QVector<int>> r = CustomTimeStepSampling::parseTxt(txtToBeParsed);
    QVector<int> ratios = r.first;
    QVector<int> quantities = r.second;
    samplingStrategy = new CustomTimeStepSampling(ratios, quantities, samplingStrategy->getNbPts(), direction, freq);
}

//////////////////////////////////
/// Static widgets
/////////////////////////////////

void MainWindow::on_nextButton_clicked()
{
    if (currentShape != nullptr) {
        samplingStrategy->next();
        plotGeometry();
        plotIntensities();
    }
}

void MainWindow::on_previousButton_clicked()
{
    if (currentShape != nullptr) {
        samplingStrategy->previous();
        plotGeometry();
        plotIntensities();
    }
}

void MainWindow::on_playButton_clicked()
{
    if(!is_playing)
    {
        is_playing = true;
        ui->playButton->setText("Stop");

        playIndex = 0;
        resetTimer();
        playNextStep();
    }
    else
    {
        timer->stop();
        is_playing = false;
        ui->playButton->setText("Play");
    }

}

void MainWindow::playNextStep() {
    if (playIndex < samplingStrategy->getNbPts()) {
        on_nextButton_clicked();
        playIndex++;
        timer->start(250);
    }
    else
    {
        timer->stop();
        is_playing = false;
        ui->playButton->setText("Play");
    }
}

void MainWindow::on_shapeComboBox_currentIndexChanged(const QString &arg1)
{
    geometryName = arg1;
    resetTimer();
    setLineEditDynParamShape();      // Removing the previous dynamic parameters
    setBaseLineEditDynParamShape();  // Setting the new (base) ones
    updateGeoMetry();
}

void MainWindow::on_nbPtsLineEdit_editingFinished()
{
    std::pair<bool, int> ok_newNbPts = lineEdit2Int(ui->nbPtsLineEdit);
    if (ok_newNbPts.first && ok_newNbPts.second >= 0) {
        samplingStrategy->setNbPoints(ok_newNbPts.second);
        samplingStrategy->reset();
        updateSampling();
    }
}

void MainWindow::on_emitStartPushButton_clicked()
{
    if (is_emitting) return;
    is_emitting = true;
    if (currentShape != nullptr) {
        emitter->set_callback(currentShape, samplingStrategy, sampleRate);
        emitter->start();
    }
}

void MainWindow::on_emitStopPushButton_clicked()
{
    emitter->stop();
    is_emitting = false;
}

void MainWindow::on_xCenterLineEdit_editingFinished()
{
    std::pair<bool, double> ok_newX = lineEdit2Double(ui->xCenterLineEdit);
    if (ok_newX.first) {
        center.x = ok_newX.second;
        updateGeoMetry();
    }
}

void MainWindow::on_yCenterLineEdit_editingFinished()
{
    std::pair<bool, double> ok_newY = lineEdit2Double(ui->yCenterLineEdit);
    if (ok_newY.first) {
        center.y = ok_newY.second;
        updateGeoMetry();
    }
}

void MainWindow::on_zCenterLineEdit_editingFinished()
{
    std::pair<bool, double> ok_newZ = lineEdit2Double(ui->zCenterLineEdit);
    if (ok_newZ.first) {
        center.z = ok_newZ.second;
        updateGeoMetry();
    }
}

void MainWindow::on_samplingComboBox_currentIndexChanged(const QString &arg1)
{
    samplingName = arg1;
    resetTimer();
    setLineEditDynParamSampling();      // Removing the previous dynamic parameters
    setBaseLineEditDynParamSampling();  // Setting the new (base) ones
    updateSampling();
}

void MainWindow::on_lineEditSampleRate_editingFinished()
{
    std::pair<bool, int> ok_newSampRate = lineEdit2Int(ui->lineEditSampleRate);
    if (ok_newSampRate.first && ok_newSampRate.second > 0) {
        sampleRate = ok_newSampRate.second;
        samplingStrategy->reset();
        updateSampling();
    }
}

//////////////////////////////////
/// Dynamic widgets
/////////////////////////////////


void MainWindow::setLineEditDynParamShape() {
    ui->labelDynParam1Shape->setText("");
    ui->labelDynParam2Shape->setText("");
    ui->labelDynParam3Shape->setText("");
    ui->plainTextEditDynParam1Shape->blockSignals(true);
    ui->plainTextEditDynParam1Shape->setPlainText("");
    ui->plainTextEditDynParam1Shape->blockSignals(false);
    ui->lineEditDynParam1Shape->setVisible(false);
    ui->lineEditDynParam2Shape->setVisible(false);
    ui->plainTextEditDynParam1Shape->setVisible(false);
}

void MainWindow::setLineEditDynParamShape(QString s1, double d1) {
    ui->labelDynParam1Shape->setText(s1);
    ui->lineEditDynParam1Shape->setVisible(true);
    ui->lineEditDynParam1Shape->setText(QString::number(d1));
}

void MainWindow::setLineEditDynParamShape(QString s1, QString s2, double d1, double d2) {
    setLineEditDynParamShape(s1, d1);
    ui->labelDynParam2Shape->setText(s2);
    ui->lineEditDynParam2Shape->setVisible(true);
    ui->lineEditDynParam2Shape->setText(QString::number(d2));
}

void MainWindow::setBaseLineEditDynParamShape() {
    if (geometryName == CIRCLE_NAME) setLineEditDynParamShape("Radius", 10.f);
    else if (geometryName == ELLIPSE_NAME) setLineEditDynParamShape("Half width", "Half height", 10.f, 5.f);
    else if (geometryName == SQUARE_NAME) setLineEditDynParamShape("Side length", 20.f);
    else if (geometryName == RECTANGLE_NAME) setLineEditDynParamShape("Width", "Height", 20.f, 10.f);
    else if (geometryName == TRIANGLE_NAME) setLineEditDynParamShape("Side Length", 20.f);
    else if (geometryName == LINE_NAME) setLineEditDynParamShape("Length", 20.f);
    else if (geometryName == POINT_LIST_NAME) {
        setLineEditDynParamShape();
        ui->plainTextEditDynParam1Shape->setVisible(true);
        ui->labelDynParam3Shape->setText("Points");
    }
    else if (geometryName == ARC_NAME) setLineEditDynParamShape("Radius", "Arc length", 10.f, 15.f);
    else if (geometryName == "...") setLineEditDynParamShape();
}

void MainWindow::on_lineEditDynParam1Shape_editingFinished()
{
    MainWindow::updateGeoMetry();
}

void MainWindow::on_lineEditDynParam2Shape_editingFinished()
{
    updateGeoMetry();
}

void MainWindow::on_plainTextEditDynParam1Shape_textChanged()
{
    dynParamStringShape = ui->plainTextEditDynParam1Shape->toPlainText();
    on_actionPointList_triggered();
    ui->nbPtsLineEdit->setText(QString::number(samplingStrategy->getNbPts()));
    plotGeometry();
    plotIntensities();
}

void MainWindow::setLineEditDynParamSampling() {
    ui->labelDynParam1Sampling->setText("");
    ui->labelDynParam2Sampling->setText("");
    ui->labelDynParam3Sampling->setText("");
    ui->lineEditDynParam1Sampling->setVisible(false);
    ui->lineEditDynParam3Sampling->setVisible(false);
    ui->plainTextEditDynParam1Sampling->setVisible(false);
    ui->dynSamplingButton->setVisible(false);
    bool oldBlock = ui->plainTextEditDynParam1Sampling->blockSignals(true);
    ui->plainTextEditDynParam1Sampling->setPlainText("");
    ui->plainTextEditDynParam1Sampling->setToolTip("");
    ui->plainTextEditDynParam1Sampling->blockSignals(oldBlock);
}

void MainWindow::setLineEditDynParamSampling(QString s1, int i1) {
    ui->labelDynParam1Sampling->setText(s1);
    ui->lineEditDynParam1Sampling->setVisible(true);
    ui->lineEditDynParam1Sampling->setText(QString::number(i1));
}

void MainWindow::setLineEditDynParamSampling(QString s1, QString s2, int val1, double val2) {
    ui->labelDynParam3Sampling->setText(s2);
    ui->lineEditDynParam3Sampling->setText(QString::number(val2));
    setLineEditDynParamSampling(s1, val1);
}

void MainWindow::setPlainTextEditDynParamSampling(QString s1, QString value, QString tooltip) {
    ui->labelDynParam2Sampling->setText(s1);
    ui->plainTextEditDynParam1Sampling->setVisible(true);
    ui->plainTextEditDynParam1Sampling->setPlainText(value);
    ui->plainTextEditDynParam1Sampling->setToolTip(tooltip);
}

void MainWindow::setBaseLineEditDynParamSampling() {
    if (samplingName == STEP_SAMPLING_NAME) setLineEditDynParamSampling("Step", 1);
    else if (samplingName == CUSTOM_STEP_SAMPLING_NAME) {
        QString tooltip = "Expected format: double int? for each line.\nThe double corresponds to the ratio of the expected step over the base step.\n"
                          "The int corresponds to the number of points using the same step.\nIf it is not written, it is set to 1.";
        setPlainTextEditDynParamSampling(CUSTOM_STEP_SAMPLING_NAME, "", tooltip);
        setLineEditDynParamSampling("Skip step", 1);
    }
    else if (samplingName == RANDOM_SAMPLING_NAME) ui->dynSamplingButton->setVisible(true);
    else if (samplingName == CUSTOM_TIME_STEP_SAMPLING_NAME) {
        QString tooltip = "Expected format: int int? for each line.\nThe first int corresponds to the number of time steps we stay at one point.\n"
                          "The second int corresponds to the number of points using the time step.\nIf it is not written, it is set to 1.";
        setPlainTextEditDynParamSampling(CUSTOM_TIME_STEP_SAMPLING_NAME, "", tooltip);
        setLineEditDynParamSampling("Direction", "Frequency", 1, 200);
        ui->lineEditDynParam3Sampling->setVisible(true);
    }
}

void MainWindow::on_lineEditDynParam1Sampling_editingFinished()
{
    updateSampling();
}

void MainWindow::on_plainTextEditDynParam1Sampling_textChanged()
{
    updateSampling();
}

void MainWindow::on_lineEditDynParam3Sampling_editingFinished()
{
    updateSampling();
}

void MainWindow::on_dynSamplingButton_clicked()
{
    if (samplingName == RANDOM_SAMPLING_NAME) {
        samplingStrategy = new RandomSampling(samplingStrategy->getNbPts());
        reloadEmitter();
        plotGeometry();
        plotIntensities();
    }
}

//////////////////////////////////
/// Plots
/////////////////////////////////

void MainWindow::plotGeometry() {
    QSize size = ui->customPlot->size();
    double ratio = (double)size.width() / (double)size.height();  // >= 1
    double range = 10.f * 1.2f; // The device is 20 * 20cm. The 1.2 factor is to have some margin

    // Remove previous information
    ui->customPlot->clearGraphs();
    // don't display the axis
    ui->customPlot->xAxis->setVisible(false);
    ui->customPlot->yAxis->setVisible(false);
    // set axes ranges, so we see all data:
    ui->customPlot->yAxis->setRange(-range, range);
    ui->customPlot->xAxis->setRange(-range * ratio, range * ratio);

    // create graph
    ui->customPlot->addGraph();
    // set the scatter options
    ui->customPlot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::blue, Qt::white, 5));
    ui->customPlot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsNone);

    ui->customPlot->addGraph();
    ui->customPlot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::red, Qt::white, 5));
    ui->customPlot->graph(1)->setLineStyle(QCPGraph::LineStyle::lsNone);

    if (currentShape != nullptr) {
        QVector<QVector<double>> pts = samplingStrategy->get2DPoints(currentShape);

        QVector<double> x = pts[0];
        QVector<double> y = pts[1];
        ui->customPlot->graph(0)->setData(x, y);
        QVector<double> xHighlight(1), yHighlight(1);
        Ultrahaptics::Vector3 pos = samplingStrategy->getCurrentPoint(currentShape);
        xHighlight[0] = pos.x;
        yHighlight[0] = pos.y;
        ui->customPlot->graph(1)->setData(xHighlight, yHighlight);
    }

    ui->customPlot->replot();
}

void MainWindow::plotIntensities() {
    QVector<double> intensitiesDynamic = computeIntensities(samplingStrategy->getCurrentPoint(currentShape));
    QVector<double> intensitiesStatic = computeIntensities(samplingStrategy->pointFromIndex(currentShape, 0));
    QVector<double> indices(samplingStrategy->getNbPts());
    std::iota(indices.begin(), indices.end(), 0);

    int xmin = -1, ymin = -100, ymax = 3000;

    if (intensitiesDynamic.length() > 0 && intensitiesStatic.length() > 0) {
        // Dynamic plot
        // Remove previous information
        ui->customPlotIntensity->clearGraphs();
        // set axes ranges, so we see all data:
        ui->customPlotIntensity->xAxis->setRange(xmin, samplingStrategy->getNbPts());
        ui->customPlotIntensity->yAxis->setRange(ymin, ymax);
        ui->customPlotIntensity->xAxis->setLabel("Point Index");
        ui->customPlotIntensity->yAxis->setLabel("Intensity");

        // create graph
        ui->customPlotIntensity->addGraph();

        ui->customPlotIntensity->graph(0)->setData(indices, intensitiesDynamic);
        ui->customPlotIntensity->replot();

        // Static plot
        // Remove previous information
        ui->customPlotIntensity_2->clearGraphs();
        // set axes ranges, so we see all data:
        ui->customPlotIntensity_2->xAxis->setRange(xmin, samplingStrategy->getNbPts());
        ui->customPlotIntensity_2->yAxis->setRange(ymin, ymax);
        ui->customPlotIntensity_2->xAxis->setLabel("Point Index");
        ui->customPlotIntensity_2->yAxis->setLabel("Intensity");

        // create graphs
        ui->customPlotIntensity_2->addGraph();
        ui->customPlotIntensity_2->addGraph(); // red point
        ui->customPlotIntensity_2->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::red, Qt::white, 5));
        ui->customPlotIntensity_2->graph(1)->setLineStyle(QCPGraph::LineStyle::lsNone);

        ui->customPlotIntensity_2->graph(0)->setData(indices, intensitiesStatic);
        int currentIndex = samplingStrategy->getAbsoluteIndex();
        QVector<double> x, y;
        x.push_back(currentIndex);
        y.push_back(intensitiesStatic[currentIndex]);
        ui->customPlotIntensity_2->graph(1)->setData(x, y);
        ui->customPlotIntensity_2->replot();
    }
}

//////////////////////////////////
/// Utils
/////////////////////////////////

void MainWindow::resetTimer() {
    if (timer != nullptr) delete timer;
    timer = new QTimer();
    connect(timer, &QTimer::timeout, [this](){playNextStep();});
}

void MainWindow::updateGeoMetry() {
    std::pair<bool, double> ok_val1 = lineEdit2Double(ui->lineEditDynParam1Shape);
    std::pair<bool, double> ok_val2 = lineEdit2Double(ui->lineEditDynParam2Shape);
    QString val3 = ui->plainTextEditDynParam1Shape->toPlainText();

    if (geometryName == CIRCLE_NAME) {

        if (ok_val1.first && ok_val1.second > 0) {
            dynParam1Shape = ok_val1.second; // Radius
        }
        on_actionCircle_triggered();
    }
    else if (geometryName == ELLIPSE_NAME) {
        if (ok_val1.first && ok_val2.first && ok_val1.second > 0 && ok_val2.second > 0) {
            dynParam1Shape = ok_val1.second; // Half width
            dynParam2Shape = ok_val2.second; // Half height
        }
        on_actionEllipse_triggered();
    }
    else if (geometryName == SQUARE_NAME) {
        if (ok_val1.first && ok_val1.second > 0) {
            dynParam1Shape = ok_val1.second;   // Side
        }
        on_actionSquare_triggered();
    }
    else if (geometryName == RECTANGLE_NAME) {
        if (ok_val1.first && ok_val2.first && ok_val1.second > 0 && ok_val2.second > 0) {
            dynParam1Shape = ok_val1.second; // Width
            dynParam2Shape = ok_val2.second; // Height
        }
        on_actionRectangle_triggered();
    }
    else if (geometryName == TRIANGLE_NAME) {
        if (ok_val1.first && ok_val1.second > 0) {
            dynParam1Shape = ok_val1.second; // Side
        }
        on_actionTriangle_triggered();
    }
    else if (geometryName == LINE_NAME) {
        if (ok_val1.first && ok_val1.second > 0) {
            dynParam1Shape = ok_val1.second; // Length
        }
        on_actionLine_triggered();
    }
    else if (geometryName == POINT_LIST_NAME) {
        dynParamStringShape = val3;
        on_actionPointList_triggered();
    }
    else if (geometryName == ARC_NAME) {
        if (ok_val1.first && ok_val1.second > 0 &&
            ok_val2.first && ok_val2.second > 0) {
            dynParam1Shape = ok_val1.second;
            dynParam2Shape = ok_val2.second;
        }
        on_actionArc_triggered();
    }
    else if (geometryName == "...") on_actionClear_triggered();

    reloadEmitter();
    updateInfoLabels();
    plotGeometry();
    plotIntensities();
}

void MainWindow::updateSampling() {
    std::pair<bool, double> ok_val1 = lineEdit2Double(ui->lineEditDynParam1Sampling);
    std::pair<bool, double> ok_val2 = lineEdit2Double(ui->lineEditDynParam3Sampling);
    dynParamStringSampling = ui->plainTextEditDynParam1Sampling->toPlainText();
    if (samplingName == STEP_SAMPLING_NAME) {
        if (ok_val1.first && ok_val1.second >= 1 && ok_val1.second < samplingStrategy->getNbPts()) {
            dynParam1Sampling = ok_val1.second;
        }
        on_actionStep_sampling_triggered();
    }
    else if (samplingName == CUSTOM_STEP_SAMPLING_NAME) {
        dynParamStringSampling = ui->plainTextEditDynParam1Sampling->toPlainText();
        if (ok_val1.first && ok_val1.second >= 1) {
            dynParam1Sampling = ok_val1.second;
        }
        on_actionCustom_step_triggered();
    }
    else if (samplingName == RANDOM_SAMPLING_NAME) {
        on_actionRandom_sampling_triggered();
    }
    else if (samplingName == CUSTOM_TIME_STEP_SAMPLING_NAME) {
        dynParamStringSampling = ui->plainTextEditDynParam1Sampling->toPlainText();
        if (ok_val1.first && ok_val1.second != 0 && ok_val2.first && ok_val2.second > 0) {
            dynParam1Sampling = ok_val1.second;
            dynParam2Sampling = ok_val2.second;
        }
        on_actionCustom_Time_Step_Sampling_triggered();
    }

    reloadEmitter();
    updateInfoLabels();
    plotGeometry();
    plotIntensities();
}

double MainWindow::computeIntensity(Ultrahaptics::Vector3 reciever, Ultrahaptics::Vector3 emitter) {
    // One is the emitter, the other is the reciever
    reciever = reciever * Ultrahaptics::Units::centimetres;
    emitter = emitter * Ultrahaptics::Units::centimetres;
    double dist2 = std::pow(reciever.x - emitter.x, 2.f) + std::pow(reciever.y - emitter.y, 2.f) + std::pow(reciever.z - emitter.z, 2.f);
    double dist = std::sqrt(dist2);

    if (dist >= .04) return .0f;

    /*int index = 0;
    double min_d = .04;
    for (int i = 0 ; i < 80 ; i++) {
        double d = std::abs(dist - modelIntensity.first[i]);
        if (d < min_d) {
            min_d = d;
            index = i;
        }
    }*/

    int index = std::round(79.f * (dist + .04) / 0.08);

    return modelIntensity.second[index];
}

QVector<double> MainWindow::computeIntensities(Ultrahaptics::Vector3 reciever) {
    QVector<Ultrahaptics::Vector3> points = samplingStrategy->get3DPoints(currentShape);
    QVector<double> res;

    for (Ultrahaptics::Vector3 emitter: points) {
        res.push_back(computeIntensity(reciever, emitter));
    }
    return res;
}

std::pair<QVector<double>, QVector<double>> MainWindow::recoverIntensitiesModel(std::string path) {
    QVector<double> dist, pressure;
    std::fstream file;
    file.open(path, std::ios::in);
    std::string dist_string, pressure_string, sep;

    while (file.peek() != EOF) {
        file >> dist_string >> sep >> pressure_string;
        dist.push_back(std::stod(dist_string));
        pressure.push_back(std::stod(pressure_string));
    }
    return std::make_pair(dist, pressure);
}

std::pair<bool, int> MainWindow::lineEdit2Int(QLineEdit* widget) {
    QString val = widget->text();
    bool ok;
    int intVal = val.toInt(&ok);
    return std::make_pair(ok, intVal);
}

std::pair<bool, double> MainWindow::lineEdit2Double(QLineEdit* widget) {
    QString val = widget->text();
    bool ok;
    double doubleVal = val.toDouble(&ok);
    return std::make_pair(ok, doubleVal);
}

void MainWindow::reloadEmitter() {
    if (is_emitting) {
        on_emitStopPushButton_clicked();
        on_emitStartPushButton_clicked();
    }
}

void MainWindow::updateInfoLabels() {
    std::vector<double> params = samplingStrategy->getCaracteristics(sampleRate, currentShape);
    if (params.size() == 4) {
        ui->labelTimeStep->setText("Base time step: " + QString::number(params[0]) + "µs");
        ui->labelPtFrequency->setText("Base frequency: " + QString::number(params[1]) + "Hz");
        ui->labelSpatialStep->setText("Base step: " + QString::number(params[2], 'g', 3) + "mm");
        ui->labelFPSpeed->setText("Base speed: " + QString::number(params[3], 'g', 3) + "m/s");
    }
    else {
        ui->labelTimeStep->setText("Base time step: NaN");
        ui->labelPtFrequency->setText("Base frequency: Nan");
        ui->labelSpatialStep->setText("Base step: Nan");
        ui->labelFPSpeed->setText("Base speed: Nan");
    }
}

void MainWindow::updateAfterSamplingLoad() {
    ui->nbPtsLineEdit->setText(QString::number(samplingStrategy->getNbPts()));
    if (samplingName == STEP_SAMPLING_NAME) {
        ui->lineEditDynParam1Sampling->setText(QString::number(((StepSampling*)samplingStrategy)->getStep()));
    }
    else if (samplingName == CUSTOM_STEP_SAMPLING_NAME) {
        QString boxText = "";
        CustomSampling* stratCast = (CustomSampling*)samplingStrategy;
        QVector<double> ratios = stratCast->getRatios();
        QVector<int> quantities = stratCast->getQuantities();
        for (int i = 0 ; i < ratios.size() ; i++)
            boxText += QString::number(ratios[i]) + ' ' + QString::number(quantities[i]) + '\n';
        ui->plainTextEditDynParam1Sampling->setPlainText(boxText);
        ui->lineEditDynParam1Sampling->setText(QString::number(stratCast->getSkipStep()));
    }
    else if (samplingName == RANDOM_SAMPLING_NAME) {;}
    else if (samplingName == CUSTOM_TIME_STEP_SAMPLING_NAME) {
        QString boxText = "";
        CustomTimeStepSampling* stratCast = (CustomTimeStepSampling*)samplingStrategy;
        QVector<int> ratios = stratCast->getRatios();
        QVector<int> quantities = stratCast->getQuantities();
        for (int i = 0 ; i < ratios.size() ; i++)
            boxText += QString::number(ratios[i]) + ' ' + QString::number(quantities[i]) + '\n';

        ui->plainTextEditDynParam1Sampling->setPlainText(boxText);
        ui->lineEditDynParam1Sampling->setText(QString::number(stratCast->getDirection()));
        ui->lineEditDynParam3Sampling->setText(QString::number(((CustomTimeStepSampling*)samplingStrategy)->getFrequency()));
    }
    ui->samplingComboBox->setCurrentText(samplingName);
}

void MainWindow::updateAfterGeometryLoad() {
    Ultrahaptics::Vector3 center = currentShape->get_center();
    ui->xCenterLineEdit->setText(QString::number(center.x));
    ui->yCenterLineEdit->setText(QString::number(center.y));
    ui->zCenterLineEdit->setText(QString::number(center.z));
    ui->shapeComboBox->setCurrentText(geometryName);

    if (geometryName == CIRCLE_NAME) {
        ui->lineEditDynParam1Shape->setText(QString::number(((Geometries::Circle*)currentShape)->getRx()));
    }
    else if (geometryName == ELLIPSE_NAME) {
        ui->lineEditDynParam1Shape->setText(QString::number(((Geometries::Ellipse*)currentShape)->getRx()));
        ui->lineEditDynParam2Shape->setText(QString::number(((Geometries::Ellipse*)currentShape)->getRy()));
    }
    else if (geometryName == SQUARE_NAME) {
        ui->lineEditDynParam1Shape->setText(QString::number(((Geometries::Square*)currentShape)->get_length()));
    }
    else if (geometryName == RECTANGLE_NAME) {
        ui->lineEditDynParam1Shape->setText(QString::number(((Geometries::Rectangle*)currentShape)->get_width()));
        ui->lineEditDynParam2Shape->setText(QString::number(((Geometries::Rectangle*)currentShape)->get_length()));
    }
    else if (geometryName == TRIANGLE_NAME) {
        ui->lineEditDynParam1Shape->setText(QString::number(((Geometries::RegularPolygon*)currentShape)->get_side_length()));
    }
    else if (geometryName == LINE_NAME) {
        ui->lineEditDynParam1Shape->setText(QString::number(((Geometries::Segment*)currentShape)->length()));
    }
    else if (geometryName == POINT_LIST_NAME) {
        std::stringstream plainText;
        for (Ultrahaptics::Vector3 pt: ((Geometries::PointList*)currentShape)->getPoints()) {
            plainText << pt.x << ' ' << pt.y << ' ' << pt.z << '\n';
        }
        ui->plainTextEditDynParam1Shape->blockSignals(true);
        ui->plainTextEditDynParam1Shape->setPlainText(QString::fromStdString(plainText.str()));
        ui->plainTextEditDynParam1Shape->blockSignals(false);
        ui->nbPtsLineEdit->setText(QString::number(samplingStrategy->getNbPts()));
    }
    else if (geometryName == ARC_NAME) {
        ui->lineEditDynParam1Shape->setText(QString::number(((Geometries::Arc*)currentShape)->getRadius()));
        ui->lineEditDynParam2Shape->setText(QString::number(((Geometries::Arc*)currentShape)->getLength()));
    }
}

//////////////////////////////////
/// Import / export actions
/////////////////////////////////

void MainWindow::on_actionSave_sampling_strategy_triggered()
{
    QString path = QFileDialog::getSaveFileName(this, "Save sampling strategy", "saved_sampling_strategies", "Sampling files (*.samp)");
    if (path != "") {
        ImportExport::exportStrategy(samplingStrategy, samplingName.toStdString(), path.toStdString());
    }
}

void MainWindow::on_actionLoad_sampling_strategy_triggered()
{
    QString path = QFileDialog::getOpenFileName(this, "Load sampling strategy", "saved_sampling_strategies", "Sampling files (*.samp)");
    if (path != "") {
        std::pair<std::string, SamplingStrategy*> pair = ImportExport::importStrategy(path.toStdString());
        samplingName = QString::fromStdString(pair.first);
        samplingStrategy = pair.second;

        bool blockTxt = ui->plainTextEditDynParam1Sampling->blockSignals(true);
        bool blockComboBox = ui->samplingComboBox->blockSignals(true);
        setLineEditDynParamSampling();      // Removing the previous dynamic parameters
        setBaseLineEditDynParamSampling();  // Setting the new (base) ones
        updateAfterSamplingLoad();          // Setting the right values
        ui->plainTextEditDynParam1Sampling->blockSignals(blockTxt);
        ui->samplingComboBox->blockSignals(blockComboBox);
        reloadEmitter();
        plotGeometry();
        plotIntensities();
    }
}

void MainWindow::on_actionSave_geometry_triggered() {
    QString path = QFileDialog::getSaveFileName(this, "Save geometry", "saved_geometries", "Geometry files (*.geom)");
    if (path != "") {
        ImportExport::exportGeometry(currentShape, geometryName.toStdString(), path.toStdString());
    }
}

void MainWindow::on_actionLoad_geometry_triggered()
{
    QString path = QFileDialog::getOpenFileName(this, "Load geometry", "saved_geometries", "Geometry files (*.geom)");
    if (path != "") {
        std::pair<std::string, Geometries::Geometry*> pair = ImportExport::importGeometry(path.toStdString());
        geometryName = QString::fromStdString(pair.first);
        currentShape = pair.second;

        if (geometryName == POINT_LIST_NAME) samplingStrategy->setNbPoints(((Geometries::PointList*)currentShape)->getPoints().size());

        ui->shapeComboBox->blockSignals(true);
        setLineEditDynParamShape();      // Removing the previous dynamic parameters
        setBaseLineEditDynParamShape();  // Setting the new (base) ones
        updateAfterGeometryLoad();       // Setting the right values
        ui->shapeComboBox->blockSignals(false);

        reloadEmitter();
        plotGeometry();
        plotIntensities();
    }
}

void MainWindow::on_actionSave_both_triggered()
{
    QString path = QFileDialog::getSaveFileName(this, "Save geometry and sampling strategy", "saved_both", "Geometry+Sampling files (*.geomsamp)");
    if (path != "") {
        ImportExport::exportGeometryAndStrategy(samplingStrategy, samplingName.toStdString(),
                                                currentShape, geometryName.toStdString(),
                                                path.toStdString(), sampleRate);
    }
}

void MainWindow::on_actionLaod_both_triggered()
{
    QString path = QFileDialog::getOpenFileName(this, "Load geometry and sampling strategy", "saved_both", "Geometry+Sampling files (*.geomsamp)");
    if (path != "") {
        std::tuple<Geometries::Geometry*, std::string,
                   SamplingStrategy*, std::string, int> t = ImportExport::importGeometryAndStrategy(path.toStdString());

        sampleRate = std::get<4>(t);
        ui->lineEditSampleRate->setText(QString::number(sampleRate));

        samplingName = QString::fromStdString(std::get<3>(t));
        samplingStrategy = std::get<2>(t);

        ui->plainTextEditDynParam1Sampling->blockSignals(true);
        ui->samplingComboBox->blockSignals(true);
        setLineEditDynParamSampling();      // Removing the previous dynamic parameters
        setBaseLineEditDynParamSampling();  // Setting the new (base) ones
        updateAfterSamplingLoad();          // Setting the right values
        ui->plainTextEditDynParam1Sampling->blockSignals(false);
        ui->samplingComboBox->blockSignals(false);

        geometryName = QString::fromStdString(std::get<1>(t));
        currentShape = std::get<0>(t);

        ui->shapeComboBox->blockSignals(true);
        setLineEditDynParamShape();      // Removing the previous dynamic parameters
        setBaseLineEditDynParamShape();  // Setting the new (base) ones
        updateAfterGeometryLoad();       // Setting the right values
        ui->shapeComboBox->blockSignals(false);

        reloadEmitter();
        plotGeometry();
        plotIntensities();
    }
}



