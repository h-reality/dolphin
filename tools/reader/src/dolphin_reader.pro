QT       += core

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../../../src/geometries/arc.cpp \
    ../../../src/geometries/ellipse.cpp \
    ../../../src/geometries/geometry.cpp \
    ../../../src/geometries/pointlist.cpp \
    ../../../src/geometries/polygon.cpp \
    ../../../src/geometries/polylines.cpp \
    ../../../src/geometries/rectangles.cpp \
    ../../../src/geometries/regularpolygon.cpp \
    ../../../src/geometries/triangle.cpp \
    ../../../src/geometries/utils.cpp \
    ../../../src/sampling_strategies/customSampling.cpp \
    ../../../src/sampling_strategies/customTimeStepSampling.cpp \
    ../../../src/sampling_strategies/randomSampling.cpp \
    ../../../src/sampling_strategies/samplingStrategy.cpp \
    ../../../src/sampling_strategies/stepSampling.cpp \
    ../../../src/emitters/ultraleapTPSEmitter.cpp \
    ../../../src/importExport.cpp \
    main.cpp

HEADERS += \
    mainwindow.h

INCLUDEPATH += "../../../src/"

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# ULTRAHAPTICS DEPENDENCIES
DEPENDPATH += "C:\Program Files\Ultrahaptics"
INCLUDEPATH += "C:\Program Files\Ultrahaptics\include"
LIBS += -L"C:\Program Files\Ultrahaptics\lib" -lUltrahaptics

