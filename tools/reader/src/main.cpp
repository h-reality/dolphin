/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include <cmath>
#include <iostream>
#include <fstream>
#include <chrono>
#include <cmath>
#include <thread>
#include <string>
#include <vector>

#include "UltrahapticsTimePointStreaming.hpp"

#include "emitters/ultraleapTPSEmitter.h"
#include "importExport.h"

#ifndef M_PI
#define M_PI 3.14159265358979323
#endif

using Seconds = std::chrono::duration<float>;
static auto start_time = std::chrono::steady_clock::now();

Emitter* emitter = new UltraleapTPSEmitter();

// Structure for passing information on the type of point to create
struct PtList
{
    std::vector<Ultrahaptics::Vector3> pts;
    Ultrahaptics::HostTimePoint pattern_start;
    std::vector<double> intensities;
    unsigned int hardware_sample_rate;
    bool is_on;
    bool stopped = false;

    const Ultrahaptics::Vector3 evaluateAt(Seconds t) {
        int index = ((unsigned int)(t.count() * hardware_sample_rate)) % pts.size();
        return pts[index];
    }
     double getIntensity(Seconds t) {
        int index = t.count() * hardware_sample_rate;
        if (index >= (int)intensities.size() || index < 0) return 0.f;
        return intensities[index];
    }
};

// Callback function for filling out complete device output states through time
void emitter_callback(const Ultrahaptics::TimePointStreaming::Emitter &timepoint_emitter,
                         Ultrahaptics::TimePointStreaming::OutputInterval &interval,
                         const Ultrahaptics::HostTimePoint &submission_deadline,
                         void *user_pointer)
{
    PtList* ptList = static_cast<PtList*>(user_pointer);

    for (auto& sample: interval) {
        const Seconds t = sample - ptList->pattern_start;
        const Ultrahaptics::Vector3 position = ptList->evaluateAt(t);
        const double intensity = ptList->getIntensity(t);

        sample.persistentControlPoint(0).setPosition(position);
        sample.persistentControlPoint(0).setIntensity(intensity);
    }
}

// Reads stdin to update the shape
void read_pipe(void* user_pointer) {
    PtList* ptList = static_cast<PtList*>(user_pointer);

    std::string input;
    std::cin >> input;

    if (input == "-2") { // -2 means stop the program
        std::cout << "Exiting" << std::endl;
        std::cout.flush();

        ptList->is_on = false;
        emitter->stop();
        return;
    }

    // path recieved
    std::vector<Ultrahaptics::Vector3> pts;
    std::vector<double> intensities;
    int sampleRate;

    // .geomsamp file received, using the import method
    if (input.find(".geomsamp") != std::string::npos) {
        std::tuple<Geometries::Geometry*, std::string,
                       SamplingStrategy*, std::string, int> t = ImportExport::importGeometryAndStrategy(input);
        Geometries::Geometry* g = std::get<0>(t);
        SamplingStrategy* s = std::get<2>(t);
        sampleRate = std::get<4>(t);

        pts = s->get3DPoints(g).toStdVector();
        intensities = s->getIntensities(sampleRate).toStdVector();

        std::cout << "Loaded " << input << " using import" << std::endl;
        std::cout.flush();
    }

    // .ptlist file received, using the parsing method
    else if (input.find(".ptlist") != std::string::npos)  {
        // format of the file: nbPts sampleRate x1 y1 z1 intensity1 x2 ... inbPts
        std::ifstream inFile(input);
        int nbPts, sampleRate;
        float x, y, z, intensity;
        inFile >> nbPts >> sampleRate;

        for (int i = 0 ; i < nbPts ; i++) {
            inFile >> x >> y >> z >> intensity;
            pts.push_back(Ultrahaptics::Vector3(x, y, z));
            intensities.push_back(intensity);

        }
        inFile.close();

        std::cout << "Loaded " << input << " using parsing" << std::endl;
        std::cout.flush();
    }

    // Wrong file format
    else {
        std::cout << "Received a wrong file format. Should be .ptlist or .geomsamp" << std::endl;
        std::cout.flush();
        return;
    }

    ptList->pts = pts;
    ptList->hardware_sample_rate = sampleRate;
    ptList->intensities = intensities;
    ptList->pattern_start = std::chrono::steady_clock::now();
    ptList->stopped = false;

    // Start the array
    emitter->start();
}

int main()
{
    // Create a structure containing our control point data and fill it in
    PtList ptList;
    //Set the pattern as on
    ptList.is_on = true;
    ptList.pattern_start = std::chrono::steady_clock::now();
    ptList.hardware_sample_rate = emitter->getSampleRate();
    // Set the callback function to the callback written above
    emitter->set_callback(emitter->getSampleRate(), &ptList, new UltraleapTPSCallbackHolder(emitter_callback));

    std::cout.flush();


    while(ptList.is_on)
    {
        read_pipe(&ptList);
    }

    // Wait for enter key to be pressed.
    std::cout << "Hit ENTER to quit..." << std::endl;
    std::string line;
    std::getline(std::cin, line);
    // Stop the array
    emitter->stop();
    return 0;
}
