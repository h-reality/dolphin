from subprocess import Popen, PIPE
import os


def send_data(p, a):
    t1 = time.time()
    p.stdout.flush()
    p.stdin.flush()
    data = str(a)
    value = data + '\n'
    value = bytes(value, 'UTF-8')  # Needed in Python 3.
    p.stdin.write(value)
    p.stdin.flush()
    result = p.stdout.readline().strip().decode()
    print("Received: ", result)
    


def main(path):
    _thisDir = os.path.dirname(os.path.abspath(__file__))
    os.chdir(_thisDir)
    p = Popen(['src\\debug\\dolphin_reader.exe'], shell=True, stdout=PIPE, stdin=PIPE)
    print("Got path " + path)
    send_data(p, path)
    #time.sleep(20)
    #send_data(p, -1)


if __name__ == "__main__":
    _thisDir = os.path.dirname(os.path.abspath(__file__))
    os.chdir(_thisDir)
    print(_thisDir)

    paths = [r"..\design_tool\src\data_exp\arc_left_6_5_1.90986.geomsamp",
             r"..\design_tool\src\data_exp\arc_left_6_5_2.86479.geomsamp",
             r"..\design_tool\src\data_exp\arc_left_6_5_4.77465.geomsamp"]

    p = Popen(['src\\debug\\dolphin_reader'], shell=True, stdout=PIPE, stdin=PIPE)
    

    for path in paths:
        send_data(p, path)
    send_data(p, -2)
    
