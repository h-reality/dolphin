<div "text-align: justify">

# DOLPHIN: A Framework for the Design and Perceptual Evaluation of Ultrasound Mid-Air Haptic Stimuli

## What is DOLPHIN

DOLPHIN is an open-source framework which helps in designing mid-air tactile stimuli and provides easy integration for runnning perception experiments with PsychoPy.
It aims to provide faster and more efficient workflows as well as higher reproducibility in the field of ultrasound mid-air haptics.

Users can use the deign tool to create an haptic stimulus based on a geometric shape and a sampling strategy. The user will be presented with a graphical representation of the designed stimulus along with some physical information. The user can then export the stimulus to later be used within the Dolphin framework or in external experiments.

The framework is composed of five main classes:

* The `Geometry` class represents a shape.
* The `SamplingStrategy` class shows how a geometry should be sampled, how the focal point should move and how the intensity of the focal point should change.
* The `Emitter` class represents an haptic interface and deals with the actual emission of the stimulus.
* The `ImportExport` class deals with the import and export of the designed stimulus.
* The  `MainWindow` class implements the stimulus design tool GUI, linking everything together.

DOLPHIN is developed in C++ and uses the Qt framework.

⚠️ At the moment, you can only install Dolphin if you have the Ultrahaptics SDK, which requires a device serial number. This limitation will no longer be in the near future.

## Installation

### Using the source files

* Download the source files by clicking the download button, or clone the repository using `git clone git@gitlab.com:HReality/dolphin.git `.
* Install the MSVC compiler. You can do this by installing Microsoft Visual Studio or by downloading the C++ build tools for Visual Studio : https://visualstudio.microsoft.com/fr/downloads/#build-tools-for-visual-studio-2019
* Download Qt v5.15.2 and Qt Creator here : https://www.qt.io/download. During the installation, install support for the MSVC compiler you installed before.
* Download and install the Ultrahaptics SDK v2.x: https://developer.ultrahaptics.com/downloads/sdk/. At the end of the installation, add it to your path variable.
* Open the project in Qt Creator using **File** > **Open File or Project...** and select the .pro file (`tools/design_tool/src/dolphin_design_tool.pro` or `tools/reader/src/dolphin_reader.pro` depending on the tool you need).
* Edit the .pro files to correctly set the path of the Ultrahaptics library to your installation path (the three lines under `# ULTRAHAPTICS DEPENDENCIES`).
* On the left of the Qt Creator window, click **Projects** and select your MSVC compiler. In the **Build Settings** section, choose the **Debug** configuration and uncheck the **Shadow build** box.
* Click the **Run** button (button with green triangle at the bottom left corner of the Qt Creator Window) to build and run the selected tool.

### Using the executable

**Not available yet**

## Documentation

### Design tool

If you wan to use your haptic interface, make sure it is on and connected **before** you start the design tool.

Most editable values require the user to press enter after being modified.

#### Designing a new stimulus

You can chose a shape using  the shape menu on the left. You can change shape parameters on the right of the menu to change the shape position, dimensions and so on.

On the right you can chose a specific sampling strategy and change its parameters the same way.
* `Step sampling`: The focal point will be rendered at the first sampled point, then it skips `Step` - 1 point and renders the next one, and so on. The sampling is uniform and the amplitude constant.
* `Custom step`: the user can write a series of `n k` lines, meaning that the next `k` points will be separated by a distance equal to `n` times the base step (written in the information on the left, above the shape menu). `k`  needs to be an integer but `n does not`. `k` is optional and is defaulted to 1 if not given. The `skip step` parameter acts as the `step` parameter of `Step sampling`. The amplitude is constant.
* `Custom time step`: the user can write a series of `n k` lines, meaning that the focal point will spend `n` time steps on each of the next `k` points. `n` and `k` have to be  integers. `k` is optional and is defaulted to 1 if not given. The `direction` indicates if the point follows the normal (value > 0) or reversed (value < 0) order. The sampling is uniform and the amplitude modulation follows a sine function at a frequency given by the `frequency` parameter.
This strategy allows to easily create a tactile pointer moving along a shape with a single line `number_of_steps_per_point number_of_points`.
* `Random sampling`: a strategy where the position of the points on the shape and the order of the points are randomized. Click the `New sample` button to get a new randomization. The amplitude is constant.

#### Visualizing the stimulus

You can preview your sampled shape on the right. You can also use the `next`/`previous` buttons at the bottom to make the focal point (shown as a red outlined point) move to the next or previous position. Clicking the `play` button will show the slowed down movement of the point for a full period.

The two graphs on the left show the intensity with respect to time. The top graphs shows the intensity at the reference point (the first point to be rendered) while the bottom one shows the intensity at the current point (shown as a red outlined point).
Alternatively, you can interpret the graphs as the intensity at each point when the focal point is at the current position.

#### Using the stimulus

You can use the `Start/Stop emitting` buttons to make your haptic interface render your designed stimulus.

You can change parameters related to the haptic interface, such as the sample rate and intensity rate on the left, below the two graphs. Be careful as if the parameters do not match your device's capability, the rendered stimulus might not be accurate.

You can use the file menu to export your stimulus for a later use or import another one.
You have several import/export options:
* Sampling strategy: you will keep the geometry while changing the sampling strategy to the imported one. These files have a .SAMP extension.
* Geometry: you will keep the sampling strategy while changing the geometry to the imported one. These files have a .GEOM extension.
* Both: your geometry and sampling strategy will be overridden to be set to the imported one. These files have a .GEOMSAMP extension. This export option also generates a .PTLIST file which stores the position and intensity of the focal point at each time steps. This is useful to use the stimuli in external application without using the DOLPHIN classes.

### Reader tool

To use the stimuli in an external application, you can either use a .PTLIST file to get the list of points and the `Emitter` class to emit it, or you can use the `ImportExport` with the .GEOMSAMP file to recover the sampling strategy and geometry objects.
If your other application is not in C++, you can run the reader program in `tools/reader` and send the .PTLIST file path to the program's stdin stream. You might want to edit the reader to make sure it uses the right emitter type. The file `tools/reader/Pipe_python.py` shows an example of how to send the data to the program in Python.

### File formats

Each file has a CSV-like format with values separated by a space. File data is condensed in one line. A second line may appear as a comment to specify what each value represent.

* .GEOM files: they start with a name representing the type of geometry (Circle, Rectangle, ...), followed by a list of parameters that can be used to re create the geometry object. A second line serves as a comment to indicate the meaning of each value.
* .SAMP files: they start with a name representing the type of sampling strategy (Step_sampling, Custom_step, ...), followed by a list of parameters that can be used to re create the sampling strategy object. A second line serves as a comment to indicate the meaning of each value.
* .GEOMSAMP files: they are a concatenation of the .GEOM file and .SAMP file.
* .PTLIST files: they with the sample rate used for the sampling, followed by the number of time steps followed by a list of `x y z i` corresponding to the position and intensity of the focal point at each time step.


## Authors

For technical questions and support, please contact Lendy Mulot and Thomas Howard. If you want to learn about future developments or contribute to the project in any way, please contact Thomas Howard.

* Lendy Mulot: lendy.mulot@ens-rennes.fr
* Thomas Howard: thomas.howard@irisa.fr
* Guillaume Gicquel: guillaume.gicquel@irisa.fr
* Quentin Zanini: quentin.zanini@ens-rennes.fr
* William Frier: william.frier@ultraleap.com
* Maud Marchal: maud.marchal@irisa.fr
* Claudio Pacchierotti: claudio.pacchierotti@irisa.fr

## Licencing

DOLPHIN is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. DOLPHIN is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with DOLPHIN.  If not, see it [here](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Acknowledgements

* The Qt framework: https://www.qt.io/product/framework - Used for the design tool.
* PsychoPy: https://www.psychopy.org/ - Used to test integration for perception experiments.
* QCustomPlot: https://www.qcustomplot.com/ - Used for the plots in the design tool.
