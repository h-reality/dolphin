﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2020.2.10),
    on juillet 16, 2021, at 23:04
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

from __future__ import absolute_import, division

from psychopy import locale_setup
from psychopy import prefs
from psychopy import sound, gui, visual, core, data, event, logging, clock
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard

import signal
import Pipe_python as pp
from subprocess import Popen, PIPE
from create_cond import generate_conditions
from numpy import floor
import threading

# pipe processus
p = Popen(['..\\tools\\reader\\src\\debug\\dolphin_reader.exe'], shell=False, stdout=PIPE, stdin=PIPE)

blockIndex = 0  # To print the block index

mySound = sound.Sound("pink_noise.wav", volume=0.1)  # Background pink noise

def quitExperiment():
    p.kill()
    core.quit()

event.globalKeys.add(key='q', modifiers=['ctrl', 'alt'], func=quitExperiment)


def emit_test1():
    global timer_test_stimuli
    
    l = int(floor(float(expInfo["hand width"])))
    main_hand = expInfo["main hand"]
    nbPts = 200
    radius1 = 5000
    path = f"stimuli/arc_{main_hand}_{l}_{nbPts}_{radius1}.geomsamp"
    pp.send_data(p, path)
    timer_test_stimuli = threading.Timer(5, emit_test2)
    timer_test_stimuli.start()
    

def emit_test2():
    global timer_test_stimuli
    
    l = int(floor(float(expInfo["hand width"])))
    main_hand = expInfo["main hand"]
    nbPts = 200
    radius2 = round(l / pi, 5)
    path = f"stimuli/arc_{main_hand}_{l}_{nbPts}_{radius2}.geomsamp"
    pp.send_data(p, path)
    timer_test_stimuli = threading.Timer(5, emit_test1)
    timer_test_stimuli.start()

timer_test_stimuli = threading.Timer(0, emit_test1)


# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)

# Store info about the experiment session
psychopyVersion = '2020.2.10'
expName = 'experiment_curvature_discrimination'  # from the Builder filename that created this script
expInfo = {'participant': '', 'session': '001', 'hand width': '', 'hand height': '', 'main hand': 'right', 'first block to do': '0'}
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/%s_%s_%s' % (expInfo['participant'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='C:\\Users\\lendy\\Cours\\ENS\\M1\\Projet_recherche\\dolphin\\experience_curvature\\experiment_curvature_discrimination_lastrun.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file
frameTolerance = 0.001  # how close to onset before 'same' frame

# Start Code - component code to be run after the window creation

# Setup the Window
win = visual.Window(
    size=[1920, 1080], fullscr=False, screen=0, 
    winType='pyglet', allowGUI=True, allowStencil=False,
    monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
    blendMode='avg', useFBO=True, 
    units='height')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard()

# Initialize components for Routine "stimuli_test"
stimuli_testClock = core.Clock()
end_stimuli_test = keyboard.Keyboard()
stimuli_test_txt = visual.TextStim(win=win, name='stimuli_test_txt',
    text='Playing test stimuli\nPress the - key to continue',
    font='Arial',
    pos=(0, 0), height=0.08, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);

# Initialize components for Routine "audio_test"
audio_testClock = core.Clock()
end_audio_test = keyboard.Keyboard()
audio_test_txt = visual.TextStim(win=win, name='audio_test_txt',
    text='Adjust the volume in order not to ear the emitter\nPress the - key to continue',
    font='Arial',
    pos=(0, 0), height=0.08, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);

# Initialize components for Routine "begin_block_routine"
begin_block_routineClock = core.Clock()
Title_block1 = visual.TextStim(win=win, name='Title_block1',
    text='default text',
    font='Arial',
    pos=(0, 0), height=0.08, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
key_resp_title_block1 = keyboard.Keyboard()

# Initialize components for Routine "block_stimuli1"
block_stimuli1Clock = core.Clock()
emit_txt1 = visual.TextStim(win=win, name='emit_txt1',
    text='default text',
    font='Arial',
    pos=(0, 0), height=0.08, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
break_txt = visual.TextStim(win=win, name='break_txt',
    text='Emition stopped',
    font='Arial',
    pos=(0, 0), height=0.08, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);

# Initialize components for Routine "block_stimuli2_and_answer"
block_stimuli2_and_answerClock = core.Clock()
emit_txt2 = visual.TextStim(win=win, name='emit_txt2',
    text='default text',
    font='Arial',
    pos=(0, 0), height=0.08, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
answer_txt = visual.TextStim(win=win, name='answer_txt',
    text='Which stimulus was the most flat?\n(Press 1 or 2 to answer)',
    font='Arial',
    pos=(0, 0), height=0.08, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
key_resp_2 = keyboard.Keyboard()

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "stimuli_test"-------
continueRoutine = True
# update component parameters for each repeat

generate_conditions(int(expInfo["participant"]), int(floor(float(expInfo["hand width"]))), 
                    expInfo["main hand"], int(expInfo["first block to do"]))

timer_test_stimuli.start()  # Start alternating between two stimuli
end_stimuli_test.keys = []
end_stimuli_test.rt = []
_end_stimuli_test_allKeys = []
# keep track of which components have finished
stimuli_testComponents = [end_stimuli_test, stimuli_test_txt]
for thisComponent in stimuli_testComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
stimuli_testClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "stimuli_test"-------
while continueRoutine:
    # get current time
    t = stimuli_testClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=stimuli_testClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *end_stimuli_test* updates
    waitOnFlip = False
    if end_stimuli_test.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        end_stimuli_test.frameNStart = frameN  # exact frame index
        end_stimuli_test.tStart = t  # local t and not account for scr refresh
        end_stimuli_test.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(end_stimuli_test, 'tStartRefresh')  # time at next scr refresh
        end_stimuli_test.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(end_stimuli_test.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(end_stimuli_test.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if end_stimuli_test.status == STARTED and not waitOnFlip:
        theseKeys = end_stimuli_test.getKeys(keyList=['num_subtract'], waitRelease=False)
        _end_stimuli_test_allKeys.extend(theseKeys)
        if len(_end_stimuli_test_allKeys):
            end_stimuli_test.keys = _end_stimuli_test_allKeys[-1].name  # just the last key pressed
            end_stimuli_test.rt = _end_stimuli_test_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # *stimuli_test_txt* updates
    if stimuli_test_txt.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        stimuli_test_txt.frameNStart = frameN  # exact frame index
        stimuli_test_txt.tStart = t  # local t and not account for scr refresh
        stimuli_test_txt.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(stimuli_test_txt, 'tStartRefresh')  # time at next scr refresh
        stimuli_test_txt.setAutoDraw(True)
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in stimuli_testComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "stimuli_test"-------
for thisComponent in stimuli_testComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('stimuli_test_txt.started', stimuli_test_txt.tStartRefresh)
thisExp.addData('stimuli_test_txt.stopped', stimuli_test_txt.tStopRefresh)
# the Routine "stimuli_test" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "audio_test"-------
continueRoutine = True
# update component parameters for each repeat
mySound.play()  # Start the pink noise
end_audio_test.keys = []
end_audio_test.rt = []
_end_audio_test_allKeys = []
# keep track of which components have finished
audio_testComponents = [end_audio_test, audio_test_txt]
for thisComponent in audio_testComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
audio_testClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "audio_test"-------
while continueRoutine:
    # get current time
    t = audio_testClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=audio_testClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *end_audio_test* updates
    waitOnFlip = False
    if end_audio_test.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        end_audio_test.frameNStart = frameN  # exact frame index
        end_audio_test.tStart = t  # local t and not account for scr refresh
        end_audio_test.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(end_audio_test, 'tStartRefresh')  # time at next scr refresh
        end_audio_test.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(end_audio_test.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(end_audio_test.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if end_audio_test.status == STARTED and not waitOnFlip:
        theseKeys = end_audio_test.getKeys(keyList=['num_subtract'], waitRelease=False)
        _end_audio_test_allKeys.extend(theseKeys)
        if len(_end_audio_test_allKeys):
            end_audio_test.keys = _end_audio_test_allKeys[-1].name  # just the last key pressed
            end_audio_test.rt = _end_audio_test_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # *audio_test_txt* updates
    if audio_test_txt.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        audio_test_txt.frameNStart = frameN  # exact frame index
        audio_test_txt.tStart = t  # local t and not account for scr refresh
        audio_test_txt.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(audio_test_txt, 'tStartRefresh')  # time at next scr refresh
        audio_test_txt.setAutoDraw(True)
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in audio_testComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "audio_test"-------
for thisComponent in audio_testComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
mySound.stop()  # Break

timer_test_stimuli.cancel()  # Stop alternating between two stimuli
thisExp.addData('audio_test_txt.started', audio_test_txt.tStartRefresh)
thisExp.addData('audio_test_txt.stopped', audio_test_txt.tStopRefresh)
# the Routine "audio_test" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
blocks = data.TrialHandler(nReps=1, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('condition_files\\choose_block.xlsx'),
    seed=None, name='blocks')
thisExp.addLoop(blocks)  # add the loop to the experiment
thisBlock = blocks.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisBlock.rgb)
if thisBlock != None:
    for paramName in thisBlock:
        exec('{} = thisBlock[paramName]'.format(paramName))

for thisBlock in blocks:
    currentLoop = blocks
    # abbreviate parameter names if possible (e.g. rgb = thisBlock.rgb)
    if thisBlock != None:
        for paramName in thisBlock:
            exec('{} = thisBlock[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "begin_block_routine"-------
    continueRoutine = True
    # update component parameters for each repeat
    blockIndex += 1
    print(f"block index: {blockIndex}")
    
    mySound.stop()  # Break
    
    thisExp.saveAsWideText(filename+'.csv', delim='auto') # Saving the data
    Title_block1.setText(f"Ready for block {blockIndex} ?\nPress the - key to continue")
    key_resp_title_block1.keys = []
    key_resp_title_block1.rt = []
    _key_resp_title_block1_allKeys = []
    # keep track of which components have finished
    begin_block_routineComponents = [Title_block1, key_resp_title_block1]
    for thisComponent in begin_block_routineComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    begin_block_routineClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "begin_block_routine"-------
    while continueRoutine:
        # get current time
        t = begin_block_routineClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=begin_block_routineClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *Title_block1* updates
        if Title_block1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            Title_block1.frameNStart = frameN  # exact frame index
            Title_block1.tStart = t  # local t and not account for scr refresh
            Title_block1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(Title_block1, 'tStartRefresh')  # time at next scr refresh
            Title_block1.setAutoDraw(True)
        
        # *key_resp_title_block1* updates
        waitOnFlip = False
        if key_resp_title_block1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            key_resp_title_block1.frameNStart = frameN  # exact frame index
            key_resp_title_block1.tStart = t  # local t and not account for scr refresh
            key_resp_title_block1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(key_resp_title_block1, 'tStartRefresh')  # time at next scr refresh
            key_resp_title_block1.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(key_resp_title_block1.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(key_resp_title_block1.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if key_resp_title_block1.status == STARTED and not waitOnFlip:
            theseKeys = key_resp_title_block1.getKeys(keyList=['num_subtract'], waitRelease=False)
            _key_resp_title_block1_allKeys.extend(theseKeys)
            if len(_key_resp_title_block1_allKeys):
                key_resp_title_block1.keys = _key_resp_title_block1_allKeys[-1].name  # just the last key pressed
                key_resp_title_block1.rt = _key_resp_title_block1_allKeys[-1].rt
                # a response ends the routine
                continueRoutine = False
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in begin_block_routineComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "begin_block_routine"-------
    for thisComponent in begin_block_routineComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    mySound.play()  # Start the pink noise
    blocks.addData('Title_block1.started', Title_block1.tStartRefresh)
    blocks.addData('Title_block1.stopped', Title_block1.tStopRefresh)
    # the Routine "begin_block_routine" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    trials = data.TrialHandler(nReps=1, method='sequential', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions(block_cond),
        seed=None, name='trials')
    thisExp.addLoop(trials)  # add the loop to the experiment
    thisTrial = trials.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
    if thisTrial != None:
        for paramName in thisTrial:
            exec('{} = thisTrial[paramName]'.format(paramName))
    
    for thisTrial in trials:
        currentLoop = trials
        # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
        if thisTrial != None:
            for paramName in thisTrial:
                exec('{} = thisTrial[paramName]'.format(paramName))
        
        # ------Prepare to start Routine "block_stimuli1"-------
        continueRoutine = True
        routineTimer.add(3.500000)
        # update component parameters for each repeat
        pp.send_data(p, stimulus1)  # Send the stimuli to the pipe, which will be sent to the emitter
        emit_txt1.setText('Emitting stimulus 1')
        # keep track of which components have finished
        block_stimuli1Components = [emit_txt1, break_txt]
        for thisComponent in block_stimuli1Components:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        block_stimuli1Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
        frameN = -1
        
        # -------Run Routine "block_stimuli1"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = block_stimuli1Clock.getTime()
            tThisFlip = win.getFutureFlipTime(clock=block_stimuli1Clock)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *emit_txt1* updates
            if emit_txt1.status == NOT_STARTED and tThisFlip >= 0.1-frameTolerance:
                # keep track of start time/frame for later
                emit_txt1.frameNStart = frameN  # exact frame index
                emit_txt1.tStart = t  # local t and not account for scr refresh
                emit_txt1.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(emit_txt1, 'tStartRefresh')  # time at next scr refresh
                emit_txt1.setAutoDraw(True)
            if emit_txt1.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > emit_txt1.tStartRefresh + 2.0-frameTolerance:
                    # keep track of stop time/frame for later
                    emit_txt1.tStop = t  # not accounting for scr refresh
                    emit_txt1.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(emit_txt1, 'tStopRefresh')  # time at next scr refresh
                    emit_txt1.setAutoDraw(False)
            
            # *break_txt* updates
            if break_txt.status == NOT_STARTED and tThisFlip >= 2.1-frameTolerance:
                # keep track of start time/frame for later
                break_txt.frameNStart = frameN  # exact frame index
                break_txt.tStart = t  # local t and not account for scr refresh
                break_txt.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(break_txt, 'tStartRefresh')  # time at next scr refresh
                break_txt.setAutoDraw(True)
            if break_txt.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > break_txt.tStartRefresh + 1.4-frameTolerance:
                    # keep track of stop time/frame for later
                    break_txt.tStop = t  # not accounting for scr refresh
                    break_txt.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(break_txt, 'tStopRefresh')  # time at next scr refresh
                    break_txt.setAutoDraw(False)
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in block_stimuli1Components:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "block_stimuli1"-------
        for thisComponent in block_stimuli1Components:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        trials.addData('emit_txt1.started', emit_txt1.tStartRefresh)
        trials.addData('emit_txt1.stopped', emit_txt1.tStopRefresh)
        trials.addData('break_txt.started', break_txt.tStartRefresh)
        trials.addData('break_txt.stopped', break_txt.tStopRefresh)
        
        # ------Prepare to start Routine "block_stimuli2_and_answer"-------
        continueRoutine = True
        # update component parameters for each repeat
        pp.send_data(p, stimulus2)  # Send the second stimuli
        emit_txt2.setText('Emitting stimulus 2')
        key_resp_2.keys = []
        key_resp_2.rt = []
        _key_resp_2_allKeys = []
        # keep track of which components have finished
        block_stimuli2_and_answerComponents = [emit_txt2, answer_txt, key_resp_2]
        for thisComponent in block_stimuli2_and_answerComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        block_stimuli2_and_answerClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
        frameN = -1
        
        # -------Run Routine "block_stimuli2_and_answer"-------
        while continueRoutine:
            # get current time
            t = block_stimuli2_and_answerClock.getTime()
            tThisFlip = win.getFutureFlipTime(clock=block_stimuli2_and_answerClock)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *emit_txt2* updates
            if emit_txt2.status == NOT_STARTED and tThisFlip >= 0.1-frameTolerance:
                # keep track of start time/frame for later
                emit_txt2.frameNStart = frameN  # exact frame index
                emit_txt2.tStart = t  # local t and not account for scr refresh
                emit_txt2.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(emit_txt2, 'tStartRefresh')  # time at next scr refresh
                emit_txt2.setAutoDraw(True)
            if emit_txt2.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > emit_txt2.tStartRefresh + 2.0-frameTolerance:
                    # keep track of stop time/frame for later
                    emit_txt2.tStop = t  # not accounting for scr refresh
                    emit_txt2.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(emit_txt2, 'tStopRefresh')  # time at next scr refresh
                    emit_txt2.setAutoDraw(False)
            
            # *answer_txt* updates
            if answer_txt.status == NOT_STARTED and tThisFlip >= 2.1-frameTolerance:
                # keep track of start time/frame for later
                answer_txt.frameNStart = frameN  # exact frame index
                answer_txt.tStart = t  # local t and not account for scr refresh
                answer_txt.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(answer_txt, 'tStartRefresh')  # time at next scr refresh
                answer_txt.setAutoDraw(True)
            
            # *key_resp_2* updates
            waitOnFlip = False
            if key_resp_2.status == NOT_STARTED and tThisFlip >= 2.1-frameTolerance:
                # keep track of start time/frame for later
                key_resp_2.frameNStart = frameN  # exact frame index
                key_resp_2.tStart = t  # local t and not account for scr refresh
                key_resp_2.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(key_resp_2, 'tStartRefresh')  # time at next scr refresh
                key_resp_2.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(key_resp_2.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(key_resp_2.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if key_resp_2.status == STARTED and not waitOnFlip:
                theseKeys = key_resp_2.getKeys(keyList=['1', '2'], waitRelease=False)
                _key_resp_2_allKeys.extend(theseKeys)
                if len(_key_resp_2_allKeys):
                    key_resp_2.keys = _key_resp_2_allKeys[-1].name  # just the last key pressed
                    key_resp_2.rt = _key_resp_2_allKeys[-1].rt
                    # was this correct?
                    if (key_resp_2.keys == str(bigger_radius)) or (key_resp_2.keys == bigger_radius):
                        key_resp_2.corr = 1
                    else:
                        key_resp_2.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in block_stimuli2_and_answerComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "block_stimuli2_and_answer"-------
        for thisComponent in block_stimuli2_and_answerComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        trials.addData('emit_txt2.started', emit_txt2.tStartRefresh)
        trials.addData('emit_txt2.stopped', emit_txt2.tStopRefresh)
        trials.addData('answer_txt.started', answer_txt.tStartRefresh)
        trials.addData('answer_txt.stopped', answer_txt.tStopRefresh)
        # check responses
        if key_resp_2.keys in ['', [], None]:  # No response was made
            key_resp_2.keys = None
            # was no response the correct answer?!
            if str(bigger_radius).lower() == 'none':
               key_resp_2.corr = 1;  # correct non-response
            else:
               key_resp_2.corr = 0;  # failed to respond (incorrectly)
        # store data for trials (TrialHandler)
        trials.addData('key_resp_2.keys',key_resp_2.keys)
        trials.addData('key_resp_2.corr', key_resp_2.corr)
        if key_resp_2.keys != None:  # we had a response
            trials.addData('key_resp_2.rt', key_resp_2.rt)
        trials.addData('key_resp_2.started', key_resp_2.tStartRefresh)
        trials.addData('key_resp_2.stopped', key_resp_2.tStopRefresh)
        # the Routine "block_stimuli2_and_answer" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
    # completed 1 repeats of 'trials'
    
# completed 1 repeats of 'blocks'


# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv', delim='auto')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
