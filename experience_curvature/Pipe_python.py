from subprocess import Popen, PIPE
import os


def send_data(p, a):
    p.stdout.flush()
    p.stdin.flush()
    data = str(a)
    value = data + '\n'
    value = bytes(value, 'UTF-8')  # Needed in Python 3.
    p.stdin.write(value)
    p.stdin.flush()
    result = p.stdout.readline().strip().decode()
    print("Received: ", result)


def main(path):
    _thisDir = os.path.dirname(os.path.abspath(__file__))
    os.chdir(_thisDir)
    p = Popen(['..\\tools\\reader\\src\\debug\\dolphin_reader.exe'], shell=True, stdout=PIPE, stdin=PIPE)
    print("Got path " + path)
    send_data(p, path)


if __name__ == "__main__":
    # Testing the pipe
    _thisDir = os.path.dirname(os.path.abspath(__file__))
    os.chdir(_thisDir)
    print(_thisDir)
    
    paths = [r"stimuli\arc_left_6_10_1.90986.ptlist",
             r"stimuli\arc_left_6_10_2.86479.ptlist",
             r"stimuli\arc_left_6_10_4.77465.ptlist"]

    p = Popen(['..\\tools\\reader\\src\\debug\\dolphin_reader.exe'], shell=True, stdout=PIPE, stdin=PIPE)
    for path in paths:
        send_data(p, path)
    send_data(p, -2)  # End

