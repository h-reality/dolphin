
from numpy import pi, floor
from numpy.random import permutation
from sys import argv
import xlsxwriter


def generate_conditions(participant, l, main_hand, first_block_index):

    radiuses = [l / pi, 3 * l / (2 * pi), 5 * l / (2 * pi), 5000]
    ids = ['a', 'b', 'c', 'd']
    
    for i in range(4):
        radiuses[i] = round(radiuses[i], 5)
    
    # Randomizing the order of the blocks
    latin_square_blocks = [ [1, 2, 5, 3, 4],
                            [5, 4, 1, 3, 2],
                            [3, 4, 2, 5, 1],
                            [2, 1, 3, 5, 4],
                            [5, 1, 4, 2, 3]]
    
    base_pts = [10, 50, 200, 400, 800]
    pts = []
    line = latin_square_blocks[participant % 5]
    for index in line:
        pts.append(base_pts[index - 1])
    """    
    latin_square_paires =   [
                            [1, 2, 12, 3, 11, 4, 10, 5, 9, 6, 8, 7],
                            [2, 3, 1, 4, 12, 5, 11, 6, 10, 7, 9, 8],
                            [3, 4, 2, 5, 1, 6, 12, 7, 11, 8, 10, 9],
                            [4, 5, 3, 6, 2, 7, 1, 8, 12, 9, 11, 10],
                            [5, 6, 4, 7, 3, 8, 2, 9, 1, 10, 12, 11],
                            [6, 7, 5, 8, 4, 9, 3, 10, 2, 11, 1, 12],
                            [7, 8, 6, 9, 5, 10, 4, 11, 3, 12, 2, 1],
                            [8, 9, 7, 10, 6, 11, 5, 12, 4, 1, 3, 2],
                            [9, 10, 8, 11, 7, 12, 6, 1, 5, 2, 4, 3],
                            [10, 11, 9, 12, 8, 1, 7, 2, 6, 3, 5, 4],
                            [11, 12, 10, 1, 9, 2, 8, 3, 7, 4, 6, 5],
                            [12, 1, 11, 2, 10, 3, 9, 4, 8, 5, 7, 6]
                            ]
    """
    
    pair_indices = ["abcd"[i] + "abcd"[j] for i in range(4) for j in range(i + 1, 4)]
    
    workbook = xlsxwriter.Workbook("condition_files/choose_block.xlsx")
    worksheet = workbook.add_worksheet()
    
    worksheet.write(0, 0, "block_cond")
    for i in range(first_block_index, 5):
        path = f"condition_files/block_{pts[i]}_pts.xlsx"
        worksheet.write(i - first_block_index + 1, 0, path)
    workbook.close()
    
    
    header = [  "stimulus1", "length1_cm", "radius1_cm", "id1", 
                "stimulus2", "length2_cm", "radius2_cm", "id2",
                "bigger_radius", "id_pair"]
    
    for k in range(first_block_index, len(pts)):
        pt = pts[k]
        # Randomizing the order of the pairs
        #line_index = (participant * 5 + k) % 12
        
        path = f"condition_files/block_{pt}_pts.xlsx"
        workbook = xlsxwriter.Workbook(path)
        worksheet = workbook.add_worksheet()
        
        # Writing the header
        for i in range(len(header)):
            worksheet.write(0, i, header[i])
        
        stimuli = []
        for r in radiuses:
            s = f"stimuli/arc_{main_hand}_{l}_{pt}_{r}.geomsamp"
            stimuli.append(s)
        
        pairs_repeat1 = permutation(12)
        pairs_repeat2 = permutation(12)
        pairs_repeat3 = permutation(12)
        all_pairs = list(pairs_repeat1) + list(pairs_repeat2) + list(pairs_repeat3)
        
        row = 1
        for pair_index in all_pairs:
            i = pair_index // 3
            val = pair_index % 3
            if val < i:
                j = val
            else:
                j = val + 1
                
            stimulus1 = stimuli[i]
            radius1 = radiuses[i]
            id1 = ids[i]
        
            stimulus2 = stimuli[j]
            radius2 = radiuses[j]
            id2 = ids[j]
            
            bigger_radius = 1 if radius1 > radius2 else 2
            pair_id = f"{id1}{id2}{l}"
            
            data = [stimulus1, l, radius1, id1,
                    stimulus2, l, radius2, id2,
                    bigger_radius, pair_id]
            
            # Writing the data
            for k in range(len(data)):
                worksheet.write(row, k, data[k])
            row += 1
                
        workbook.close()


if __name__ == "__main__":
    # participant id: first parameter
    participant = 0

    # hand size: second parameters
    l = 8
    
    # main hand: third parameter, 1 for right, -1 for left
    main_hand = "right"
    
    # index of the first block to use, useful in case of crash during the experiment
    first_block_index = 0
    
    try:
        if len(argv) >= 2:
            participant = int(argv[1])
        if len(argv) >= 3:
            l = int(floor(float(argv[2])))
        if len(argv) >= 4:
            main_hand = argv[3]
        if len(argv) >= 5:
            first_block_index = int(argv[4])
    except:
        print("Usage: python create_cond.py [participant_id] [hand_width] [main_hand (1: right, -1: left)] [first_block_index]")
        exit()
    generate_conditions(participant, l, main_hand, first_block_index)
