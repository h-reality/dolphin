/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#include "polygon.h"

using namespace Geometries;
Polygon::Polygon()
{

}

Polygon::Polygon(std::vector<Vector2> points, Ultrahaptics::Vector3 ref_point, float rot_x, float rot_y, float rot_z)
{
    tag = 1;
    cycle = true;
    rot_center = ref_point;
    r_x = rot_x * M_PI / 180.0f;
    r_y = rot_y * M_PI / 180.0f;
    r_z = rot_z * M_PI / 180.0f;
    points2D = points;

    size_t size = points.size();
    points2D.resize(size + 1, points.at(0));

    std::vector<Ultrahaptics::Vector3> temp(size);

    Ultrahaptics::Vector3 x(1, 0, 0);
    Ultrahaptics::Vector3 y(0, 1, 0);
    Ultrahaptics::Vector3 z(0, 0, 1);
    x = rotate(x + rot_center) - rot_center;
    y = rotate(y + rot_center) - rot_center;
    z = rotate(z + rot_center) - rot_center;

    mat = Ultrahaptics::Matrix3x3(x.x, y.x, z.x, x.y, y.y, z.y, x.z, y.z, z.z);

    //we convert the Vector2 into Vector3
    for (size_t i = 0 ; i < size ; ++i)
    {
        temp.at(i) = rotate(Ultrahaptics::Vector3(points.at(i).x, points.at(i).y, 0) + ref_point);
    }

    line = std::vector<Segment>(size, Segment());
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size - 1 ; ++i)
    {
        line.at(i) = Segment(temp.at(i), temp.at(i + 1));
        cut.at(i + 1) = cut.at(i) + line.at(i).length();
    };
    line.at(size - 1) = Segment(temp.at(size - 1), temp.at(0));
    cut.at(size) = cut.at(size - 1) + line.at(size - 1).length();

}

Polygon::Polygon(std::vector<Vector2> points, Ultrahaptics::Vector3 pivot, Ultrahaptics::Vector3 ref_point, Ultrahaptics::Vector3 rot_axis, float rot)
{
    tag = 1;
    cycle = true;
    rot_center = pivot + rot_vect(ref_point - pivot, rot, rot_axis);
    r_x = 0.0f;
    r_y = 0.0f;
    r_z = 0.0f;
    points2D = points;

    size_t size = points.size();
    points2D.resize(size + 1, points.at(0));

    std::vector<Ultrahaptics::Vector3> temp(size);

    Ultrahaptics::Vector3 x(1, 0, 0);
    Ultrahaptics::Vector3 y(0, 1, 0);
    Ultrahaptics::Vector3 z(0, 0, 1);
    x = rot_vect(x, rot , rot_axis);
    y = rot_vect(y, rot , rot_axis);
    z = rot_vect(z, rot , rot_axis);

    mat = Ultrahaptics::Matrix3x3(x.x, y.x, z.x, x.y, y.y, z.y, x.z, y.z, z.z);
    //we convert the Vector2 into Vector3
    for (size_t i = 0 ; i < size ; ++i)
    {
        temp.at(i) = pivot + rot_vect(Ultrahaptics::Vector3(points.at(i).x, points.at(i).y, 0) + ref_point - pivot, rot , rot_axis);
    }

    line = std::vector<Segment>(size, Segment());
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size - 1 ; ++i)
    {
        line.at(i) = Segment(temp.at(i), temp.at(i + 1));
        cut.at(i + 1) = cut.at(i) + line.at(i).length();
    };
    line.at(size - 1) = Segment(temp.at(size - 1), temp.at(0));
    cut.at(size) = cut.at(size - 1) + line.at(size - 1).length();


}


//check if the half line starting from the point toward the left intersect the segment [seg1, seg2]
bool intersect(Vector2 pt, Vector2 seg1, Vector2 seg2)
{
    //the point is above or under the segment
    if (pt.y < std::min(seg1.y, seg2.y) ||  pt.y > std::max(seg1.y, seg2.y) ||  pt.x > std::max(seg1.x, seg2.x))
    {
        return false;
    }
    else if (pt.x < std::min(seg1.x, seg2.x)) return true;
    else
    {
        if ( std::abs(seg1.x - seg2.x) < 0.000001 || std::abs(pt.x - std::min(seg1.x, seg2.x)) < 0.000001) return true;
        if (std::abs(std::max(seg1.x, seg2.x) - pt.x) < 0.000001) return false;
        float temp1 = (seg2.y - seg1.y) / (seg2.x - seg1.x);
        float temp2;
        if (seg1.x < seg2.x) {temp2 = (pt.y - seg1.y) / (pt.x - seg1.x);}
        else {temp2 = (pt.y - seg2.y) / (pt.x - seg2.x);}
        if (std::abs(temp1) < std::abs(temp2))
        {
            return true;
        }
        return false;

    }
}

bool Polygon::is_inside(Vector2 point)
{
    int count = 0;
    for (size_t i = 0 ; i < points2D.size() - 1 ; ++i)
    {
        if (intersect(point, points2D.at(i), points2D.at(i + 1)))
        {
            ++count;
        }
    }

    return count % 2 == 1;
}
