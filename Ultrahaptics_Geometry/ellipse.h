/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef ELLIPSE_H
#define ELLIPSE_H

#include "geometry.h"

namespace Geometries {
class Ellipse : public Geometry
{
private:
    Ultrahaptics::Vector3 center;
    float angle;

protected:
    float rx;
    float ry;

public:
    Ellipse();
    //rx must be greater than ry
    Ellipse(Ultrahaptics::Vector3 center, float rx, float ry, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;

    float getRx();
    float getRy();

    double perimeter() override;
};

class Circle : public Ellipse
{
public:
    Circle();
    Circle(Ultrahaptics::Vector3 center, float radius, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
    double perimeter() override;
};
};

#endif // ELLIPSE_H
