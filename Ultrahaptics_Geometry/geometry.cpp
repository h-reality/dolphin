/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#include "geometry.h"

#ifndef M_PI
#define M_PI 3.14159265358979323
#endif

using namespace Geometries;
bool Geometry::is_cycle()
{
    return cycle;
}

int Geometry::get_tag()
{
    return tag;
}

Ultrahaptics::Vector3 Geometry::rotate(Ultrahaptics::Vector3 point)
{
    return rotation(point, Ultrahaptics::Vector3(r_x, r_y, r_z), rot_center);
}

Ultrahaptics::Vector3 Geometry::get_center() {
    return rot_center;
}

double Geometry::perimeter() {
    return 0.f;
}
