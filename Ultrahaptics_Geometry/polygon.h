/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef POLYGON_H
#define POLYGON_H

#include "geometry.h"
#include "polylines.h"
#include "utils.h"
#include "algorithm"

namespace Geometries {
struct Vector2
{
public:
    float x, y;
    Vector2() {x = 0.0f; y = 0.0f;}
    Vector2(float x, float y) : x(x), y(y) {}
    Vector2 add(Vector2 vect) {return Vector2(x + vect.x, y + vect.y);}
    Vector2 mult(float t) {return Vector2( t * x, t * y);}
    float length() {return std::sqrt( x * x + y * y);}
};


class Polygon : public ClosedPolylines
{
protected:
    std::vector<Vector2> points2D;
    Ultrahaptics::Matrix3x3 mat;
public:
    Polygon();
    Polygon(std::vector<Vector2> points, Ultrahaptics::Vector3 ref_point = Ultrahaptics::Vector3(0.0f, 0.0f, 20.0f), float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
    Polygon(std::vector<Vector2> points, Ultrahaptics::Vector3 pivot, Ultrahaptics::Vector3 ref_point = Ultrahaptics::Vector3(0.0f, 0.0f, 20.0f), Ultrahaptics::Vector3 rot_axis = Ultrahaptics::Vector3(1, 0, 0), float rot = 0.0f);
    bool is_inside(Vector2 point);
    std::vector<Vector2> get_pts2() {return points2D;}
    Ultrahaptics::Vector3 get_ref_point() {return rot_center;}
    std::vector<float> get_rot() {return std::vector<float>({r_x, r_y, r_z});}
    Ultrahaptics::Matrix3x3 get_mat() {return mat;}
};
}
#endif // POLYGON_H
