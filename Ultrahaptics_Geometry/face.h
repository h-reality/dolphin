#ifndef FACE_H
#define FACE_H

#include "utils.h"
#include "geometry.h"
#include "polygon.h"

#include <float.h>
namespace Geometries {
class Face : public Polygon
{
private:
    Vector2 summit;
    Vector2 base1;
    Vector2 base2;
    static const Ultrahaptics::Vector3 OUT;
    Ultrahaptics::Vector3 convert(Ultrahaptics::Vector3 point);
public:
    Face();
    Face(std::vector<Vector2> points, Ultrahaptics::Vector3 ref_point = Ultrahaptics::Vector3(0.0f, 0.0f, 20.0f), float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
    Face(Polygon* poly);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
    bool is_out(Ultrahaptics::Vector3 point);
    float get_square_area();
};

class Polyfaces : public Geometry
{
protected:
    std::vector<Face*> faces;
    std::vector<float> cut;
public:
    Polyfaces();
    Polyfaces(std::vector<Face*> list_face);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
};
}
#endif // FACE_H
