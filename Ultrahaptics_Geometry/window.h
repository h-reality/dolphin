#ifndef WINDOW_H
#define WINDOW_H

#include "Ultrahaptics.hpp"
#include <vector>
#include <utility>
#include "utils.h"

class Window
{
public:
    virtual bool is_inside(Ultrahaptics::Vector3 point) =0;
};

class PlaneWindow : public Window
{
private:
    std::pair<Ultrahaptics::Vector3, Ultrahaptics::Vector3> plane;
public:
    PlaneWindow();
    PlaneWindow(Ultrahaptics::Vector3 point, Ultrahaptics::Vector3 normal);
    bool is_inside(Ultrahaptics::Vector3 point) override;
};

class CurveWindow : public Window
{
private:
    bool (*lim)(Ultrahaptics::Vector3);
public:
    CurveWindow(bool (*limit)(Ultrahaptics::Vector3));
    bool is_inside(Ultrahaptics::Vector3 point) override;
};

class ParaboloidWindow : public Window
{
private:
    float p;
    float q;
    bool inside;
    Ultrahaptics::Vector3 center;
    Ultrahaptics::Vector3 rot;
public:
    ParaboloidWindow(Ultrahaptics::Vector3 center, float p = 1.0f, float q = 1.0f, bool inside = true, float rx = 0.0f, float ry = 0.0f, float rz = 0.0f);
    bool is_inside(Ultrahaptics::Vector3 point);
};

#endif // WINDOW_H
