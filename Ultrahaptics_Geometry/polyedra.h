#ifndef POLYEDRA_H
#define POLYEDRA_H

#include "face.h"
#include "triangle.h"
#include "Ultrahaptics.hpp"
#include <algorithm>

namespace Geometries {
class Polyedra : public Polyfaces
{
public:
    Polyedra();
};

class Tetrahedron : public Polyedra
{
public:
    Tetrahedron(Ultrahaptics::Vector3 center, float radius, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
};

class Cube : public Polyedra
{
public:
    Cube(Ultrahaptics::Vector3 center, float radius, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);

};


class Octahedron : public Polyedra
{
public:
    Octahedron(Ultrahaptics::Vector3 center, float radius, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);

};


class Dodecahedron : public Polyedra
{
public:
    Dodecahedron(Ultrahaptics::Vector3 center, float radius, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
};


class Icosahedron : public Polyedra
{
public:
    Icosahedron(Ultrahaptics::Vector3 center, float radius, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
};
}

#endif // POLYEDRA_H
