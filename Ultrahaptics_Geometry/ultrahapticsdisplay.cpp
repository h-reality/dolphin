#include "ultrahapticsdisplay.h"

using Seconds = std::chrono::duration<float>;
static auto start_time = std::chrono::steady_clock::now();

// Callback function for filling out complete device output states through time
void my_emitter_callback(const Ultrahaptics::TimePointStreaming::Emitter &timepoint_emitter,
                         Ultrahaptics::TimePointStreaming::OutputInterval &interval,
                         const Ultrahaptics::HostTimePoint &submission_deadline,
                         void *user_pointer)
{
    //qDebug("PointPair : Ultrahaptics my_emitter_callback() called");

    // Cast the user pointer to the struct that describes the control point behaviour
    UltrahapticsGeometry *current_point = static_cast<UltrahapticsGeometry*>(user_pointer);

    // Loop through the samples in this interval
    for (auto& sample : interval)
    {
        // Calculate the sample time
        Seconds t = sample - start_time;
        //const Ultrahaptics::Vector3 position = current_point->evaluate_at_next();
        // Set the position and intensity of the persistent control point to that of the modulated wave at this point in time.
        sample.persistentControlPoint(0).setPosition(current_point -> evaluate_at_next());
        sample.persistentControlPoint(0).setIntensity(current_point -> get_intensity());
    }
}

UltrahapticsDisplay::UltrahapticsDisplay(UltrahapticsGeometry* uhg)
{
    qDebug("UltrahapticsDisplay : constructor called");

    // Set the maximum number of simultaneous control points
    m_emitter.setMaximumControlPointCount(1);
    // Setting the sample rate after the max control point count so it doesnt revert to default 40kHz
    m_emitter.setSampleRate(200);
    qDebug("PointPair : constructor : Ultrahaptics device serial # %s", qPrintable(m_emitter.getDeviceInfo().getSerialNumber()));
    qDebug("PointPair : constructor : Ultrahaptics device firmware version %s", qPrintable(m_emitter.getDeviceInfo().getFirmwareVersion()));
    qDebug("PointPair : constructor : Ultrahaptics device ID %s", qPrintable(m_emitter.getDeviceInfo().getDeviceIdentifier()));
    qDebug("PointPair : constructor : Ultrahaptics device sample rate %s", qPrintable(QString::number(m_emitter.getSampleRate())));
    qDebug("PointPair : constructor : Ultrahaptics device control point count %s", qPrintable(QString::number(m_emitter.getMaximumControlPointCount())));

    // Set the emission callback
    m_uh_geometry = uhg;
    m_uh_geometry->set_hardware_sample_rate(m_emitter.getSampleRate());
    m_emitter.setEmissionCallback(my_emitter_callback, m_uh_geometry);
}

UltrahapticsDisplay::~UltrahapticsDisplay()
{
    delete m_uh_geometry;
}

void UltrahapticsDisplay::play()
{
    m_emitter.start();
}

void UltrahapticsDisplay::stop()
{
    m_emitter.stop();
}
