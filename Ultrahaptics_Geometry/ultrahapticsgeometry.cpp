#include "ultrahapticsgeometry.h"


//The unit is the centimeter.

//////////////////////////
//    Constructors      //
//////////////////////////

UltrahapticsGeometry::UltrahapticsGeometry()
{
    previous_trans = 0.0f;
    m_next_idx = 0;
    movement = Movement();
    intensity = Intensity();
    mvt_speed = 1.0f;
    wind_ind = 0;
}

UltrahapticsGeometry::UltrahapticsGeometry(Geometry* geo,
                                           size_t nb_points,
                                           float speed,
                                           unsigned int hsr,
                                           std::vector<std::vector<Window*>> window_list,
                                           Intensity intensity, float mvt_speed)
    : windows(window_list), intensity(intensity) , mvt_speed(mvt_speed)
{
    previous_trans = 0.0f;
    m_next_idx = 0;
    wind_ind = 0;
    movement = Movement();
    //make sure that m_point_cloud is an empty vector
    m_point_cloud = std::vector<Ultrahaptics::Vector3>();
    add_geometry(geo, nb_points);
    UltrahapticsGeometry::initialize_timepoint_cloud(speed, hsr);
}


//////////////////////////
//      Destructor      //
//////////////////////////

UltrahapticsGeometry::~UltrahapticsGeometry()
//not sure if it works
{
    m_point_cloud.~vector();
    m_timepoint_cloud.~vector();
    m_next_idx.~size_t();
}


//////////////////////////
//   Update functions   //
//////////////////////////


Ultrahaptics::Vector3 change_unit(Ultrahaptics::Vector3 point)
{
    return Ultrahaptics::Vector3(point.x * Ultrahaptics::Units::centimetres,
                                 point.y * Ultrahaptics::Units::centimetres,
                                 point.z * Ultrahaptics::Units::centimetres);
}


// Sets the hardware sample rate (needed for temporal sampling)
void UltrahapticsGeometry::set_hardware_sample_rate(unsigned int samplerate)
{
    m_hardware_sample_rate = samplerate;
    movement.set_sample_rate(samplerate);
}


//add new points corresponding to a new geometry after the current points in m_point_cloud
void UltrahapticsGeometry::add_geometry(Geometry* geo, size_t nb_points)
{
    //initialize a temporary vector with the right size
    std::vector<Ultrahaptics::Vector3> temp;    
    float step;
    if (nb_points == 1)
    {
        step = 1.;
    }
    //if the geometric figure is closed, we avoid to have to points in the sames coordinates
    else if (geo -> is_cycle())
    {
        step = 1.0f /nb_points;
    }
    //case when the figure s not closed
    else
    {
        step = 1.0f / (nb_points - 1);
    }
    std::clog << geo->get_tag() << std::endl;
    //we assign the points depending of the dimension of the figure
    switch (geo->get_tag())
    {
        //1D curve
        case 1:
        {
            temp = std::vector<Ultrahaptics::Vector3>(nb_points, Ultrahaptics::Vector3(0,0,0));
            for (size_t i = 0 ; i < nb_points ; ++i)
            {
                temp.at(i) = geo -> evaluate_at(i * step);
                //std::clog << temp.at(i) << std::endl; //<- for the tests
            }
            break;
        }
        //parametric surface
        case 2:
        {
            temp = std::vector<Ultrahaptics::Vector3>(nb_points * nb_points, Ultrahaptics::Vector3(0,0,0));
            for (size_t i = 0 ; i < nb_points ; ++i)
            {
                for (size_t j = 0 ; j < nb_points ; ++j)
                {
                    temp.at(nb_points * i + j) = geo -> evaluate_at((i * step), j * step);
                    //std::clog << temp.at(nb_points * i + j) << std::endl; //<- for the tests                                                        ///////////////////////////////////////////
                }
            }
            break;
        }
        //polygonal surface
        case 3:
        {
            temp = std::vector<Ultrahaptics::Vector3>(nb_points * (nb_points + 1));
            for (size_t i = 0 ; i < nb_points ; ++i)
            {
                temp.at(i) = geo -> evaluate_at(i * step);
            }
            for (size_t i = 0 ; i < nb_points ; ++i)
            {
                for (size_t j = 0 ; j < nb_points ; ++j)
                {
                    temp.at(nb_points * (i + 1) + j) = geo -> evaluate_at((i * step), j * step, 1.0f);
                    //std::clog << (nb_points * (i + 1) + j) << std::endl; //<- for the tests                                                        ///////////////////////////////////////////
                }
            }
            break;
        }
    }

    m_point_cloud.reserve(m_point_cloud.size() + temp.size());
    m_point_cloud.insert(m_point_cloud.end(), temp.begin(), temp.end());
}


//make the vector m_timepoint_cloud with the current state of m_point_cloud with the speed given as a parameter
void UltrahapticsGeometry::initialize_timepoint_cloud(float speed)
{
    initialize_windowpoint();
    size_t size = m_windowpoint_cloud.size();
    float pos = 0;
    m_timepoint_cloud = std::vector<Ultrahaptics::Vector3>(size, Ultrahaptics::Vector3(0,0,0));
    for (size_t i = 0; i < size; ++i)
    {
        //assgin the closest point for the position
        if (pos - std::floor(pos) < 0.5)
        {
            m_timepoint_cloud.at(i) = change_unit(m_windowpoint_cloud.at(std::floor(pos)));
        }
        else
        {
            m_timepoint_cloud.at(i) = change_unit(m_windowpoint_cloud.at(std::ceil(pos)));
        }
        //std::clog << m_timepoint_cloud.at(i) << std::endl;                                                                    ///////////////////////////////////
        //move pos on the figure
        pos += speed / m_hardware_sample_rate;
        //make sure pos don't leave the figure
        if (pos >= size)
        {
            pos = pos - std::floor(pos /size) * size;
        };
    };
}


//make the vector m_timepoint_cloud with the current state of m_point_cloud with the speed and the hardware sample rate given as a parameter
void UltrahapticsGeometry::initialize_timepoint_cloud(float speed, unsigned int hsr)
{
    set_hardware_sample_rate(hsr);
    initialize_timepoint_cloud(speed);
}

void UltrahapticsGeometry::add_new_window(Window* window, size_t ind)
{
    if (ind < wind_ind)
    {
        windows.at(ind).resize(windows.at(ind).size() + 1, window);
    }
    else
    {
        windows.resize(windows.size() + 1,  std::vector<Window*>(1, window));
        wind_ind += 1;
    }
}

void UltrahapticsGeometry::set_rotation(Ultrahaptics::Vector3 center, float speed_x, float speed_y, float speed_z)
{
    movement.set_rotation(center, speed_x, speed_y, speed_z);
}

void UltrahapticsGeometry::set_translation(Ultrahaptics::Vector3 (*trans)(Ultrahaptics::Vector3, float))
{
    movement.set_translation(trans);
}


//check if a point is inside the list of window (return true if (is_inside(any window of windows.at(0)) and (is_inside(any window of windows.at(1)) and ...)
bool UltrahapticsGeometry::is_inside(Ultrahaptics::Vector3 point)
{
    for (size_t i = 0 ; i < windows.size() ; ++i)
    {
        for(size_t j = 0; j < windows.at(i).size() ; ++j)
        {
            if (windows.at(i).at(j)->is_inside(point))
            {
                break;
            }
            //if the point isn't in any window of window.at(i), false is returned
            if (j == windows.at(i).size() - 1)
            {
                return false;
            }
        }
    }
    return true;
}


//build the m_windowpoint_cloud vector
void UltrahapticsGeometry::initialize_windowpoint()
{
    size_t size = m_point_cloud.size();
    m_windowpoint_cloud = std::vector<Ultrahaptics::Vector3>(size, Ultrahaptics::Vector3(0,0,0));
    size_t ind = 0;
    for (size_t i = 0 ; i < size ; ++i)
    {
        if (is_inside(m_point_cloud.at(i)))
        {
            m_windowpoint_cloud.at(ind) = m_point_cloud.at(i);
            ++ind;
        };
    };
    m_windowpoint_cloud.resize(ind);
}


//////////////////////////
//   Return functions   //
//////////////////////////


// Returns the Ultrahaptics focal point coordinates for the next time sample
Ultrahaptics::Vector3 UltrahapticsGeometry::evaluate_at_next()
{
    //Ultrahaptics::Vector3 temp =  m_timepoint_cloud.at(m_next_idx);
    //m_next_idx = (m_next_idx + 1) % m_timepoint_cloud.size();

    //take care of the translation
    float phi = previous_trans;
    previous_trans += mvt_speed / m_hardware_sample_rate;
    previous_trans -= std::floor(previous_trans);

    return movement.translate(movement.rotate(m_timepoint_cloud.at(m_next_idx)), phi);
}

float UltrahapticsGeometry::get_intensity()
{
    float temp =  intensity.get_intensity(m_next_idx);
    m_next_idx = (m_next_idx + 1) % m_timepoint_cloud.size();
    return temp;
}

//////////////////////////
//  .obj import/export  //
//////////////////////////

//export the sampling in the .obj format
void UltrahapticsGeometry::export_obj(std::string filename, std::string path)
{
    std::ofstream file;
    file.open(path + filename);
    if (!file.is_open())
    {
        std::cout << "error in creation" << std::endl;
    }
    else
    {
        file << "# ultrahaptics library OBJ File: ' '" << std::endl << std::endl;
        for (size_t i = 0; i < m_windowpoint_cloud.size() ; ++i)
        {
            file << "v " << m_windowpoint_cloud.at(i).x << " " << m_windowpoint_cloud.at(i).y << " " << m_windowpoint_cloud.at(i).z << std::endl;
        }
        file.close();
    }
}

//import an already sampled figure (in format .obj)
//this is inspired by the C++ .obj parser found here: http://www.cplusplus.com/forum/windows/89066/, written by raphael1
void UltrahapticsGeometry::import_obj(std::string filename)
{
    std::ifstream file;
    file.open(filename);
    std::vector<std::string*>lines;
    if (!file.is_open())
    {
        std::cout << "error : file has not be opened" << std::endl;
    }
    else
    {
        char buf[256]; //create buffer to read line
        while (!file.eof())
        {
            file.getline(buf,256);
            lines.push_back(new std::string(buf));
        }
        for (size_t i = 0 ; i < lines.size() ; ++i)
        {
            if (lines.at(i)->c_str()[0] == 'v' && lines.at(i)->c_str()[1] == ' ')
            {
                char tmp;
                float tmp_x,tmp_y,tmp_z;
                sscanf_s(lines.at(i)->c_str(),"v %f %f %f",&tmp_x,&tmp_y,&tmp_z); //read in the 3
                m_point_cloud.push_back(Ultrahaptics::Vector3(tmp_x, tmp_y, tmp_z));
            }
        }
        //Delete lines to avoid memory leaks
        for(size_t i = 0; i < lines.size();i++)
        delete lines.at(i);
    }
    file.close();
}
