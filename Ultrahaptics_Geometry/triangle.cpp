/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#include "triangle.h"

using namespace Geometries;
//////////////////////////
//    Basic triangles   //
//////////////////////////

Triangle::Triangle()
    : Triangle::Triangle(Ultrahaptics::Vector3(0.0f, 1.0f, 20.0f),
                         Ultrahaptics::Vector3(-1.0f, 0.0f, 20.0f),
                         Ultrahaptics::Vector3(1.0f, 1.0f, 20.0f)) {}

Triangle::Triangle(Ultrahaptics::Vector3 point1, Ultrahaptics::Vector3 point2, Ultrahaptics::Vector3 point3)
{
    rot_center = point1;
    cycle = true;

    points2D = std::vector<Vector2>(4);
    points2D.at(0) = Vector2(0, 0);
    points2D.at(3) = Vector2(0, 0);

    //we define the base
    Ultrahaptics::Vector3 x = (point2 - point1) / (point2 - point1).length();
    Ultrahaptics::Vector3 y = (point3 - point1) / (point3 - point1).length();
    Ultrahaptics::Vector3 z = x.cross(y);
    z = z / z.length();
    y = z.cross(x);

    mat = Ultrahaptics::Matrix3x3(x.x, y.x, z.x, x.y, y.y, z.y, x.z, y.z, z.z);
    Ultrahaptics::Matrix3x3 inv  = mat.inverse();

    //we define the 2D points
    Ultrahaptics::Vector3 pt_tmp = chgt_base(point2 - point1, inv);
    points2D.at(1) = Vector2(pt_tmp.x, pt_tmp.y);
    pt_tmp = chgt_base(point3 - point1, inv);
    points2D.at(2) = Vector2(pt_tmp.x, pt_tmp.y);

    //we fill the cut array
    std::vector<Ultrahaptics::Vector3> list_points({point1, point2, point3});
    size_t size = list_points.size();
    line = std::vector<Segment>(size, Segment());
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size - 1 ; ++i)
    {
        line.at(i) = Segment(list_points.at(i), list_points.at(i + 1));
        cut.at(i + 1) = cut.at(i) + line.at(i).length();
    };
    line.at(size - 1) = Segment(list_points.at(size - 1), list_points.at(0));
    cut.at(size) = cut.at(size - 1) + line.at(size - 1).length();
}


//////////////////////////
// Rectangles triangles //
//////////////////////////

RectangleTriangle::RectangleTriangle()
    : RectangleTriangle::RectangleTriangle(Ultrahaptics::Vector3(0.0f, 0.0f, 20.0f), 1.0f, 1.0f) {}


RectangleTriangle::RectangleTriangle(Ultrahaptics::Vector3 right_angle, float first_side_length, float second_side_length, float rot_x, float rot_y, float rot_z)
    : Triangle::Triangle(right_angle,
                         right_angle + Ultrahaptics::Vector3(first_side_length, 0.0f, 0.0f),
                         right_angle + Ultrahaptics::Vector3(0.0f, second_side_length, 0.0f))
{
    //the point invariant to the rotation is the middle of the height of the triangle
    rot_center = right_angle + 0.5f * (Ultrahaptics::Vector3(first_side_length, 0.0f, 0.0f) + Ultrahaptics::Vector3(0.0f, second_side_length, 0.0f));
    r_x = rot_x;
    r_y = rot_y;
    r_z = rot_z;
}


//////////////////////////
//  Isoceles triangles  //
//////////////////////////

IsocelesTriangle::IsocelesTriangle()
    : IsocelesTriangle::IsocelesTriangle(Ultrahaptics::Vector3(1.0f, 0.0f, 20.0f), 1.0f, 30.0f, 0.0f) {}

IsocelesTriangle::IsocelesTriangle(Ultrahaptics::Vector3 summit, float length, float teta, float rot_x, float rot_y, float rot_z)
{
    tag = 1;
    cycle = true;
    //the point invariant to the rotation is the middle of the height of the triangle
    rot_center = summit + (length / 2) * Ultrahaptics::Vector3(0.0f, -cos(teta / 2), 0.0f);
    r_x = rot_x * M_PI / 180.0f;
    r_y = rot_y * M_PI / 180.0f;
    r_z = rot_z * M_PI / 180.0f;

    Ultrahaptics::Vector3 x(1, 0, 0);
    Ultrahaptics::Vector3 y(0, 1, 0);
    Ultrahaptics::Vector3 z(0, 0, 1);
    x = rotate(x + rot_center) - rot_center;
    y = rotate(y + rot_center) - rot_center;
    z = rotate(z + rot_center) - rot_center;

    mat = Ultrahaptics::Matrix3x3(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);

    //creation of the two remaining points
    Ultrahaptics::Vector3 point1(- sin(teta / 2), -cos(teta / 2), 0.0f);
    Ultrahaptics::Vector3 point2(sin(teta / 2), -cos(teta / 2), 0.0f);

    std::vector<Ultrahaptics::Vector3> list_points({summit,summit + length * point1,summit + length * point2});
    size_t size = list_points.size();
    line = std::vector<Segment>(size, Segment());
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size - 1 ; ++i)
    {
        line.at(i) = Segment(list_points.at(i), list_points.at(i + 1));
        cut.at(i + 1) = cut.at(i) + line.at(i).length();
    };
    line.at(size - 1) = Segment(list_points.at(size - 1), list_points.at(0));
    cut.at(size) = cut.at(size - 1) + line.at(size - 1).length();

}

//////////////////////////
// Equilateral triangles//
//////////////////////////

EquilateralTriangle::EquilateralTriangle()
    : RegularPolygon::RegularPolygon(Ultrahaptics::Vector3(0.0f, 0.0f, 20.0f), 3) {}

EquilateralTriangle::EquilateralTriangle(Ultrahaptics::Vector3 center, float side_length, float rot_x, float rot_y, float rot_z)
    : RegularPolygon::RegularPolygon(center, 3, side_length, rot_x, rot_y, rot_z) {}
