#include <iostream>
#include "geometry.h"
#include "ellipse.h"
#include "rectangles.h"
#include "parametricfigure.h"
#include "polylines.h"
#include "polygon.h"
#include "face.h"
#include "polyedra.h"
#include "triangle.h"
#include "regularpolygon.h"
#include "window.h"
#include "ultrahapticsdisplay.h"
#include "ultrahapticsgeometry.h"
#include "spline.h"
#include "splinesurface.h"

using namespace std;

void test_geo(Geometry* geo)
{
    for (int i = 0; i < 20; i++)
    {
        cout << geo->evaluate_at(i / 20.) << endl;
    };
}

void test_ug(Geometry* geo, size_t size, float speed, unsigned int hrs)
{
    UltrahapticsGeometry ug(geo, size, speed, hrs);
    cout << endl;
    for(size_t i = 0; i < size; i++)
    {
        cout << ug.evaluate_at_next() << endl;
    };
}

void test()
{
    float cx = 0.0f;
    float cy = 0.0f;
    float cz = 0.0f;
    Ultrahaptics::Vector3 center(cx, cy, cz);
    float rx = 4.0f;
    float ry = 15.0f;
    Ellipse e(center, rx, ry);
    Circle c(center, 10,0,90);

    B_lemniscate b(center, ry);

    PlaneWindow win(Ultrahaptics::Vector3(-1, 0.0f, 20), Ultrahaptics::Vector3(1.0f, 0.0f, 0.0f));
    PlaneWindow win2(Ultrahaptics::Vector3(1, 0.0f, 20), Ultrahaptics::Vector3(-1.0f, 0.0f, 0.0f));
    ParaboloidWindow winp(Ultrahaptics::Vector3(0.0f, 0.0f, -8.0f), 1, 1, true);
    ParaboloidWindow winp2(Ultrahaptics::Vector3(-8.0f, 0.0f, 0.0f), 1, 1, false, -50, 130, 0);
    unsigned int hrs = 200;
    float speed = hrs * 7.5f;
    size_t size = 100;

    /*float x1 = -10.0f;
    float y1 = 0.0f;
    float z1 = 20.0f;

    float x2 = 10.0f;
    float y2 = 0.0f;
    float z2 = 20.0f;

    float x3 = -10.0f;
    float y3 = -10.0f;
    float z3 = 20.0f;

    float x4 = -10.0f;
    float y4 = 10.0f;
    float z4 = 20.0f;

    float x5 = 8.0f;
    float y5 = 8.0f;
    float z5 = 20.0f;

    float x6 = 10.0f;
    float y6 = -10.0f;
    float z6 = 20.0f;

    float x7 = -15.0f;
    float y7 = 5.0f;
    float z7 = 10.0f;

    float x8 = -5.0f;
    float y8 = 5.0f;
    float z8 = 20.0f;

    float x9 = 5.0f;
    float y9 = 5.0f;
    float z9 = 20.0f;

    float x10 = 15.0f;
    float y10 = 5.0f;
    float z10 = 10.0f;

    float x11 = -15.0f;
    float y11 = -5.0f;
    float z11 = 10.0f;

    float x12 = -5.0f;
    float y12 = -5.0f;
    float z12 = 20.0f;

    float x13 = 5.0f;
    float y13 = -5.0f;
    float z13 = 20.0f;

    float x14 = 15.0f;
    float y14 = -5.0f;
    float z14 = 10.0f;

    Ultrahaptics::Vector3 p1(1, 4, 0);
    Ultrahaptics::Vector3 p2(0.5, 6, 0);
    Ultrahaptics::Vector3 p3(5, 4, 0);
    Ultrahaptics::Vector3 p4(3, 12, 0);
    Ultrahaptics::Vector3 p5(11, 14, 0);
    Ultrahaptics::Vector3 p6(8, 4, 0);
    Ultrahaptics::Vector3 p7(12, 3, 0);
    Ultrahaptics::Vector3 p8(11, 9, 0);
    Ultrahaptics::Vector3 p9(15, 10, 0);
    Ultrahaptics::Vector3 p10(17, 8, 0);
    Ultrahaptics::Vector3 p11(0, 1, 0);
    Ultrahaptics::Vector3 p12(2, 7, 0);
    Ultrahaptics::Vector3 p13(6, 3, 0);
    Ultrahaptics::Vector3 p14(10, 2, 0);
    Ultrahaptics::Vector3 p15(x14, y14, z14);
    Ultrahaptics::Vector3 p16(x14, y14, z14);
    Ultrahaptics::Vector3 p17(x14, y14, z14);

    vector<Ultrahaptics::Vector3> pts({p11, p12, p13, p14});
    vector<float> nodes({0, 0, 0, 0, 1/8., 2/8., 3/8., 4/8., 5/8., 6/8., 1, 1, 1, 1});
    //BSpline btest(pts, nodes);
    Spline1D spline(pts, 4);

    vector<Ultrahaptics::Vector3> vect4({p7, p11});
    vector<Ultrahaptics::Vector3> vect5({p8, p12});
    vector<Ultrahaptics::Vector3> vect6({p9, p13});
    vector<Ultrahaptics::Vector3> vect7({p10, p14});

    vector<Ultrahaptics::Vector3> vect8({p7, p8, p9, p10});
    vector<Ultrahaptics::Vector3> vect9({p11, p12, p13, p14});

    vector<vector<Ultrahaptics::Vector3>> test_surface({vect4, vect5, vect6, vect7});
    vector<vector<Ultrahaptics::Vector3>> test_surface2({vect8, vect9});
    BesierSurface bs(test_surface2);

    Segment seg1(p1, p2);
    Segment seg2(p3, p4);
    Segment seg3(p4, p6);

    vector<Segment> vect1({seg1, seg2, seg3});
    vector<Ultrahaptics::Vector3> vect2({p1, p3, p5});
    vector<Ultrahaptics::Vector3> vect3({p3, p4, p5, p6});

    BesierCurve bc(vect3);
    BSpline besier(vect3, vector<float>({0, 0, 0, 0, 1, 1, 1, 1}));
    BSpline bsp(vect3, nodes);
    PolyLines poly1(vect1);*/
    Tetrahedron tet(center, 10);
    Square sq(center, 20.0f / std::sqrt(3));
    RegularPolygon pent(center, 5, (10 / std::sqrt(3) / std::sin(M_PI * 54.0f / 180)));
    Segment segment(Ultrahaptics::Vector3(-10,0,0) + center, Ultrahaptics::Vector3(10,0,0) + center);

    Cube cube(center, 10);
    Face face(&sq);
    Face penta(&pent);
    Dodecahedron dodec(center, 10);


    Octahedron octa(center, 10);
    Icosahedron iso(center, 10);
    /*
    */
    /*test_geo(&c);
    cout << endl << endl;
    test_geo(&e);
    cout << endl << endl;
    test_geo(&b);
    cout << endl << endl;
    test_ug(&c, size, speed, hrs);*/
    std::clog << "coucou" << std::endl;
    Face tri(new Triangle(Ultrahaptics::Vector3(2,1,5), Ultrahaptics::Vector3(7,10,7), Ultrahaptics::Vector3(3, -1,-2)));
    UltrahapticsGeometry uhg(&iso, 500, speed, hrs);
    UltrahapticsGeometry u2;
    //u2.import_obj("C:/Users/qzanini/Desktop/win_test.obj");
    //.add_new_window(&winp);
    //u2.add_new_window(&winp2, 0);
    //u2.add_new_window(&win2);
    //u2.add_new_window(&win);
    //u2.initialize_windowpoint();
    //u2.export_obj("window.obj", "C:/Users/qzanini/Desktop/");
    uhg.export_obj("test.obj", "C:/Users/qzanini/Desktop/");
    //uhg.set_rotation(center, 0, 0, 90); //to be changed to a z_axis only rotation
    //UltrahapticsDisplay disp(&uhg);
    //disp.play();
    std::cout << "Hit ENTER to quit..." << std::endl;
        std::string line;
        std::getline(std::cin, line);
     //disp.stop();

}
