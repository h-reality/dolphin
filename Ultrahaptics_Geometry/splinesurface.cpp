#include "splinesurface.h"

using namespace Geometries;
//////////////////////////
//    Surface splines   //
//////////////////////////

SplineSurface::SplineSurface() {}
/*
SplineSurface::SplineSurface(std::vector<std::vector<Ultrahaptics::Vector3>> points, int spline_order)
{
    order = 3;
    point_list = points;


    //calcul of the coeficients for an order 3 spline
}

Ultrahaptics::Vector3 SplineSurface::evaluate_at(float phi, float psi, float teta)
{
    return Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f);
}*/

//////////////////////////
//    Besier surfaces   //
//////////////////////////

BesierSurface::BesierSurface(std::vector<std::vector<Ultrahaptics::Vector3>> points)
{
    tag = 2;
    cycle = false;
    order = points.size() - 1;
    point_list = points;
    n = points.size() - 1;
    m = points.at(0).size() - 1;
    list_coeff = std::vector<std::vector<float>>(2, binomial(n));
    list_coeff.at(1) = binomial(m);


}

Ultrahaptics::Vector3 BesierSurface::evaluate_at(float phi, float psi, float teta)
{
    Ultrahaptics::Vector3 res(0.0f, 0.0f, 0.0f);
    float temp;
    for (size_t i = 0 ; i <= n ; ++i)
    {
        temp = list_coeff.at(0).at(i) * exp_rep(1 - phi, n - i) * exp_rep(phi, i);
        for (size_t j = 0; j <= m ; ++j)
        {
            res += temp * list_coeff.at(1).at(j) * exp_rep(1 - psi, m - j) * exp_rep(psi, j) * point_list.at(i).at(j);
        }
    };
    return res;
}


//////////////////////////
//      B_splines3D     //
//////////////////////////

//in comment is the code for regular B_spline (may be implemented)

BSpline3D::BSpline3D(std::vector<std::vector<Ultrahaptics::Vector3>> points, std::vector<float> nodes1, std::vector<float> nodes2)
{
    tag = 2;
    cycle = false;
    point_list = points;
    node_list1 = nodes1;
    node_list2 = nodes2;
    //we must have k1 > n and k2 > m
    k1 = nodes1.size() - 1;
    k2 = nodes2.size() - 1;
    n = points.size();
    m = points.at(0).size();
    length_line = nodes1.at(n) - nodes1.at(k1 - n);
    length_col = nodes1.at(m) - nodes1.at(k2 - m);
}

Ultrahaptics::Vector3 BSpline3D::evaluate_at(float phi, float psi, float teta)
{
    Ultrahaptics::Vector3 res(0.0f, 0.0f, 0.0f);
    float u = node_list1.at(k1 - n) + phi / length_line;
    float v = node_list2.at(k2 - m) + psi / length_col;
    std::vector<float> coeffs_1 = cox_de_boor(node_list1, k1 - n, u);
    std::vector<float> coeffs_2 = cox_de_boor(node_list2, k2 - m, v);
    for (size_t i = 0 ; i < n ; ++i)
    {
        for (size_t j = 0 ; j < m ; ++j)
        {
            res += coeffs_1.at(i) * coeffs_2.at(j) *  point_list.at(i).at(j);
        }
    }
    return res;
}

//////////////////////////
//        NURBS3D       //
//////////////////////////

Nurbs3D::Nurbs3D(std::vector<std::vector<Ultrahaptics::Vector3>> points, std::vector<float> nodes1, std::vector<float> nodes2, std::vector<std::vector<float>> weights)
    : BSpline3D::BSpline3D(points, nodes1, nodes2), w(weights) {}

Ultrahaptics::Vector3 Nurbs3D::evaluate_at(float phi, float psi, float teta)
{
    Ultrahaptics::Vector3 res(0.0f, 0.0f, 0.0f);
    std::vector<float> coeffs_1 = cox_de_boor(node_list1, k1 - n + 1, phi);
    std::vector<float> coeffs_2 = cox_de_boor(node_list2, k2 - m + 1, psi);
    float tot = 0.0f;
    for (size_t i = 0 ; i <= n ; ++i)
    {
        for (size_t j = 0 ; j <= m ; ++j)
        {
            res += coeffs_1.at(i) * coeffs_2.at(j) * w.at(i).at(j) * point_list.at(i).at(j);
            tot += coeffs_1.at(i) * coeffs_2.at(j) * w.at(i).at(j);
        }
    }
    return (1.0f / tot) * res;

}
