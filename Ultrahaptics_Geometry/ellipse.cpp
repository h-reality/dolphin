/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#include "ellipse.h"

using namespace Geometries;
Ellipse::Ellipse()
    : Ellipse(Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f), 2.0f, 1.0f) {}

Ellipse::Ellipse(Ultrahaptics::Vector3 center, float rx, float ry, float rot_x, float rot_y, float rot_z)
    : center(center), rx(rx), ry(ry)
{
    tag = 1;
    rot_center = center;
    r_x = rot_x * M_PI / 180.0f;
    r_y = rot_y * M_PI / 180.0f;
    r_z = rot_z * M_PI / 180.0f;

    cycle = true;
};

//give a point depending of the phi parameter (0 <= phi <= 1)
Ultrahaptics::Vector3 Ellipse::evaluate_at(float phi, float psi, float teta)
{
    float angle_phi = phi * 2 * M_PI;
    Ultrahaptics::Vector3 point(
                center.x + rx * cos(angle_phi),
                center.y + ry * sin(angle_phi),
                center.z + 0);
    return rotate(point);
};

float Ellipse::getRx() {
    return rx;
}

float Ellipse::getRy() {
    return ry;
}

double Ellipse::perimeter() {
    // This is Ramanujan's approximation
    // source: https://www.mathsisfun.com/geometry/ellipse-perimeter.html
    double h = pow(rx - ry, 2.f) / pow(rx + ry, 2.f);
    return M_PI * (rx + ry) * (1.f + 3.f * h / (10.f + sqrt(4.f - 3.f * h)));
}

Circle::Circle()
    : Circle::Circle(Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f), 1.0f) {}

Circle::Circle(Ultrahaptics::Vector3 center, float radius, float rot_x, float rot_y, float rot_z)
    : Ellipse(center, radius, radius, rot_x, rot_y, rot_z) {}

double Circle::perimeter() {
    return 2.f * M_PI * rx;
}
