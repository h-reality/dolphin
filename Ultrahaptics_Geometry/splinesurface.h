#ifndef SPLINESURFACE_H
#define SPLINESURFACE_H

#include <vector>
#include "geometry.h"
#include "utils.h"

namespace Geometries {
class SplineSurface : public Geometry
{
protected:
    int order;
    std::vector<std::vector<Ultrahaptics::Vector3>> point_list;
    std::vector<std::vector<float>> list_coeff;

public:
    SplineSurface();
};

class BesierSurface : public SplineSurface
{
private:
    size_t n;
    size_t m;
public:
    BesierSurface(std::vector<std::vector<Ultrahaptics::Vector3>> points);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
};

class BSpline3D : public SplineSurface
{
protected:
    size_t k1;
    size_t k2;
    size_t n;
    size_t m;
    float length_line;
    float length_col;
    std::vector<float> node_list1;
    std::vector<float> node_list2;
public:
    BSpline3D(std::vector<std::vector<Ultrahaptics::Vector3>> points, std::vector<float> nodes1, std::vector<float> nodes2);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
};

class Nurbs3D : public BSpline3D
{
private:
    std::vector<std::vector<float>> w;
public:
    Nurbs3D(std::vector<std::vector<Ultrahaptics::Vector3>> points, std::vector<float> nodes1, std::vector<float> nodes2, std::vector<std::vector<float>> weights);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
};
}

#endif // SPLINESURFACE_H
