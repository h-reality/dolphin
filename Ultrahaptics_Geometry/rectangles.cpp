/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#include "rectangles.h"

using namespace Geometries;
Rectangle::Rectangle()
    : Rectangle::Rectangle(Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f), 2.0f, 1.0f) {}


Rectangle::Rectangle(Ultrahaptics::Vector3 center, float length, float width,  float rot_x, float rot_y, float rot_z)
{
    tag = 1;
    cycle = true;
    rot_center = center;
    r_x = rot_x * M_PI / 180.0f;
    r_y = rot_y * M_PI / 180.0f;
    r_z = rot_z * M_PI / 180.0f;
    this->length = length;
    this->width = width;

    //we define the four points of the rectangle
    Ultrahaptics::Vector3 p1(center.x - length / 2, center.y + width / 2, center.z);
    Ultrahaptics::Vector3 p2(center.x + length / 2, center.y + width / 2, center.z);
    Ultrahaptics::Vector3 p3(center.x + length / 2, center.y - width / 2, center.z);
    Ultrahaptics::Vector3 p4(center.x - length / 2, center.y - width / 2, center.z);
    std::vector<Ultrahaptics::Vector3> list_points({p1, p2, p3, p4});

    points2D = std::vector<Vector2>({Vector2(p1.x, p1.y), Vector2(p2.x, p2.y), Vector2(p3.x, p3.y), Vector2(p4.x, p4.y), Vector2(p1.x, p1.y)});

    size_t size = 4;
    line = std::vector<Segment>(size, Segment());
    cut = std::vector<float>(size + 1, 0.0f);
    line = std::vector<Segment>(size, Segment());
    cut = std::vector<float>(size + 1, 0.0f);

    Ultrahaptics::Vector3 x(1, 0, 0);
    Ultrahaptics::Vector3 y(0, 1, 0);
    Ultrahaptics::Vector3 z(0, 0, 1);
    x = rotate(x + rot_center) - rot_center;
    y = rotate(y + rot_center) - rot_center;
    z = rotate(z + rot_center) - rot_center;

    mat = Ultrahaptics::Matrix3x3(x.x, y.x, z.x, x.y, y.y, z.y, x.z, y.z, z.z);

    for (size_t i = 0 ; i < size - 1 ; ++i)
    {
        line.at(i) = Segment(list_points.at(i), list_points.at(i + 1));
        cut.at(i + 1) = cut.at(i) + line.at(i).length();
    };
    line.at(size - 1) = Segment(list_points.at(size - 1), list_points.at(0));
    cut.at(size) = cut.at(size - 1) + line.at(size - 1).length();
}


float Rectangle::get_length() {
    return length;
}

float Rectangle::get_width() {
    return width;
}

double Rectangle::perimeter() {
    return 2.f * length + 2.f * width;
}

Square::Square()
    : Rectangle::Rectangle(Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f), 1.0f, 1.0f) {}

Square::Square(Ultrahaptics::Vector3 center, float side, float rot_x, float rot_y, float rot_z)
    : Rectangle::Rectangle(center, side, side, rot_x, rot_y, rot_z) {}
