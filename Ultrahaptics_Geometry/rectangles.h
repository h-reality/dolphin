/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef RECTANGLES_H
#define RECTANGLES_H

#include "geometry.h"
#include "polylines.h"
#include "polygon.h"


namespace Geometries {
class Rectangle : public Polygon
{
public:
    Rectangle();
    Rectangle(Ultrahaptics::Vector3 center, float length, float width, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
    //Ultrahaptics::Vector3 evaluate_at(float phi) override;

    float get_length();
    float get_width();
    double perimeter() override;

protected:
    float length, width;
};


class Square : public Rectangle
{
public:
    Square();
    Square(Ultrahaptics::Vector3 center, float side, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
};
}
#endif // RECTANGLES_H
