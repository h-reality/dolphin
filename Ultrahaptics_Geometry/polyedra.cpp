#include "polyedra.h"

using namespace Geometries;
Polyedra::Polyedra()
{

}

Tetrahedron::Tetrahedron(Ultrahaptics::Vector3 center, float radius, float rot_x, float rot_y, float rot_z)
{
    std::clog << "1" << " ";
    tag = 3;
    cycle = false;
    rot_center = center;
    r_x = rot_x;
    r_y = rot_y;
    r_z = rot_z;

    //we create the 2D points
    std::vector<Ultrahaptics::Vector3> pts(4);
    float temp = radius / std::sqrt(3);

    pts.at(0) = center + Ultrahaptics::Vector3(-temp, temp, temp);
    pts.at(1) = center + Ultrahaptics::Vector3(temp, -temp, temp);
    pts.at(2) = center + Ultrahaptics::Vector3(temp, temp, -temp);
    pts.at(3) = center + Ultrahaptics::Vector3(-temp, -temp, -temp);

    std::clog << "2" << " ";
    faces = std::vector<Face*>(4);
    //we create the faces
    faces.at(0) = new Face(new Triangle(pts.at(0), pts.at(1), pts.at(2)));
    std::clog << "3" << " ";
    faces.at(1) = new Face(new Triangle(pts.at(0), pts.at(1), pts.at(3)));
    std::clog << "4" << " ";
    faces.at(2) = new Face(new Triangle(pts.at(0), pts.at(2), pts.at(3)));
    std::clog << "5" << " ";
    faces.at(3) = new Face(new Triangle(pts.at(1), pts.at(2), pts.at(3)));
    std::clog << "6" << " ";

    size_t size = faces.size();
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size ; ++i)
    {
        cut.at(i + 1) = cut.at(i) + faces.at(i)->get_square_area();
    };
}

Cube::Cube(Ultrahaptics::Vector3 center, float radius, float rot_x, float rot_y, float rot_z)
{
    tag = 3;
    cycle = false;
    rot_center = center;
    r_x = rot_x;
    r_y = rot_y;
    r_z = rot_z;

    //we create the 2D points
    std::vector<Vector2> pts(4);
    float temp = radius / std::sqrt(3);

    /*pts.at(0) = Vector2(center.x - std::sqrt(2) / 2, center.y - temp);
    pts.at(1) = Vector2(center.x + std::sqrt(2) / 2, center.y - temp);
    pts.at(2) = Vector2(center.x + std::sqrt(2) / 2, center.y + temp);
    pts.at(3) = Vector2(center.x - std::sqrt(2) / 2, center.y + temp);*/

    faces = std::vector<Face*> (6);
    //we create the faces
    faces.at(0) = new Face(new RegularPolygon(center + Ultrahaptics::Vector3(0, 0, temp), 4, 2 * temp, 0, 0, 45));
    faces.at(1) = new Face(new RegularPolygon(center - Ultrahaptics::Vector3(0, 0, temp), 4, 2 * temp, 0, 0, 45));
    faces.at(2) = new Face(new RegularPolygon(center + Ultrahaptics::Vector3(0, temp, 0), 4, 2 * temp, 90, 45));
    faces.at(3) = new Face(new RegularPolygon(center - Ultrahaptics::Vector3(0, temp, 0), 4, 2 * temp, 90, 45));
    faces.at(4) = new Face(new RegularPolygon(center + Ultrahaptics::Vector3(temp, 0, 0), 4, 2 * temp, 90, 45, 90));
    faces.at(5) = new Face(new RegularPolygon(center - Ultrahaptics::Vector3(temp, 0, 0), 4, 2 * temp, 90, 45, 90));

    size_t size = faces.size();
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size ; ++i)
    {
        cut.at(i + 1) = cut.at(i) + faces.at(i)->get_square_area();
    };
}

Octahedron::Octahedron(Ultrahaptics::Vector3 center, float radius, float rot_x, float rot_y, float rot_z)
{
    tag = 3;
    cycle = false;
    rot_center = center;
    r_x = rot_x;
    r_y = rot_y;
    r_z = rot_z;

    //we create the 2D points
    std::vector<Ultrahaptics::Vector3> pts(6);

    pts.at(0) = center + Ultrahaptics::Vector3(radius, 0, 0);
    pts.at(1) = center - Ultrahaptics::Vector3(radius, 0, 0);
    pts.at(2) = center + Ultrahaptics::Vector3(0, radius, 0);
    pts.at(3) = center - Ultrahaptics::Vector3(0, radius, 0);
    pts.at(4) = center + Ultrahaptics::Vector3(0, 0, radius);
    pts.at(5) = center - Ultrahaptics::Vector3(0, 0, radius);

    faces = std::vector<Face*>(8);
    //we create the faces
    faces.at(0) = new Face(new Triangle(pts.at(0), pts.at(2), pts.at(4)));
    faces.at(1) = new Face(new Triangle(pts.at(0), pts.at(2), pts.at(5)));
    faces.at(2) = new Face(new Triangle(pts.at(0), pts.at(3), pts.at(4)));
    faces.at(3) = new Face(new Triangle(pts.at(0), pts.at(3), pts.at(5)));
    faces.at(4) = new Face(new Triangle(pts.at(1), pts.at(2), pts.at(4)));
    faces.at(5) = new Face(new Triangle(pts.at(1), pts.at(2), pts.at(5)));
    faces.at(6) = new Face(new Triangle(pts.at(1), pts.at(3), pts.at(4)));
    faces.at(7) = new Face(new Triangle(pts.at(1), pts.at(3), pts.at(5)));



    size_t size = faces.size();
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size ; ++i)
    {
        cut.at(i + 1) = cut.at(i) + faces.at(i)->get_square_area();
    };

}

Dodecahedron::Dodecahedron(Ultrahaptics::Vector3 center, float radius, float rot_x, float rot_y, float rot_z)
{
    tag = 3;
    cycle = false;
    rot_center = center;
    r_x = rot_x;
    r_y = rot_y;
    r_z = rot_z;

    float teta = 2 * M_PI / 5;
    //
    float length = (radius / std::sqrt(3) / std::sin(M_PI * 54.0f / 180));
    float rad = (length /(2 * std::sin(teta/2)));
    float center_pos = std::sqrt(radius * radius - rad * rad);
    //we create the 2D points (centers of the penagons)
    std::vector<Ultrahaptics::Vector3> pts(2);
    std::vector<Ultrahaptics::Vector3> penta1(6);
    std::vector<Ultrahaptics::Vector3> penta2(6);



    pts.at(0) = center + Ultrahaptics::Vector3(0, 0, center_pos);
    pts.at(1) = center - Ultrahaptics::Vector3(0, 0, center_pos);
    for  (size_t i = 0 ; i < 5 ; ++i)
    {
        penta1.at(i) = pts.at(0) + rad * Ultrahaptics::Vector3(std::cos(M_PI / 2 - i * teta),
                                                                  std::sin(M_PI / 2 - i * teta),
                                                                  0.0f);

        penta2.at(i) = pts.at(1) + rad * Ultrahaptics::Vector3(std::cos(M_PI / 2 - i * teta + M_PI/5),
                                                                  std::sin(M_PI / 2 - i * teta + M_PI/5),
                                                                  0.0f);
    }
    penta1.at(5) = penta1.at(0);
    penta2.at(5) = penta2.at(0);
    std::vector<Vector2> penta11(5);
    std::vector<Vector2> penta22(5);

    for (size_t i = 0 ; i < 5 ; ++i)
    {
        penta11.at(i) = Vector2(penta1.at(i).x, penta1.at(i).y);
        penta22.at(i) = Vector2(penta2.at(i).x, penta2.at(i).y);
    }

    faces = std::vector<Face*>(12);
    //we create the faces
    faces.at(0) = new Face(new RegularPolygon(pts.at(0), 5, length));
    faces.at(11) = new Face(new RegularPolygon(pts.at(1), 5, length,0,0,36));

    float angle = 2 * M_PI - std::acos(-1.0f / std::sqrt(5));
    for (size_t i = 0 ; i < 5 ; ++i)
    {
        faces.at(i + 1) = new Face(new Polygon(penta11, penta1.at(i), pts.at(0), (1.0f / length) * (penta1.at(i + 1) - penta1.at(i)), -angle));
        faces.at(i + 6) = new Face(new Polygon(penta22, penta2.at(i), pts.at(1), (1.0f / length) * (penta2.at(i + 1) - penta2.at(i)), angle));
    }
    size_t size = faces.size();
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size ; ++i)
    {
        cut.at(i + 1) = cut.at(i) + faces.at(i)->get_square_area();
    };
    std::clog << "over" << std::endl;
}

Icosahedron::Icosahedron(Ultrahaptics::Vector3 center, float radius, float rot_x, float rot_y, float rot_z)
{
    tag = 3;
    cycle = false;
    rot_center = center;
    r_x = rot_x;
    r_y = rot_y;
    r_z = rot_z;

    //we create the points
    std::vector<Ultrahaptics::Vector3> pts(14);
    float phi = (1 + std::sqrt(5)) / 2;
    float side = 2 * radius / std::sqrt(2 + phi);
    float temp =  radius - side * std::sqrt(0.5 - 1 / (2 * std::sqrt(5)));
    float length = std::sqrt(radius * radius - temp * temp);
    std::clog << "let's see the values: " << temp << " " << length << std::endl;

    pts.at(0) = center + Ultrahaptics::Vector3(0, 0, radius);
    pts.at(1) = center - Ultrahaptics::Vector3(0, 0, radius);
    for  (size_t i = 0 ; i < 5 ; ++i)
    {
        pts.at(i + 2) = center + Ultrahaptics::Vector3(length * std::cos(2 * M_PI / 5 * i),
                                                       length * std::sin(2 * M_PI / 5 * i),
                                                       temp);
        pts.at(i + 8) = center + Ultrahaptics::Vector3(length * std::cos(2 * M_PI / 5 * i + M_PI / 5),
                                                       length * std::sin(2 * M_PI / 5 * i + M_PI / 5),
                                                       - temp);
    }
    pts.at(7) =  pts.at(2);
    pts.at(13) = pts.at(8);
    faces = std::vector<Face*>(20);
    //we create the faces

    for (size_t i = 0 ; i < 5 ; ++i)
    {
        faces.at(i) = new Face(new Triangle(pts.at(0), pts.at(2 + i), pts.at(3 + i)));
        faces.at(i + 5) = new Face(new Triangle(pts.at(2 + i), pts.at(3 + i), pts.at(8 + i)));
        faces.at(i + 10) = new Face(new Triangle(pts.at(8 + i), pts.at(9 + i), pts.at(3 + i)));
        faces.at(i + 15) = new Face(new Triangle(pts.at(1), pts.at(8 + i), pts.at(9 + i)));

    }

    size_t size = faces.size();
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size ; ++i)
    {
        cut.at(i + 1) = cut.at(i) + faces.at(i)->get_square_area();
    };
}
