#ifndef MOVEMENT_H
#define MOVEMENT_H

#include "Ultrahaptics.hpp"
#include <cmath>
#include "utils.h"

class Movement
{
private:
    unsigned int sample_rate;

    //Part dedicated to the rotation
    Ultrahaptics::Vector3 rot_center;
    Ultrahaptics::Vector3 rot_speed;
    Ultrahaptics::Vector3 rot;

    //Part dedicated to the translation
    Ultrahaptics::Vector3 (*translation)(Ultrahaptics::Vector3, float);
public:
    Movement();

    void set_sample_rate(unsigned int sample_rate);
    void set_rotation(Ultrahaptics::Vector3 center, float speed_x, float speed_y, float speed_z);
    void set_translation(Ultrahaptics::Vector3 (*trans)(Ultrahaptics::Vector3, float));

    Ultrahaptics::Vector3 rotate(Ultrahaptics::Vector3 point);
    Ultrahaptics::Vector3 translate(Ultrahaptics::Vector3 point, float phi);
};

#endif // MOVEMENT_H
