#include "movement.h"

Ultrahaptics::Vector3 default_trans(Ultrahaptics::Vector3 point, float phi)
{
    return point;
}

Movement::Movement()
{
    rot = Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f);
    rot_speed = Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f);
    translation = &default_trans;
}

void Movement::set_sample_rate(unsigned int rate)
{
    sample_rate = rate;
}

void Movement::set_rotation(Ultrahaptics::Vector3 center, float speed_x, float speed_y, float speed_z)
{
    rot_center = center;
    rot = Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f);
    rot_speed = Ultrahaptics::Vector3(M_PI * speed_x / 180, M_PI * speed_y / 180, M_PI * speed_z / 180);

}

void Movement::set_translation(Ultrahaptics::Vector3 (*trans)(Ultrahaptics::Vector3, float))
{
    translation = trans;
}

Ultrahaptics::Vector3 Movement::rotate(Ultrahaptics::Vector3 point)
{
    Ultrahaptics::Vector3 res = rotation(point, rot, rot_center);
    rot = rot + (1.0 / sample_rate) * rot_speed;
    return res;
}

Ultrahaptics::Vector3 Movement::translate(Ultrahaptics::Vector3 point, float phi)
{
    return translation(point, phi);
}
