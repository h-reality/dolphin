#ifndef ULTRAHAPTICSDISPLAY_H
#define ULTRAHAPTICSDISPLAY_H

#include <QObject>
#include <UltrahapticsTimePointStreaming.hpp>
#include "ultrahapticsgeometry.h"
#include "movement.h"

class UltrahapticsDisplay
{
public:
    UltrahapticsDisplay(UltrahapticsGeometry* uhg);
    ~UltrahapticsDisplay();

    void play();
    void stop();

private:
    Ultrahaptics::TimePointStreaming::Emitter m_emitter;
    UltrahapticsGeometry* m_uh_geometry;
};

#endif // ULTRAHAPTICSDISPLAY_H
