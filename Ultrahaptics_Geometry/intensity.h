#ifndef INTENSITY_H
#define INTENSITY_H

class Intensity
{
private:
    enum Variation {TIME, POINT};
    float (*point_intensity_modulator)(unsigned int, unsigned int);
    float (*time_intensity_modulator)(unsigned int);
    unsigned int sample_size;
public:
    Intensity();
    Intensity(float (*fun)(unsigned int, unsigned int), unsigned int size = 100);
    Intensity(float (*fun)(unsigned int));
    void change_sample_size(unsigned int size);
    float get_intensity(unsigned int i);
};

#endif // INTENSITY_H
