#include "intensity.h"

float default_intensity(unsigned int i, unsigned int size)
{
    return 1.0f;
}


Intensity::Intensity()
    : Intensity::Intensity(&default_intensity, 100) {}

Intensity::Intensity(float (*fun)(unsigned int, unsigned int), unsigned int size)
    : point_intensity_modulator(fun), sample_size(size) {}

void Intensity::change_sample_size(unsigned int size)
{
    sample_size = size;
}

float Intensity::get_intensity(unsigned int i)
{
    float res = point_intensity_modulator(i, sample_size);
    if (res < 1.0f)
    {
        return res;
    }
    else
    {
        return 1.0f;
    }
}


/* il faut placer la variation temporelle propremnt, utiliser les variatons possibles
 * (est-ce intéressant d'enregistrer la taille, je ne suis pas sûr)
 * comment mettre en place la différence de traitement ? Où dans UltrahapticDisplay ?*/
