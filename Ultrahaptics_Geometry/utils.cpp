/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#include "utils.h"
#include <iostream>

//fast exponentiation
float exp_rep(float x, unsigned int n)
{
    unsigned int a = n;
    float res = 1.0f;
    float mult = x;
    while (a != 0)
    {
        if (a%2 == 1)
        {
            res *= mult;
        };
        mult *= mult;
        a = a / 2;
    };
    return  res;
}

//return all the binomial coefficients i among n for i = 0 to i = n
std::vector<float> binomial(int n)
{
    std::vector<float> vect(n + 1, 0);
    int temp_1 = 1;
    int temp_2;
    vect.at(0) = 1;
    for (int k = 1 ; k <= n ; k++)
    {
        temp_1 = 1;
        for (int l = 1 ; l <= n ; l++)
        {
            temp_2 = vect.at(l);
            vect.at(l) = temp_1 + temp_2;
            temp_1 = temp_2;
        };
    };
    return vect;
}

//compute n!
float fact(int n)
{
    if (n == 0)
    {
        return 1.0f;
    }
    else
    {
        return n * fact(n - 1);
    };
}

float cdb_aux(float t1, float t2)
{
    if (t1 - t2 < 0.0000001) //if the difference is negligeable
    {
        return 0.0f;
    }
    else
    {
        return 1.0f / (t1 - t2);
    }
}

//do the cox_de_boor algorithm (needed for the B_splines)
std::vector<float> cox_de_boor(std::vector<float> nodes, size_t m, float t)
{
    size_t n = nodes.size() - 1;
    std::vector<float> res(n, 0.0f);
    size_t i = 0;

    while (i + 1 < n && t >= nodes.at(i + 1) && (nodes.at(i + 1) < 1.0f))
    {
        ++i;
    };
    if (i < m || i > n - m)
    {
        return res;
    }
    res.at(i) = 1;
    for (size_t i = 1 ; i <= m ; ++i)
    {
        for (size_t j = 0 ; j < n - i ; ++j)
        {
            res.at(j) = (t - nodes.at(j)) * cdb_aux(nodes.at(j + i), nodes.at(j)) * res.at(j)
                        + (nodes.at(i + j + 1) - t) * cdb_aux(nodes.at(j + i + 1), nodes.at(j + 1)) * res.at(j + 1);
        }
    }
    res.resize(n - m);
    return res;
}

//rotate the vector point according to the axis axis
Ultrahaptics::Vector3 rot_vect(Ultrahaptics::Vector3 point, float rot, Ultrahaptics::Vector3 rot_axis)
{
    Ultrahaptics::Vector3 axis = rot_axis / rot_axis.length();
    Ultrahaptics::Vector3 res;
    float c = std::cos(rot);
    float s = std::sin(rot);
    res.x = (axis.x * axis.x * (1 - c) + c) * point.x + (axis.x * axis.y * (1 - c) - axis.z * s) * point.y + (axis.x * axis.z * (1 - c) + axis.y * s) * point.z;
    res.y = (axis.x * axis.y * (1 - c) + axis.z * s) * point.x + (axis.y * axis.y * (1 - c) + c) * point.y + (axis.y * axis.z * (1 - c) - axis.x * s) * point.z;
    res.z = (axis.x * axis.z * (1 - c) - axis.y * s) * point.x + (axis.y * axis.z * (1 - c) + axis.x * s) * point.y + (axis.z * axis.z * (1 - c) + c) * point.z;
    return res;
}


//rotate the vector (point - rot_center) by the rotations rot.x (roll), rot.y (pitch), rot.z (yaw)
Ultrahaptics::Vector3 rotation(Ultrahaptics::Vector3 point, Ultrahaptics::Vector3 rot, Ultrahaptics::Vector3 rot_center)
{
    //Ultrahaptics::Vector3 res = point - rot_center;
    point = point - rot_center;
    Ultrahaptics::Vector3 res;
    Ultrahaptics::Vector3 x(1,0,0);
    Ultrahaptics::Vector3 y(0,1,0);
    Ultrahaptics::Vector3 z(0,0,1);

    res = rot_vect(point, rot.z, z);
    x = rot_vect(x, rot.z, z);
    y = rot_vect(y, rot.z, z);

    res = rot_vect(res, rot.y, y);
    x = rot_vect(x, rot.y, y);

    res = rot_vect(res, rot.x, x);

    /*res.x = std::cos(rot.z) * std::cos(rot.y) * point.x +
             (-std::sin(rot.z) * std::cos(rot.x) + std::cos(rot.z) * std::sin(rot.y) * std::sin(rot.x)) * point.y +
             (std::sin(rot.z) * std::sin(rot.x) + std::cos(rot.z) * std::sin(rot.y) * std::cos(rot.x)) * point.z;

    res.y = std::sin(rot.z) * std::cos(rot.y) * point.x +
             (std::cos(rot.z) * std::cos(rot.x) + std::sin(rot.z) * std::sin(rot.y) * std::sin(rot.x)) * point.y +
             (-std::cos(rot.z) * std::sin(rot.x) + std::sin(rot.z) * std::sin(rot.y) * std::cos(rot.x)) * point.z;

    res.z = -std::sin(rot.y) * point.x + std::cos(rot.y) * std::sin(rot.x) * point.y + std::cos(rot.y) * std::cos(rot.x) * point.z;
    */return rot_center + res;
}


//sort the list of points using the inf comparaison (yet to be implemented)
template<typename point_t>
std::vector<point_t*> fusion_sort(std::vector<point_t>, bool (*inf)(point_t, point_t))
{
    return std::vector<point_t*>(0);
}

Ultrahaptics::Vector3 chgt_base(Ultrahaptics::Vector3 point, Ultrahaptics::Matrix3x3 mat)
{
    return Ultrahaptics::Vector3(mat.at(0, 0) * point.x + mat.at(0, 1) * point.y + mat.at(0, 2) * point.z,
                                 mat.at(1, 0) * point.x + mat.at(1, 1) * point.y + mat.at(1, 2) * point.z,
                                 mat.at(2, 0) * point.x + mat.at(2, 1) * point.y + mat.at(2, 2) * point.z);

}








//implementation of fusion sort


std::pair<std::vector<Ultrahaptics::Vector3*>, std::vector<Ultrahaptics::Vector3*>> divide_aux(std::vector<Ultrahaptics::Vector3*> pts,
                                                                                               size_t i,
                                                                                               std::vector<Ultrahaptics::Vector3*> l1,
                                                                                               std::vector<Ultrahaptics::Vector3*> l2)
{
    if (i >= pts.size())
    {
        return std::make_pair(l1, l2);
    }
    else if (i == pts.size() - 1)
    {
        l1.push_back(pts.at(i));
        return std::make_pair(l1, l2);
    }
    else
    {
        l1.push_back(pts.at(i));
        l2.push_back(pts.at(i + 1));
        return divide_aux(pts, i + 2, l1, l2);
    }
}

std::pair<std::vector<Ultrahaptics::Vector3*>, std::vector<Ultrahaptics::Vector3*>> divide(std::vector<Ultrahaptics::Vector3*> pts)
{
    return divide_aux(pts, 0, std::vector<Ultrahaptics::Vector3*>(0), std::vector<Ultrahaptics::Vector3*>(0));
}

std::vector<Ultrahaptics::Vector3*> fusion_aux(std::vector<Ultrahaptics::Vector3*> l1,
                                               std::vector<Ultrahaptics::Vector3*> l2,
                                               std::vector<Ultrahaptics::Vector3*> res,
                                               bool (*inf)(Ultrahaptics::Vector3, Ultrahaptics::Vector3))
{
    if (l1.empty())
    {
        for (size_t i = 0 ; i < l2.size() ; ++i)
        {
            res.push_back(l2.at(i));
        }
        return res;

    }
    else if (l2.empty())
    {
        for (size_t i = 0 ; i < l1.size() ; ++i)
        {
            res.push_back(l2.at(i));
        }
        return res;
    }
    else
    {
        if (inf(*l1.at(l1.size() - 1), *l2.at(l2.size() - 1)))
        {
            res.push_back(l1.at(l1.size()));
            l1.pop_back();
        }
        else
        {
            res.push_back(l2.at(l2.size()));
            l2.pop_back();
        }
        return fusion_aux(l1, l2, res, inf);
    }
}

std::vector<Ultrahaptics::Vector3*> fusion(std::vector<Ultrahaptics::Vector3*> l1,
                                           std::vector<Ultrahaptics::Vector3*> l2,
                                           bool (*inf)(Ultrahaptics::Vector3, Ultrahaptics::Vector3))
{
    return fusion_aux(l1, l2, std::vector<Ultrahaptics::Vector3*>(0), inf);
}

std::vector<Ultrahaptics::Vector3*> fusion_sort_aux(std::vector<Ultrahaptics::Vector3*> pts,
                                                    bool (*inf)(Ultrahaptics::Vector3, Ultrahaptics::Vector3))
{
    if (pts.size() < 2) return pts;
    else
    {
        std::pair<std::vector<Ultrahaptics::Vector3*>, std::vector<Ultrahaptics::Vector3*>> temp = divide(pts);
        return fusion(fusion_sort_aux(std::get<0>(temp), inf), fusion_sort_aux(std::get<1>(temp), inf), inf);
    }
}

std::vector<Ultrahaptics::Vector3*> fusion_sort(std::vector<Ultrahaptics::Vector3> pts,
                                                bool (*inf)(Ultrahaptics::Vector3, Ultrahaptics::Vector3))
{
    std::vector<Ultrahaptics::Vector3*> res(pts.size());
    for (size_t i = 0 ; i < pts.size() ; ++ i)
    {
        res.at(i) = &pts.at(i);
    }
    return fusion_sort_aux(res, inf);
}
