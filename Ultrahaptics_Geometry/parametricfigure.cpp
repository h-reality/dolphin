#include "parametricfigure.h"

using namespace Geometries;
B_lemniscate::B_lemniscate()
    : B_lemniscate::B_lemniscate(Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f), 1.0f, 0.0f, 0.0f, 0.0f) {}

B_lemniscate::B_lemniscate(Ultrahaptics::Vector3 center, float length, float rot_x, float rot_y, float rot_z)
    : center(center), length(length)
{
    cycle = true;
    rot_center = center;
    r_x = rot_x * M_PI / 180.0f;
    r_y = rot_y * M_PI / 180.0f;
    r_z = rot_z * M_PI / 180.0f;
}

Ultrahaptics::Vector3 B_lemniscate::evaluate_at(float phi, float psi, float teta)
{
    float angle_phi = phi * 2 * M_PI;
    Ultrahaptics::Vector3 point(
                center.x + length * (sin(angle_phi) / (1 + cos(angle_phi) * cos(angle_phi))),
                center.y + length * (sin(angle_phi) * cos(angle_phi) / (1 + cos(angle_phi) * cos(angle_phi))),
                center.z);
    return rotate(point);
}
