/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#include "pointlist.h"

namespace Geometries {

PointList::PointList(std::string str) : PointList(parseTxt(str)) {}

PointList::PointList(std::vector<Ultrahaptics::Vector3> points)
{
    tag = 1;
    rot_center = Ultrahaptics::Vector3(0.f, 0.f, 0.f);
    cycle = false;
    this->points = points;
}

std::vector<Ultrahaptics::Vector3> PointList::getPoints() {
    return points;
}

Ultrahaptics::Vector3 PointList::evaluate_at(float phi, float psi, float teta) {
    float nbPts = points.size();
    if (nbPts > 0) {
        int index = nbPts * fmod(phi, 1.f);
        return  points[index];
    }
    return Ultrahaptics::Vector3(0.f, 0.f, 0.f);
}

std::vector<Ultrahaptics::Vector3> PointList::parseTxt(std::string txt) {
    std::vector<Ultrahaptics::Vector3> res;
    std::string line = "", xstr="", ystr="", zstr="";
    std::stringstream ss(txt);

    while (std::getline(ss, line, '\n')) {
        std::stringstream ssLine(line);
        ssLine >> xstr >> ystr >> zstr;
        try {
            float x = std::stof(xstr);
            float y = std::stof(ystr);
            float z = std::stof(zstr);
            res.push_back(Ultrahaptics::Vector3(x, y, z));
        }  catch (std::invalid_argument _) {}
        xstr = "";
        ystr = "";
        zstr = "";
    }
    return res;
}

double PointList::perimeter() {
    return 0.f;
}

}
