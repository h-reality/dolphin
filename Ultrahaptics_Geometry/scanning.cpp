#include "scanning.h"

static float EPSILLON = 0.0000001;


bool infx(Ultrahaptics::Vector3 point1, Ultrahaptics::Vector3 point2)
{
    if (point1.x > point2.x) return false;
    if (point1.x < point2.x) return true;
    if (point1.y > point2.y) return false;
    if (point1.y < point2.y) return true;
    if (point1.z > point2.z) return false;
    return true;
}
bool infy(Ultrahaptics::Vector3 point1, Ultrahaptics::Vector3 point2)
{
    if (point1.y > point2.y) return false;
    if (point1.y < point2.y) return true;
    if (point1.z > point2.z) return false;
    if (point1.z < point2.z) return true;
    if (point1.x > point2.x) return false;
    return true;
}
bool infz(Ultrahaptics::Vector3 point1, Ultrahaptics::Vector3 point2)
{
    if (point1.z > point2.z) return false;
    if (point1.z < point2.z) return true;
    if (point1.x > point2.x) return false;
    if (point1.x < point2.x) return true;
    if (point1.y > point2.y) return false;
    return true;
}
bool infc(Ultrahaptics::Vector3 point1, Ultrahaptics::Vector3 point2)
{
    if (point1.length() > point2.length()) return false;
    else if ( std::abs(point1.length() - point2.length()) < EPSILLON) return infx(point1, point2);
    return true;
}


Scanning::Scanning(){}


Scanning1D::Scanning1D(std::vector<Ultrahaptics::Vector3> list_pts, Intensity inten)
{
    m_next_idx = 0;
    intensity = inten;
    points = std::vector<Ultrahaptics::Vector3*>(list_pts.size());
    for (size_t i = 0 ; i < list_pts.size() ; ++ i)
    {
        points.at(i) = &list_pts.at(i);
    }
}

std::pair<Ultrahaptics::Vector3, float> Scanning1D::evaluate_at_next()
{
    std::pair<Ultrahaptics::Vector3, float> temp(*points.at(m_next_idx),intensity.get_intensity(m_next_idx));
    m_next_idx = (m_next_idx + 1) % points.size();
    return temp;
}

CoordScanning::CoordScanning(std::vector<Ultrahaptics::Vector3> list_pts, Intensity inten, Coord c)
{
    m_next_idx = 0;
    intensity = inten;
    coord = c;
    if (c == X)
    {
        points = fusion_sort(list_pts, &infx);
    }
    else if (c == Y)
    {
        points = fusion_sort(list_pts, &infy);
    }
    else
    {
        points = fusion_sort(list_pts, &infz);
    }
}

std::pair<Ultrahaptics::Vector3, float> CoordScanning::evaluate_at_next()
{
    std::pair<Ultrahaptics::Vector3, float> temp(*points.at(m_next_idx),intensity.get_intensity(m_next_idx));
    m_next_idx = (m_next_idx + 1) % points.size();
    return temp;
}


CentricScanning::CentricScanning(std::vector<Ultrahaptics::Vector3> list_pts, Intensity inten)
{
    m_next_idx = 0;
    intensity = inten;
    size_t size = list_pts.size();
    bar = Ultrahaptics::Vector3(0, 0, 0);
    for (size_t i = 0 ; i < size ; ++i)
    {
        bar += list_pts.at(i);
    }
    bar = bar / size;
    std::vector<Ultrahaptics::Vector3> temp(size);
    for (size_t i = 0 ; i < size ; ++i)
    {
        temp.at(i) = list_pts.at(i) - bar;
    }
    points = fusion_sort(temp, &infc);
}

std::pair<Ultrahaptics::Vector3, float> CentricScanning::evaluate_at_next()
{
    std::pair<Ultrahaptics::Vector3, float> temp(*points.at(m_next_idx),intensity.get_intensity(m_next_idx));
    m_next_idx = (m_next_idx + 1) % points.size();
    return temp;
}

RandomScanning::RandomScanning(std::vector<Ultrahaptics::Vector3> list_pts, Intensity inten)
{
    std::srand(std::time(nullptr));
    m_next_idx = rand() % list_pts.size();
    intensity = inten;
    points = std::vector<Ultrahaptics::Vector3*>(list_pts.size());
    for (size_t i = 0 ; i < list_pts.size() ; ++ i)
    {
        points.at(i) = &list_pts.at(i);
    }
}

std::pair<Ultrahaptics::Vector3, float> RandomScanning::evaluate_at_next()
{
    std::pair<Ultrahaptics::Vector3, float> temp(*points.at(m_next_idx),intensity.get_intensity(m_next_idx));
    m_next_idx = rand() % points.size();
    return temp;
}
