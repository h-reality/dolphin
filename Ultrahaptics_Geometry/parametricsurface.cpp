#include "parametricsurface.h"
#include <cmath>

using namespace Geometries;
FilledEllipse::FilledEllipse(Ultrahaptics::Vector3 center, float rx, float ry, float rot_x, float rot_y, float rot_z)
    : center(center), rx(rx), ry(ry)
{
    tag = 2;
    rot_center = center;
    r_x = rot_x * M_PI / 180.0f;
    r_y = rot_y * M_PI / 180.0f;
    r_z = rot_z * M_PI / 180.0f;


    cycle = true;
};

//give a point depending of the phi parameter (0 <= phi <= 1)
Ultrahaptics::Vector3 FilledEllipse::evaluate_at(float phi, float psi, float teta)
{
    float angle_phi = phi * 2 * M_PI;
    Ultrahaptics::Vector3 point(
                center.x + rx * cos(angle_phi) * psi,
                center.y + ry * sin(angle_phi) * psi,
                center.z + 0);
    return rotate(point);
};

Disk::Disk(Ultrahaptics::Vector3 center, float radius, float rot_x, float rot_y, float rot_z)
    : FilledEllipse(center, radius, radius, rot_x, rot_y, rot_z) {}
