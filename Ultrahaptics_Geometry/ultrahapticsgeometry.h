#ifndef ULTRAHAPTICSGEOMETRY_H
#define ULTRAHAPTICSGEOMETRY_H

#include <UltrahapticsTimePointStreaming.hpp>
#include <vector>
#include <cmath>
#include <fstream>
#include "geometry.h"
#include "window.h"
#include "intensity.h"
#include "movement.h"
#include "scanning.h"

using namespace Geometries;
class UltrahapticsGeometry
{
public:
    UltrahapticsGeometry();
    UltrahapticsGeometry(Geometry* geo, size_t nb_points, float speed = 7.5f, unsigned int hsr = 40000, std::vector<std::vector<Window*>> window_list = std::vector<std::vector<Window*>>(0), Intensity intensity = Intensity(), float mvt_speed = 1.0f);


    ~UltrahapticsGeometry();

    void set_hardware_sample_rate(unsigned int samplerate);
    void add_geometry(Geometry* geo, size_t nb_points);
    void initialize_timepoint_cloud(float speed);
    void initialize_timepoint_cloud(float speed, unsigned int hsr);
    void initialize_windowpoint();
    void add_new_window(Window* window, size_t ind = INT_MAX);
    void set_rotation(Ultrahaptics::Vector3 center, float speed_x, float speed_y, float speed_z);
    void set_translation(Ultrahaptics::Vector3 (*trans)(Ultrahaptics::Vector3, float));

    Ultrahaptics::Vector3 evaluate_at_next();
    float get_intensity();

    void export_obj(std::string filname, std::string path = "./");
    void import_obj(std::string filename);

private:
    std::vector<Ultrahaptics::Vector3> m_point_cloud;
    std::vector<Ultrahaptics::Vector3> m_windowpoint_cloud;
    std::vector<Ultrahaptics::Vector3> m_timepoint_cloud;
    std::vector<std::vector<Window*>> windows;
    size_t wind_ind;
    Movement movement;
    Intensity intensity;
    size_t m_next_idx;
    float mvt_speed;
    float previous_trans;
    unsigned int m_hardware_sample_rate;
    bool is_inside(Ultrahaptics::Vector3 point);
};

#endif // ULTRAHAPTICSGEOMETRY_H
