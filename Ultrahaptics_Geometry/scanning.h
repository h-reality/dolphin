#ifndef SCANNING_H
#define SCANNING_H

#include <vector>
#include <random>
#include "Ultrahaptics.hpp"
#include "intensity.h"
#include "utils.h"


class Scanning
{
protected:
    size_t m_next_idx;
    std::vector<Ultrahaptics::Vector3*> points;
    Intensity intensity;
public:
    Scanning();
    std::vector<Ultrahaptics::Vector3*> get_points();
    virtual std::pair<Ultrahaptics::Vector3, float> evaluate_at_next() =0;

};

class Scanning1D : public Scanning
{
public:
    Scanning1D(std::vector<Ultrahaptics::Vector3> list_pts, Intensity intensity);
    std::pair<Ultrahaptics::Vector3, float> evaluate_at_next() override;
};

class CoordScanning : public Scanning
{
private:
    enum Coord {X, Y, Z};
    Coord coord;
public:
    CoordScanning(std::vector<Ultrahaptics::Vector3> list_pts, Intensity intensity, Coord c = X);
    std::pair<Ultrahaptics::Vector3, float> evaluate_at_next() override;
};

class CentricScanning : public Scanning
{
private:
    Ultrahaptics::Vector3 bar;
    enum Dir {CENTRIFUGAL, CENTRIPETAL};
public:
    CentricScanning(std::vector<Ultrahaptics::Vector3> list_pts, Intensity intensity);
    std::pair<Ultrahaptics::Vector3, float> evaluate_at_next() override;
};

class RandomScanning : public Scanning
{
public:
    RandomScanning(std::vector<Ultrahaptics::Vector3> list_pts, Intensity intensity);
    std::pair<Ultrahaptics::Vector3, float> evaluate_at_next() override;
};

#endif // SCANNING_H
