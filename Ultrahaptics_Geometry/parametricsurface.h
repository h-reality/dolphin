#ifndef PARAMETRICSURFACE_H
#define PARAMETRICSURFACE_H

#include "geometry.h"

namespace Geometries {
class FilledEllipse : public Geometry
{
protected:
    Ultrahaptics::Vector3 center;
    float angle;
    float rx;
    float ry;
public:
    FilledEllipse(Ultrahaptics::Vector3 center, float rx, float ry, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
};

class Disk : public FilledEllipse
{
public:
    Disk(Ultrahaptics::Vector3 center, float radius, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
};
}

#endif // PARAMETRICSURFACE_H
