#include "window.h"

//////////////////////////
//     Plane windows    //
//////////////////////////

PlaneWindow::PlaneWindow()
{
    plane = std::pair<Ultrahaptics::Vector3, Ultrahaptics::Vector3>(Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f), Ultrahaptics::Vector3(1.0f, 0.0f, 0.0f));
}

PlaneWindow::PlaneWindow(Ultrahaptics::Vector3 point, Ultrahaptics::Vector3 normal)
{
    plane = std::pair<Ultrahaptics::Vector3, Ultrahaptics::Vector3>(point, normal);
}


bool PlaneWindow::is_inside(Ultrahaptics::Vector3 point)
{
    Ultrahaptics::Vector3 test_vector = point - std::get<0>(plane);
    if (test_vector.dot(std::get<1>(plane)) < 0)
    {
        return false;
    }
    return true;
}

//////////////////////////
//     Curve windows    //
//////////////////////////

CurveWindow::CurveWindow(bool (*limit)(Ultrahaptics::Vector3))
    : lim(limit) {}

bool CurveWindow::is_inside(Ultrahaptics::Vector3 point)
{
    return lim(point);
}

//////////////////////////
//  Paraboloid windows  //
//////////////////////////

//Windows where space is divided by a paraboloid curve
ParaboloidWindow::ParaboloidWindow(Ultrahaptics::Vector3 center, float p, float q, bool inside, float rx, float ry, float rz)
    : p(p), q(q), inside(inside), center(center)
{
    rot = Ultrahaptics::Vector3(-rx * M_PI / 180.0f, -ry * M_PI / 180.0f, -rz * M_PI / 180.0f);
}

bool ParaboloidWindow::is_inside(Ultrahaptics::Vector3 point)
{
    Ultrahaptics::Vector3 new_point = rotation(point, rot, center);
    new_point = new_point - center;
    if (inside)
    {
        return 2 * new_point.z >= (new_point.x * new_point.x / p + new_point.y * new_point.y / q);
    }
    else
    {
        return 2 * new_point.z < (new_point.x * new_point.x / p + new_point.y * new_point.y / q);
    }
}
