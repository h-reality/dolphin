QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        ellipse.cpp \
        face.cpp \
        geometry.cpp \
        intensity.cpp \
        main.cpp \
        movement.cpp \
        parametricfigure.cpp \
        parametricsurface.cpp \
        polyedra.cpp \
        polygon.cpp \
        polylines.cpp \
        rectangles.cpp \
        regularpolygon.cpp \
        scanning.cpp \
        spline.cpp \
        splinesurface.cpp \
        tests.cpp \
        triangle.cpp \
        ultrahapticsdisplay.cpp \
        ultrahapticsgeometry.cpp \
        utils.cpp \
        window.cpp \
        Spline-master/splines.inl

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# ULTRAHAPTICS DEPENDENCIE
#DEPENDPATH +=D:/Ultrahaptics/Libraries/CPlusPlus
#INCLUDEPATH += D:/Ultrahaptics/Headers
#LIBS += -LD:/Ultrahaptics/Libraries/CPlusPlus -lUltrahaptics

# ULTRAHAPTICS DEPENDENCIES
DEPENDPATH += D:/Cours/ENS/M1/Projet_recherche/Windows_Win64/Libraries/CPlusPlus
INCLUDEPATH += D:/Cours/ENS/M1/Projet_recherche/Windows_Win64/Headers
LIBS += -LD:/Cours/ENS/M1/Projet_recherche/Windows_Win64/Libraries/CPlusPlus -lUltrahaptics

HEADERS += \
    ellipse.h \
    face.h \
    geometry.h \
    intensity.h \
    movement.h \
    parametricfigure.h \
    parametricsurface.h \
    polyedra.h \
    polygon.h \
    polylines.h \
    rectangles.h \
    regularpolygon.h \
    scanning.h \
    spline.h \
    splinesurface.h \
    triangle.h \
    ultrahapticsdisplay.h \
    ultrahapticsgeometry.h \
    utils.h \
    window.h \
    Spline-master/splines.hpp
