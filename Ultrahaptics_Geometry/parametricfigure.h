#ifndef PARAMETRICFIGURE_H
#define PARAMETRICFIGURE_H

#include "geometry.h"

namespace Geometries {
//Lemniscate of Bernoulli
class B_lemniscate : public Geometry
{
private:
    Ultrahaptics::Vector3 center;
    float length;
    float angle;
public:
    B_lemniscate();
    B_lemniscate(Ultrahaptics::Vector3 center, float length, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
};
}


#endif // PARAMETRICFIGURE_H
