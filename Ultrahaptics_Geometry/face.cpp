#include "face.h"

using namespace Geometries;
Face::Face() {}

Face::Face(std::vector<Vector2> points, Ultrahaptics::Vector3 ref_point, float rot_x, float rot_y, float rot_z)
    : Polygon (points, ref_point, rot_x, rot_y, rot_z)
{
    tag = 3;
    cycle = false;
    float min_x = FLT_MAX, min_y = FLT_MAX, max_x = FLT_MIN, max_y = FLT_MIN;
    for (size_t i = 0 ; i < points2D.size() - 1 ; ++i)
    {
        if (points2D.at(i).x < min_x) {min_x = points2D.at(i).x;}
        if (points2D.at(i).x > max_x) max_x = points2D.at(i).x;
        if (points2D.at(i).y < min_y) min_y = points2D.at(i).y;
        if (points2D.at(i).y > max_y) max_y = points2D.at(i).y;
    }
    summit = Vector2(min_x, min_y);
    base1 = Vector2(max_x - min_x, 0.0f);
    base2 = Vector2(0.0f, max_y - min_y);
    Ultrahaptics::Vector3 x(1, 0, 0);
    Ultrahaptics::Vector3 y(0, 1, 0);
    Ultrahaptics::Vector3 z(0, 0, 1);
    x = rotate(x + rot_center) - rot_center;
    y = rotate(y + rot_center) - rot_center;
    z = rotate(z + rot_center) - rot_center;

    mat = Ultrahaptics::Matrix3x3(x.x, y.x, z.x, x.y, y.y, z.y, x.z, y.z, z.z);
}

Face::Face(Polygon* poly)
{
    std::clog << "a" << " ";
    line = poly->get_line();
    cut = poly->get_cut();
    points2D = poly->get_pts2();
    rot_center = poly->get_ref_point();
    r_x = poly->get_rot().at(0);
    r_y = poly->get_rot().at(1);
    r_z = poly->get_rot().at(2);
    mat = poly->get_mat();

    tag = 3;
    cycle = false;
    float min_x = FLT_MAX, min_y = FLT_MAX, max_x = FLT_MIN, max_y = FLT_MIN;
    std::clog << "b" << " " << points2D.at(0).x << " " << points2D.at(0).y << std::endl;
    for (size_t i = 0 ; i < points2D.size() - 1 ; ++i)
    {
        std::clog << i << " ";
        if (points2D.at(i).x < min_x) {min_x = points2D.at(i).x;}
        if (points2D.at(i).x > max_x) {max_x = points2D.at(i).x;}
        if (points2D.at(i).y < min_y) {min_y = points2D.at(i).y;}
        if (points2D.at(i).y > max_y) {max_y = points2D.at(i).y;}
    }
    std::clog << "c" << " ";
    summit = Vector2(min_x, min_y);
    base1 = Vector2(max_x - min_x, 0.0f);
    base2 = Vector2(0.0f, max_y - min_y);
}

const Ultrahaptics::Vector3 Face::OUT = Ultrahaptics::Vector3(FLT_MAX, FLT_MAX, FLT_MAX);


Ultrahaptics::Vector3 Face::convert(Ultrahaptics::Vector3 point)
{
    return chgt_base(point, mat);
}

Ultrahaptics::Vector3 Face::evaluate_at(float phi, float psi, float teta)
{
    //to get the external shape of the polygon
    if (teta <= 0.5f)
    {
        return Polygon::evaluate_at(phi, psi, teta);
    }

    //we fill the polygon to get a surface

    Vector2 test = summit.add((base1.mult(phi)).add(base2.mult(psi)));
    if (is_inside(test))
    {
        return convert(Ultrahaptics::Vector3(test.x, test.y, 0)) + rot_center;
    }
    //we return a default value that is not to be considered
    return OUT;
}


bool Face::is_out(Ultrahaptics::Vector3 point)
{
    return (OUT.x - point.x <= 0.0000001 && OUT.y - point.y <= 0.0000001 && OUT.z - point.z <= 0.0000001);
}

//return the area of the square used to sample the surface
float Face::get_square_area()
{
    return base1.length() * base2.length();
}

Polyfaces::Polyfaces() {}

Polyfaces::Polyfaces(std::vector<Face*> list_face)
    : faces(list_face)
{
    tag = 3;
    cycle = false;
    rot_center = Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f);
    r_x = 0.0f;
    r_y = 0.0f;
    r_z = 0.0f;
    size_t size = faces.size();
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size ; ++i)
    {
        cut.at(i + 1) = cut.at(i) + faces.at(i)->get_square_area();
    };
}

Ultrahaptics::Vector3 Polyfaces::evaluate_at(float phi, float psi, float teta)
{
    size_t size = faces.size();
    for (size_t i = 1 ; i < size ; ++ i)
    {
        if (phi  * cut.at(size) <  cut.at(i))
        {
            float frac_phi = (phi * cut.at(size) - cut.at(i - 1)) / (cut.at(i) - cut.at(i - 1));
            //float frac_psi = (psi * cut.at(size) - cut.at(i - 1)) / (cut.at(i) - cut.at(i - 1));
            return rotate((faces.at(i - 1))->evaluate_at(frac_phi, psi, teta));
        };
    };
    float frac_phi = (phi * cut.at(size) - cut.at(size - 1)) / (cut.at(size) - cut.at(size - 1));
    //float frac_psi = (psi * cut.at(size) - cut.at(size - 1)) / (cut.at(size) - cut.at(size - 1));
    return rotate((faces.at(size - 1))->evaluate_at(frac_phi, psi, teta));
}
