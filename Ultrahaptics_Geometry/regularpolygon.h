/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef REGULARPOLYGONE_H
#define REGULARPOLYGONE_H

#include "polylines.h"
#include "polygon.h"
#include <cmath>

namespace Geometries {
class RegularPolygon : public Polygon
{
public:
    RegularPolygon();
    RegularPolygon(Ultrahaptics::Vector3 center, unsigned int side_number);
    RegularPolygon(Ultrahaptics::Vector3 center, unsigned int side_number, float side_length, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);

    float get_side_length();
    double perimeter() override;
private:
    float side_length = 1.0f;
    int side_number;
};
}
#endif // REGULARPOLYGONE_H
