#include "spline.h"

using namespace Geometries;
//////////////////////////
//     Basic splines    //
//////////////////////////

//wrap the spline type

//spline_order must be greater than 2
Spline1D::Spline1D(std::vector<Ultrahaptics::Vector3> points, int spline_order)
{
    cycle = false;
    tag = 1;
    spline = Spline<Ultrahaptics::Vector3, float>(spline_order);
    spline.set_ctrl_points(points);
}

Ultrahaptics::Vector3 Spline1D::evaluate_at(float phi, float psi, float teta)
{
    return spline.eval_f(phi);
}

//////////////////////////
//     Besier curves    //
//////////////////////////

BesierCurve::BesierCurve(std::vector<Ultrahaptics::Vector3> points)
{
    tag = 1;
    cycle = false;
    order = points.size() - 1;
    point_list = points;
    list_coeff = std::vector<std::vector<float>>(1, binomial(order));

}

Ultrahaptics::Vector3 BesierCurve::evaluate_at(float phi, float psi, float teta)
{
    Ultrahaptics::Vector3 res(0.0f, 0.0f, 0.0f);
    for (size_t i = 0 ; i <= order ; ++i)
    {
        res += list_coeff.at(0).at(i) * exp_rep(1 - phi, order - i) * exp_rep(phi, i) * point_list.at(i);
    };
    return res;
}


//////////////////////////
//       B_splines      //
//////////////////////////

//in comment is the code for regular B_spline (may be implemented)

BSpline::BSpline(std::vector<Ultrahaptics::Vector3> points, std::vector<float> nodes)
{
    tag = 1;
    cycle = false;
    point_list = points;
    node_list = nodes;
    n = nodes.size() - 1;
    m = n - points.size();
    length = nodes.at(n - m) - nodes.at(m);
}

Ultrahaptics::Vector3 BSpline::evaluate_at(float phi, float psi, float teta)
{
    Ultrahaptics::Vector3 res(0.0f, 0.0f, 0.0f);
    float t = node_list.at(m) + phi / length;

    std::vector<float> coeffs = cox_de_boor(node_list, m, t);
    for (size_t i = 0 ; i < n - m ; ++i)
    {
        res += coeffs.at(i) * point_list.at(i);
    };
    std::clog << res << std::endl;
    return res;
}

//////////////////////////
//         NURBS        //
//////////////////////////

Nurbs::Nurbs(std::vector<Ultrahaptics::Vector3> points, std::vector<float> nodes, std::vector<float> weights)
    : BSpline::BSpline(points, nodes), w(weights) {}

Ultrahaptics::Vector3 Nurbs::evaluate_at(float phi, float psi, float teta)
{
    Ultrahaptics::Vector3 res(0.0f, 0.0f, 0.0f);
    float t = node_list.at(m) + phi / length;
    std::vector<float> coeffs = cox_de_boor(node_list, m, t);
    float tot = 0.0f;
    for (size_t i = 0 ; i < n - m ; ++i)
    {
        res += coeffs.at(i) * w.at(i) * point_list.at(i);
        tot += coeffs.at(i) * w.at(i);
    };
    return (1.0f / tot) * res;

}
