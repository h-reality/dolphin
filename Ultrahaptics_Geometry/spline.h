#ifndef SPLINE_H
#define SPLINE_H

#include <vector>
#include "geometry.h"
#include "utils.h"
#include "Spline-master/splines.hpp"

namespace Geometries {
class Spline1D : public Geometry
{
private:
    Spline<Ultrahaptics::Vector3, float> spline;


public:
    Spline1D(std::vector<Ultrahaptics::Vector3> points, int spline_order = 3);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
};


class BesierCurve : public Geometry
{
private:
    size_t order;
    std::vector<Ultrahaptics::Vector3> point_list;
    std::vector<std::vector<float>> list_coeff;

public:
    BesierCurve(std::vector<Ultrahaptics::Vector3> points);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
};

class BSpline : public Geometry
{
protected:
    std::vector<Ultrahaptics::Vector3> point_list;
    std::vector<std::vector<float>> list_coeff;
    size_t n;
    size_t m;
    float length;
    std::vector<float> node_list;
public:
    BSpline(std::vector<Ultrahaptics::Vector3> points, std::vector<float> nodes);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
};

class Nurbs : public BSpline
{
private:
    std::vector<float> w;
public:
    Nurbs(std::vector<Ultrahaptics::Vector3> points, std::vector<float> nodes, std::vector<float> weights);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
};
}
#endif // SPLINE_H
