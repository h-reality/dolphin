/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "Ultrahaptics.hpp"
#include <cmath>
#include "utils.h"

#define CIRCLE_NAME "Circle"
#define ELLIPSE_NAME "Ellipse"
#define SQUARE_NAME "Square"
#define RECTANGLE_NAME "Rectangle"
#define TRIANGLE_NAME "Triangle"
#define LINE_NAME "Line"
#define POINT_LIST_NAME "Point list"
#define ARC_NAME "Arc"

namespace Geometries {
class Geometry
{
protected:
    int tag; //to know how many arguments of evaluate_at are needed
    bool cycle;
    float r_x;
    float r_y;
    float r_z;
    Ultrahaptics::Vector3 rot_center;
public:
    virtual Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) =0;
    Ultrahaptics::Vector3 rotate(Ultrahaptics::Vector3 point);
    //give a point depending of the phi parameter (0 <= phi <= 1)
    bool is_cycle();
    int get_tag();
    Ultrahaptics::Vector3 get_center();
    virtual double perimeter();
};
}
#endif // GEOMETRY_H
