/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef CUSTOMSAMPLING_H
#define CUSTOMSAMPLING_H

#include "samplingStrategy.h"

class CustomSampling: public SamplingStrategy {
public:
    CustomSampling(QVector<double> ratios, QVector<int> quantities, int nbPts, int step=1);

    void reset() override;
    void setNbPoints(int nb) override;
    Ultrahaptics::Vector3 pointFromIndex(Geometries::Geometry* g, int index) override;
    std::vector<double> getCaracteristics(int sampleRate, Geometries::Geometry* g) override;

    QVector<double> getRatios();
    QVector<int> getQuantities();
    int getSkipStep();

    // returns ratios, quantities, step
    static std::pair<QVector<double>, QVector<int>> parseTxt(QString txt);
private:
    int skipStep;

    QVector<double> ratios;
    QVector<int> quantities;
    QVector<double> values2Evaluate;

    void computeValues2Evaluate(int skipStep);
};


#endif // CUSTOMSAMPLING_H
