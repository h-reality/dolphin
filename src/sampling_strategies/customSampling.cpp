/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "customSampling.h"

CustomSampling::CustomSampling(QVector<double> ratios, QVector<int> quantities, int nbPts, int skipStep) {
    this->ratios = ratios;
    this->quantities = quantities;
    this->nbPts = nbPts;
    this->skipStep = skipStep;
    computeValues2Evaluate(skipStep);
}

void CustomSampling::reset() {
    currentIndex = 0;
}

void CustomSampling::setNbPoints(int nb) {
    nbPts = nb;
    computeValues2Evaluate(skipStep);
}

Ultrahaptics::Vector3 CustomSampling::pointFromIndex(Geometries::Geometry* g, int index) {
    if (g != nullptr) {
        double phi = values2Evaluate[index];
        phi = fmod(1.f + fmod(phi, 1.f), 1.f);
        return g->evaluate_at(phi);
    }
    return Ultrahaptics::Vector3();
}

std::vector<double> CustomSampling::getCaracteristics(int sampleRate, Geometries::Geometry* g) {
    if (g != nullptr) {
        double frequency = sampleRate * Ultrahaptics::Units::Hz; // Hz
        double timeStep_ms = 1.f / frequency * 1000000.f; // µs

        double ptFrequency = frequency / (double)nbPts; // Frequency at which the focal point passes at the same point (Hz)
        ptFrequency *= (double)std::gcd(nbPts, skipStep);

        double spatialStep_mm = g->perimeter() * (double)skipStep / (double)nbPts * 10.f; // mm

        double focalPtSpeed = spatialStep_mm * frequency / 1000.f; // m / s
        return std::vector<double>({timeStep_ms, ptFrequency, spatialStep_mm, focalPtSpeed});
    }
    return std::vector<double>();
}

QVector<double> CustomSampling::getRatios() {
    return ratios;
}

QVector<int> CustomSampling::getQuantities() {
    return quantities;
}

int CustomSampling::getSkipStep() {
    return skipStep;
}

std::pair<QVector<double>, QVector<int>> CustomSampling::parseTxt(QString txt) {
    QVector<double> ratios;
    QVector<int> quantities;
    std::string ratio = "", quantity = "", line="";
    std::stringstream sstream(txt.toStdString());

    while (std::getline(sstream, line, '\n')) {
        std::stringstream linestream(line);
        linestream >> ratio >> quantity;
        try {
            double r = std::stod(ratio);
            try {
                int qty = std::stoi(quantity);
                if (qty < 1) throw std::invalid_argument("");
                ratios.push_back(r);
                quantities.push_back(qty);
            }  catch (std::invalid_argument _) {
                if (quantity == "") { // if nothing is set, the value is considered to be 1
                    quantities.push_back(1);
                    ratios.push_back(r);
                }
            }
        } catch (std::invalid_argument _) {}
        ratio = "";
        quantity = "";
    }
    return std::make_pair(ratios, quantities);
}

void CustomSampling::computeValues2Evaluate(int skipStep) {
    assert(ratios.length() == quantities.size());

    int nbDefinedPts = std::accumulate(quantities.begin(), quantities.end(), 0);
    double step = 1.f / (double) nbPts;
    QVector<double> values2Evaluate_step1 = QVector<double>();

    double currentDist = 0;
    for (int i = 0 ; i < ratios.length() ; i++) {
        for (int j = 0 ; j < quantities[i] ; j++) {
            currentDist = std::fmod(currentDist + step * ratios[i], 1.f);
            values2Evaluate_step1.push_back(currentDist);
        }
    }
    if (nbDefinedPts < nbPts) {
        double defaultValue = (1.f - std::fmod(currentDist, 1.f)) / ((double) (nbPts - nbDefinedPts + 1));
        for (int i = nbDefinedPts ; i < nbPts ; i++) {
            currentDist = std::fmod(currentDist + defaultValue, 1.f);
            values2Evaluate_step1.push_back(currentDist);
        }
    }

    // Swapping the values according to the skip step
    values2Evaluate = QVector<double>();
    for (int i = 0 ; i < values2Evaluate_step1.size() ; i++) {
        int newIndex = (i * skipStep) % values2Evaluate_step1.size();
        values2Evaluate.push_back(values2Evaluate_step1[newIndex]);
    }
}
