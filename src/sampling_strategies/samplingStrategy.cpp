/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "samplingStrategy.h"

#include <chrono>

//////////////////////////////////
/// SamplingStrategy
//////////////////////////////////

void SamplingStrategy::next() {
    currentIndex = (currentIndex + 1) % nbPts;
}

void SamplingStrategy::previous() {
    currentIndex = (currentIndex - 1 + nbPts) % nbPts;
    // add nbPts because % can return negative values
}
/*
void SamplingStrategy::setGeometry(Geometries::Geometry *g) {
    geometry = g;
    currentIndex = 0;
}*/

Ultrahaptics::Vector3 SamplingStrategy::getCurrentPoint(Geometries::Geometry* g) {
    return pointFromIndex(g, getAbsoluteIndex());
}

QVector<QVector<double>> SamplingStrategy::get2DPoints(Geometries::Geometry* g) {
    QVector<QVector<double>> res(2);
    if (g != nullptr) {

        QVector<double> x(nbPts), y(nbPts);
        for (int i = 0 ; i < nbPts ; i++) {
            Ultrahaptics::Vector3 pt = pointFromIndex(g, i);
            x[i] = pt.x;
            y[i] = pt.y;
        }
        res[0] = x;
        res[1] = y;
    }
    return res;
}

QVector<Ultrahaptics::Vector3> SamplingStrategy::get3DPoints(Geometries::Geometry* g) {
    QVector<Ultrahaptics::Vector3> res;
    if (g != nullptr) {

        QVector<double> x(nbPts), y(nbPts);
        for (int i = 0 ; i < nbPts ; i++) {
            res.push_back(pointFromIndex(g, i));
        }
    }
    return res;
}

int SamplingStrategy::getNbPts() {
    return nbPts;
}

int SamplingStrategy::getAbsoluteIndex() {
    return currentIndex;
}

QVector<double> SamplingStrategy::getIntensities(int sampleRate) {
    return QVector<double>(nbPts, 1.f); // Full intensity for every point
}
