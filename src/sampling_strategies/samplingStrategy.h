/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef SAMPLINGSTRATEGY_H
#define SAMPLINGSTRATEGY_H

#include "Ultrahaptics.hpp"

#include "../geometries/geometry.h"

#include <QVector>
#include <vector>

#define STEP_SAMPLING_NAME "Step sampling"
#define CUSTOM_STEP_SAMPLING_NAME "Custom step"
#define RANDOM_SAMPLING_NAME "Random sampling"
#define CUSTOM_TIME_STEP_SAMPLING_NAME "Custom time step"

// Base class for every sampling strategy
// Every strategy inheritates from this class
class SamplingStrategy {
public:
    virtual void reset() = 0;
    virtual void setNbPoints(int nb) = 0;
    virtual Ultrahaptics::Vector3 pointFromIndex(Geometries::Geometry* g, int index) = 0;
    virtual std::vector<double> getCaracteristics(int sampleRate, Geometries::Geometry* g) = 0; // get steps, frequencies, ...

    virtual void next();
    virtual void previous();
    virtual int getAbsoluteIndex();
    virtual QVector<double> getIntensities(int sampleRate);

    Ultrahaptics::Vector3 getCurrentPoint(Geometries::Geometry* g);
    QVector<QVector<double>> get2DPoints(Geometries::Geometry* g);
    QVector<Ultrahaptics::Vector3> get3DPoints(Geometries::Geometry* g);
    int getNbPts();

protected:
    int nbPts;
    int currentIndex = 0;
    double samplingRate = 40000;
};

#endif // SAMPLINGSTRATEGY_H
