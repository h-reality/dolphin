/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef CUSTOMTIMESTEPSAMPLING_H
#define CUSTOMTIMESTEPSAMPLING_H

#include "samplingStrategy.h"

#include "UltrahapticsVector3.hpp"

class CustomTimeStepSampling: public SamplingStrategy {
public:
    CustomTimeStepSampling(QVector<int> ratios, QVector<int> quantities, int nbPts, int direction=1, double frequency=200.f * Ultrahaptics::Units::Hz);

    void reset() override;
    void setNbPoints(int nb) override;
    Ultrahaptics::Vector3 pointFromIndex(Geometries::Geometry* g, int index) override;
    std::vector<double> getCaracteristics(int sampleRate, Geometries::Geometry* g) override;

    virtual QVector<double> getIntensities(int sampleRate) override;

    QVector<int> getRatios();
    QVector<int> getQuantities();
    int getDirection();
    double getFrequency();

    // returns ratios, quantities, step
    static std::pair<QVector<int>, QVector<int>> parseTxt(QString txt);

private:
    int direction;  // > 0 means in the "original" way, < 0 means in the reversed way
    double frequency;

    QVector<int> ratios, quantities;
    QVector<double> values2Evaluate;

    void computeValues2Evaluate();
};

#endif // CUSTOMTIMESTEPSAMPLING
