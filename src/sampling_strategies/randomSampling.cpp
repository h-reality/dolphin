/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "randomSampling.h"

#include <chrono>

RandomSampling::RandomSampling(int nbPts) {
    this->nbPts = nbPts;
    computeValues2Evaluate();
}

RandomSampling::RandomSampling(QVector<double> values2Evaluate) {
    this->nbPts = values2Evaluate.size();
    this->values2Evaluate = values2Evaluate;
}

void RandomSampling::reset() {
    currentIndex = 0;
}

void RandomSampling::setNbPoints(int nb) {
    this->nbPts = nb;
    computeValues2Evaluate();
}

Ultrahaptics::Vector3 RandomSampling::pointFromIndex(Geometries::Geometry* g, int index) {
    if (g != nullptr) {
        return g->evaluate_at(values2Evaluate[index]);
    }
    return Ultrahaptics::Vector3();
}

std::vector<double> RandomSampling::getCaracteristics(int sampleRate, Geometries::Geometry* g) {
    return std::vector<double>();
}

QVector<double> RandomSampling::getValues2Evaluate() {
    return values2Evaluate;
}

void RandomSampling::computeValues2Evaluate() {
    values2Evaluate = QVector<double>(nbPts);

    int seed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    srand(seed); // set the random generator
    for (int i = 0 ; i < nbPts ; i++) values2Evaluate[i] = (double)rand() / RAND_MAX;
}
