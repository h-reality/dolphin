/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "stepSampling.h"

StepSampling::StepSampling(int nb, int step) {
    this->nbPts = nb;
    this->step = step;
}

void StepSampling::reset() {
    currentIndex = 0;
    absoluteIndex = 0;
}

void StepSampling::setNbPoints(int nb) {
    nbPts = nb;
}

Ultrahaptics::Vector3 StepSampling::pointFromIndex(Geometries::Geometry* g, int index) {
    if (g != nullptr) {
        return g->evaluate_at((double)(index * step % nbPts) / nbPts);
    }
    return Ultrahaptics::Vector3();
}

void StepSampling::next() {
    currentIndex = (currentIndex + step) % nbPts;
    absoluteIndex = (absoluteIndex + 1) % nbPts;
}

void StepSampling::previous() {
    currentIndex = (currentIndex - step + nbPts) % nbPts;
    absoluteIndex = (absoluteIndex - 1 + nbPts) % nbPts;
    // add nbPts because % can return negative values
}

int StepSampling::getAbsoluteIndex() {
    return absoluteIndex;
}

std::vector<double> StepSampling::getCaracteristics(int sampleRate, Geometries::Geometry* g) {
    if (g != nullptr) {
        double frequency = sampleRate * Ultrahaptics::Units::Hz; // Hz
        double timeStep_ms = 1.f / frequency * 1000000.f; // µs

        double ptFrequency = frequency / (double)nbPts; // Frequency at which the focal point passes at the same point (Hz)
        ptFrequency *= (double)std::gcd(nbPts, step);

        double spatialStep_mm = g->perimeter() * (double)step / (double)nbPts * 10.f; // mm

        double focalPtSpeed = spatialStep_mm * frequency / 1000.f; // m / s
        return std::vector<double>({timeStep_ms, ptFrequency, spatialStep_mm, focalPtSpeed});
    }
    return std::vector<double>();
}

int StepSampling::getStep() {
    return step;
}
