/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef RANDOMSAMPLING_H
#define RANDOMSAMPLING_H

#include "samplingStrategy.h"

class RandomSampling: public SamplingStrategy {
public:
    RandomSampling(int nbPts);
    RandomSampling(QVector<double> values2Evaluate);

    void reset() override;
    void setNbPoints(int nb) override;
    Ultrahaptics::Vector3 pointFromIndex(Geometries::Geometry* g, int index) override;
    std::vector<double> getCaracteristics(int sampleRate, Geometries::Geometry* g) override;

    QVector<double> getValues2Evaluate();

private:
    QVector<double> values2Evaluate;

    void computeValues2Evaluate();
};

#endif // RANDOMSAMPLING_H
