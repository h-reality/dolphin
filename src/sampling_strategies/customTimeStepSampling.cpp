/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "customTimeStepSampling.h"

CustomTimeStepSampling::CustomTimeStepSampling(QVector<int> ratios, QVector<int> quantities, int nbPts, int direction, double frequency) {
    this->ratios = ratios;
    this->quantities = quantities;
    this->nbPts = nbPts;
    this->direction = direction;
    this->frequency = frequency;
    computeValues2Evaluate();
}

void CustomTimeStepSampling::reset() {
    currentIndex = 0;
}

void CustomTimeStepSampling::setNbPoints(int nb) {
    nbPts = nb;
    computeValues2Evaluate();
}

Ultrahaptics::Vector3 CustomTimeStepSampling::pointFromIndex(Geometries::Geometry* g, int index) {
    if (g != nullptr) {
        double phi = values2Evaluate[index];
        phi = fmod(1.f + fmod(phi, 1.f), 1.f);
        return g->evaluate_at(phi);
    }
    return Ultrahaptics::Vector3();
}

std::vector<double> CustomTimeStepSampling::getCaracteristics(int sampleRate, Geometries::Geometry* g) {
    if (g != nullptr) {
        double frequency = sampleRate * Ultrahaptics::Units::Hz; // Hz
        double timeStep_ms = 1.f / frequency * 1000000.f; // µs

        double ptFrequency = frequency / (double)nbPts; // Frequency at which the focal point passes at the same point (Hz)

        double spatialStep_mm = g->perimeter() / (double)nbPts * 10.f; // mm

        double focalPtSpeed = spatialStep_mm * frequency / 1000.f; // m / s
        return std::vector<double>({timeStep_ms, ptFrequency, spatialStep_mm, focalPtSpeed});
    }
    return std::vector<double>();
}

QVector<double> CustomTimeStepSampling::getIntensities(int sampleRate) {
    QVector<double> res;

    // Computing the total number of time steps
    assert(ratios.length() == quantities.length());
    int nbTimeSteps = 0;
    for (int i = 0 ; i < ratios.length() ; i++) nbTimeSteps += ratios[i] * quantities[i];

    // Intensity is a sine wave at the desired frequency
    for (int i = 0 ; i < nbTimeSteps ; i++) {
        double intensity = 0.5 * (1.0 + std::cos(2.0 * M_PI * frequency * i / sampleRate));
        res.push_back(intensity);
    }
    return res;
}

QVector<int> CustomTimeStepSampling::getRatios() {
    return ratios;
}

QVector<int> CustomTimeStepSampling::getQuantities() {
    return quantities;
}

int CustomTimeStepSampling::getDirection() {
    return direction;
}

double CustomTimeStepSampling::getFrequency() {
    return frequency;
}

std::pair<QVector<int>, QVector<int>> CustomTimeStepSampling::parseTxt(QString txt) {
    QVector<int> ratios, quantities;
    std::string ratio = "", quantity = "", line="";
    std::stringstream sstream(txt.toStdString());

    while (std::getline(sstream, line, '\n')) {
        std::stringstream linestream(line);
        linestream >> ratio >> quantity;

        try {
            int r = std::stoi(ratio);
            if (r < 0) throw std::invalid_argument("");
            try {
                int qty = std::stoi(quantity);
                if (qty < 1) throw std::invalid_argument("");
                quantities.push_back(qty);
                ratios.push_back(r);
            }  catch (std::invalid_argument _) {
                if (quantity == "") { // if nothing is set, the value is considered to be 1
                    quantities.push_back(1);
                    ratios.push_back(r);
                }
            }
        } catch (std::invalid_argument _) {}
        ratio = "";
        quantity = "";
    }
    return std::make_pair(ratios, quantities);
}

void CustomTimeStepSampling::computeValues2Evaluate() {
    assert(ratios.length() == quantities.size());

    // Computing the number of defined points
    int nbDefinedPts = 0;
    for (int i = 0 ; i < quantities.size() ; i++) {
        nbDefinedPts += quantities[i] * ratios[i];
    }
    nbDefinedPts = std::min(nbDefinedPts, nbPts);

    int nbRealPts = nbPts - std::min(nbPts, nbDefinedPts) + std::accumulate(quantities.begin(), quantities.end(), 0);;
    double step = 1.f / (double)nbRealPts;

    values2Evaluate = QVector<double>();

    double currentDist = 0;
    for (int i = 0 ; i < quantities.length() ; i++) {
        for (int j = 0 ; j < quantities[i] ; j++) {
            for (int k = 0 ; k < ratios[i] ; k++) {
                if (values2Evaluate.size() < nbDefinedPts) values2Evaluate.push_back(currentDist);
                else break;
            }
            currentDist += step;
        }
    }

    assert(values2Evaluate.length() == nbDefinedPts);
    if (nbDefinedPts == 0) currentDist -= step;  // So that it starts at the first point even if nothing is written
    if (nbDefinedPts < nbPts) {
        for (int i = nbDefinedPts ; i < nbPts ; i++) {
            currentDist += step;
            values2Evaluate.push_back(currentDist);
        }
    }

    // Swapping the values according to the direction
    if (direction < 0) {
        for (int i = 0 ; i <= values2Evaluate.size() / 2; i++) {
            values2Evaluate.swapItemsAt(i, values2Evaluate.size() - i - 1);
        }
    }
}
