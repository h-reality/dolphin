/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "ultraleapTPSEmitter.h"

#include <fstream>

UltraleapTPSEmitter::UltraleapTPSEmitter() {
    emitter = new Ultrahaptics::TimePointStreaming::Emitter();
    emitter->setMaximumControlPointCount(1);
}

void UltraleapTPSEmitter::start() {
    emitter->start();
}

void UltraleapTPSEmitter::stop() {
    emitter->stop();
}

void UltraleapTPSEmitter::set_callback(int sampleRate, void *user_pointer, void *callbackFunctionObject) {
    if (callbackFunctionObject == nullptr) callbackFunctionObject = new UltraleapTPSCallbackHolder();

    emitter->setSampleRate(sampleRate);
    UltraleapTPSCallbackHolder* callbackHolder = static_cast<UltraleapTPSCallbackHolder*>(callbackFunctionObject);
    emitter->setEmissionCallback(callbackHolder->fct, user_pointer);
}

void UltraleapTPSEmitter::set_callback(std::string ptListPath, void* callbackFunctionObject) {
    if (callbackFunctionObject == nullptr) callbackFunctionObject = new UltraleapTPSCallbackHolder();

    UltraleapTPSCallbackHolder* callbackHolder = static_cast<UltraleapTPSCallbackHolder*>(callbackFunctionObject);

    std::ifstream inFile(ptListPath);
    int sampleRate, nbPts;
    inFile >> nbPts >> sampleRate;

    emitter->setSampleRate(sampleRate);

    std::vector<Ultrahaptics::Vector3> points;
    std::vector<double> intensities;

    float x, y, z, intensity;
    for (int i = 0 ; i < nbPts ; i++) {
        inFile >> x >> y >> z;
        points.push_back(Ultrahaptics::Vector3(x, y, z));
        intensities.push_back(intensity);
    }
    inFile.close();

    UserData* user_data = new UserData(points, intensities);
    // Set the callback function to the callback written in ultrahapticsEmiterUtils.h
    emitter->setEmissionCallback(callbackHolder->fct, user_data);
}

void UltraleapTPSEmitter::set_callback(Geometries::Geometry* geometry, SamplingStrategy* samplingStrategy, int sampleRate, void* callbackFunctionObject) {
    if (callbackFunctionObject == nullptr) callbackFunctionObject = new UltraleapTPSCallbackHolder();
    UltraleapTPSCallbackHolder* callbackHolder = static_cast<UltraleapTPSCallbackHolder*>(callbackFunctionObject);

    emitter->setSampleRate(sampleRate);

    QVector<Ultrahaptics::Vector3> points = samplingStrategy->get3DPoints(geometry);
    QVector<double> intensities = samplingStrategy->getIntensities(sampleRate);

    int nbPts = points.length();

    UserData* user_data = new UserData(nbPts);
    user_data->hardware_sample_rate = sampleRate;
    user_data->pattern_start = std::chrono::steady_clock::now();

    for (int i = 0 ; i < nbPts ; i++) {
        user_data->timepoints[i].setPosition(points[i] * Ultrahaptics::Units::centimetres);
        user_data->timepoints[i].setIntensity(intensities[i]);
    }

    // Set the callback function to the callback written in ultrahapticsEmiterUtils.h
    emitter->setEmissionCallback(callbackHolder->fct, user_data);
}

int UltraleapTPSEmitter::getSampleRate() {
    return emitter->getSampleRate();
}
