/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef ULTRALEAPTPSEMITTER_H
#define ULTRALEAPTPSEMITTER_H

#include "emitter.h"

// Object that holds the callback function, that can converted in void* and passed to set_callback
class UltraleapTPSCallbackHolder
{
public:
    UltraleapTPSCallbackHolder() : fct(my_emitter_callback) {};
    UltraleapTPSCallbackHolder(Ultrahaptics::TimePointStreaming::TimePointEmissionCallback callback) : fct(callback) {};
    Ultrahaptics::TimePointStreaming::TimePointEmissionCallback fct;
};


class UltraleapTPSEmitter: public Emitter
{
public:
    UltraleapTPSEmitter();
    void start() override;
    void stop() override;
    void set_callback(Geometries::Geometry* geometry, SamplingStrategy* samplingStrategy, int sampleRate,
                      void* callbackFunctionObject = new UltraleapTPSCallbackHolder()) override;
    void set_callback(int sampleRate, void* user_pointer,
                      void* callbackFunctionObject = new UltraleapTPSCallbackHolder()) override;
    void set_callback(std::string ptListPath,
                      void* callbackFunctionObject = new UltraleapTPSCallbackHolder()) override;
    int getSampleRate() override;

private:
    Ultrahaptics::TimePointStreaming::Emitter* emitter;
};

#endif // ULTRALEAPTPSEMITTER_H
