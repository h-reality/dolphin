/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef ULTRAHAPTICSEMITERUTILS_H
#define ULTRAHAPTICSEMITERUTILS_H

#include <vector>

#include <UltrahapticsTimePointStreaming.hpp>

using Seconds = std::chrono::duration<float>;

// Structure for passing information over time for our control point
struct UserData
{
    explicit UserData(unsigned int timepointCount)
        : timepoints(std::vector<Ultrahaptics::TimePointStreaming::ControlPoint>(timepointCount))
    {}

    explicit UserData(std::vector<Ultrahaptics::Vector3> points, std::vector<double> intensities) {
        unsigned int timepointCount = points.size();
        timepoints = std::vector<Ultrahaptics::TimePointStreaming::ControlPoint>(timepointCount);

        for (int i = 0 ; i < timepointCount ; i++) {
            timepoints[i].position = points[i];
            timepoints[i].intensity = intensities[i];
        }
    }

    std::size_t next_index = 0;
    std::vector<Ultrahaptics::TimePointStreaming::ControlPoint> timepoints;
    unsigned int hardware_sample_rate;
    Ultrahaptics::HostTimePoint pattern_start;

    const Ultrahaptics::TimePointStreaming::ControlPoint evaluateAt(Seconds t) {
        int index = ((unsigned int)(t.count() * hardware_sample_rate)) % timepoints.size();
        return timepoints[index];
    }
};


// Callback function for filling out complete device output states through time
inline void my_emitter_callback(const Ultrahaptics::TimePointStreaming::Emitter& timepoint_emitter,
    Ultrahaptics::TimePointStreaming::OutputInterval& interval,
    const Ultrahaptics::HostTimePoint& submission_deadline,
    void* user_data_ptr)
{

    // The user pointer allows us to store data we can re-use in our callback.
    // In this case, we've used a custom struct that describes the precomputed control point
    // data for the number of samples required to represent a complete period of our waveform.
    UserData* user_data = static_cast<UserData*>(user_data_ptr);

    // Iterate through all of the time points in the given output interval.
    // We only have one control point, and at each point in time we are updating the control
    // point to the next precomputed state.
    // This outputs a continuous smooth waveform on the array.
    for (auto& sample : interval)
    {
        // Update the control point at this time point with our precomputed value
        const Seconds t = sample - user_data->pattern_start;
        sample.persistentControlPoint(0) = user_data->evaluateAt(t);


        // Update our custom struct so we know which control point we are going to use for the
        // next time point. Because we have precomputed one period of the waveform, we wrap
        // around to the start of our precomputed vector of control points.
        user_data->next_index = (user_data->next_index + 1) % user_data->timepoints.size();
    }
}

#endif // ULTRAHAPTICSEMITERUTILS_H
