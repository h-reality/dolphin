/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "importExport.h"

#include <fstream>


//////////////////////////////////
/// Strategy
//////////////////////////////////

std::string ImportExport::exportStrategy(SamplingStrategy* strategy, std::string strategyName, std::string path) {
    std::string unformattedName = strategyName;
    std::replace(strategyName.begin(), strategyName.end(), ' ', '_');  // Replaces spaces with underscores
    std::string exportString = strategyName + ' ';
    if (unformattedName == STEP_SAMPLING_NAME) exportString += computeExportStrategy((StepSampling*) strategy);
    else if (unformattedName == CUSTOM_STEP_SAMPLING_NAME) exportString += computeExportStrategy((CustomSampling*) strategy);
    else if (unformattedName == RANDOM_SAMPLING_NAME) exportString += computeExportStrategy((RandomSampling*) strategy);
    else if (unformattedName == CUSTOM_TIME_STEP_SAMPLING_NAME) exportString += computeExportStrategy((CustomTimeStepSampling*) strategy);

    std::ofstream out(path);
    out << exportString;
    out.close();

    return exportString;
}

std::string ImportExport::computeExportStrategy(StepSampling* strategy) {
    std::stringstream ss;
    ss << strategy->getNbPts() << ' ' << strategy->getStep()
       << "\nnbPts step";
    return ss.str();
}

std::string ImportExport::computeExportStrategy(CustomSampling* strategy) {
    std::stringstream ss;
    ss << strategy->getNbPts() << ' ' << strategy->getSkipStep() << ' ' << strategy->getRatios().length();
    for (double ratio: strategy->getRatios()) ss << ' ' << ratio;
    for (int qty: strategy->getQuantities()) ss << ' ' << qty;
    ss << "\nnbPts skipStep n ratio_1 ... ratio_n qty_1 ... qty_n";
    return ss.str();
}

std::string ImportExport::computeExportStrategy(RandomSampling* strategy) {
    std::stringstream ss;
    ss << strategy->getNbPts();
    for (double value: strategy->getValues2Evaluate()) ss << ' ' << value;
    ss << "\nnbPts n val_1 ... val_n";
    return ss.str();
}

std::string ImportExport::computeExportStrategy(CustomTimeStepSampling* strategy) {
    std::stringstream ss;
    ss << strategy->getNbPts() << ' ' << strategy->getDirection() << ' ' << strategy->getFrequency() << ' ' << strategy->getRatios().length();
    for (double ratio: strategy->getRatios()) ss << ' ' << ratio;
    for (double qty: strategy->getQuantities()) ss << ' ' << qty;
    ss << "\nnbPts direction frequency n ratio_1 ... ratio_n qty_1 ... qty_n";
    return ss.str();
}

std::pair<std::string, SamplingStrategy*> ImportExport::importStrategy(std::string path) {
    std::ifstream in(path);
    std::string strategyName;
    in >> strategyName;
    std::replace(strategyName.begin(), strategyName.end(), '_', ' ');  // Replacing underscores with spaces to restore the name

    SamplingStrategy* strat = nullptr;

    if (strategyName == STEP_SAMPLING_NAME) {
        int nbPts, step;
        in >> nbPts >> step;
        strat = new StepSampling(nbPts, step);
    }

    else if (strategyName == CUSTOM_STEP_SAMPLING_NAME) {
        int nbPts, skipStep, size;
        in >> nbPts >> skipStep >> size;

        QVector<double> ratios(size);
        QVector<int> quantities(size);

        for (int i = 0 ; i < size ; i++) in >> ratios[i];
        for (int i = 0 ; i < size ; i++) in >> quantities[i];

        strat = new CustomSampling(ratios, quantities, nbPts, skipStep);
    }

    else if (strategyName == RANDOM_SAMPLING_NAME) {
        int nbPts;
        in >> nbPts;
        QVector<double> values2Evaluate(nbPts);

        for (int i = 0 ; i < nbPts ; i++) in >> values2Evaluate[i];

        strat = new RandomSampling(values2Evaluate);
    }

    else if (strategyName == CUSTOM_TIME_STEP_SAMPLING_NAME) {
        int nbPts, direction, size;
        double frequency;
        in >> nbPts >> direction >> frequency >> size;

        QVector<int> ratios(size);
        QVector<int> quantities(size);

        for (int i = 0 ; i < size ; i++) in >> ratios[i];
        for (int i = 0 ; i < size ; i++) in >> quantities[i];

        strat = new CustomTimeStepSampling(ratios, quantities, nbPts, direction, frequency);
    }
    return std::make_pair(strategyName, strat);
}

//////////////////////////////////
/// Geometry
//////////////////////////////////

std::string ImportExport::exportGeometry(Geometries::Geometry *g, std::string geometryName, std::string path) {
    std::string unformattedName = geometryName;
    std::replace(geometryName.begin(), geometryName.end(), ' ', '_');  // Replaces spaces with underscores
    std::string exportString = geometryName + ' ';
    if (unformattedName == CIRCLE_NAME) exportString += computeExportGeometry((Geometries::Circle*)g);
    else if (unformattedName == ELLIPSE_NAME) exportString += computeExportGeometry((Geometries::Ellipse*)g);
    else if (unformattedName == SQUARE_NAME) exportString += computeExportGeometry((Geometries::Square*)g);
    else if (unformattedName == RECTANGLE_NAME) exportString += computeExportGeometry((Geometries::Rectangle*)g);
    else if (unformattedName == TRIANGLE_NAME) exportString += computeExportGeometry((Geometries::RegularPolygon*)g);
    else if (unformattedName == LINE_NAME) exportString += computeExportGeometry((Geometries::Segment*)g);
    else if (unformattedName == POINT_LIST_NAME) exportString += computeExportGeometry((Geometries::PointList*)g);
    else if (unformattedName == ARC_NAME) exportString += computeExportGeometry((Geometries::Arc*)g);

    if (geometryName != "...") {  // Can't save empty geometry
        std::ofstream out(path);
        out << exportString;
        out.close();
    }

    return exportString;
}

std::string ImportExport::computeExportGeometry(Geometries::Circle* g) {
    std::stringstream ss;
    Ultrahaptics::Vector3 center = g->get_center();
    ss << g->getRx() << ' ' << center.x << ' ' << center.y << ' '<< center.z
        << "\nradius center_x center_y center_z";
   return ss.str();
}

std::string ImportExport::computeExportGeometry(Geometries::Ellipse* g) {
    std::stringstream ss;
    Ultrahaptics::Vector3 center = g->get_center();
    ss << g->getRx() << ' ' << g->getRy() << ' ' << center.x << ' ' << center.y << ' '<< center.z
        << "\nr_x r_y center_x center_y center_z";
   return ss.str();
}

std::string ImportExport::computeExportGeometry(Geometries::Square* g) {
    std::stringstream ss;
    Ultrahaptics::Vector3 center = g->get_center();
    ss << g->get_length() << ' ' << center.x << ' ' << center.y << ' '<< center.z
        << "\nside center_x center_y center_z";
   return ss.str();
}

std::string ImportExport::computeExportGeometry(Geometries::Rectangle* g) {
    std::stringstream ss;
    Ultrahaptics::Vector3 center = g->get_center();
    ss << g->get_length() << ' ' << g->get_width() << ' ' << center.x << ' ' << center.y << ' '<< center.z
        << "\nlength width center_x center_y center_z";
   return ss.str();
}

std::string ImportExport::computeExportGeometry(Geometries::RegularPolygon* g) {
    std::stringstream ss;
    Ultrahaptics::Vector3 center = g->get_center();
    ss << g->get_side_length() << ' ' << center.x << ' ' << center.y << ' '<< center.z
        << "\nside center_x center_y center_z";
   return ss.str();
}

std::string ImportExport::computeExportGeometry(Geometries::Segment* g) {
    std::stringstream ss;
    Ultrahaptics::Vector3 center = g->get_center();
    ss << g->length() << ' ' << center.x << ' ' << center.y << ' '<< center.z
        << "\nlength center_x center_y center_z";
   return ss.str();
}

std::string ImportExport::computeExportGeometry(Geometries::PointList* g) {
    std::stringstream ss;
    std::vector<Ultrahaptics::Vector3> pts = g->getPoints();
    ss << pts.size();

    for (Ultrahaptics::Vector3 pt: pts) {
        ss << ' ' << pt.x << ' ' << pt.y << ' ' << pt.z;
    }
    ss << "\nn x_1 y_1 z_1 x_2 ... z_n";
    return ss.str();
}

std::string ImportExport::computeExportGeometry(Geometries::Arc* g) {
    std::stringstream ss;
    Ultrahaptics::Vector3 center = g->get_center();
    ss << g->getLength() << ' ' << g->getRadius() << ' '
       << center.x << ' ' << center.y << ' ' << center.z
       << "\nlength radius center_x center_y center_z";
    return ss.str();
}

std::pair<std::string, Geometries::Geometry*> ImportExport::importGeometry(std::string path) {
    std::ifstream in(path);
    std::string geometryName;
    in >> geometryName;
    std::replace(geometryName.begin(), geometryName.end(), '_', ' ');  // Replacing underscores with spaces to restore the name

    Geometries::Geometry* g = nullptr;
    float x, y, z;  // Center

    if (geometryName == CIRCLE_NAME) {
        float radius;
        in >> radius >> x >> y >> z;
        g = new Geometries::Circle(Ultrahaptics::Vector3(x, y, z), radius);
    }

    else if (geometryName == ELLIPSE_NAME) {
        float rx, ry;
        in >> rx >> ry >> x >> y >> z;
        g = new Geometries::Ellipse(Ultrahaptics::Vector3(x, y, z), rx, ry);
    }

    else if (geometryName == SQUARE_NAME) {
        float length;
        in >> length >> x >> y >> z;
        g = new Geometries::Square(Ultrahaptics::Vector3(x, y, z), length);
    }

    else if (geometryName == RECTANGLE_NAME) {
        float length, width;
        in >> length >> width >> x >> y >> z;
        g = new Geometries::Rectangle(Ultrahaptics::Vector3(x, y, z), length, width);
    }

    else if (geometryName == TRIANGLE_NAME) {
        float side;
        in >> side >> x >> y >> z;
        g = new Geometries::RegularPolygon(Ultrahaptics::Vector3(x, y, z), 3, side);
    }

    else if (geometryName == LINE_NAME) {
        float length;
        in >> length >> x >> y >> z;
        g = new Geometries::Segment(Ultrahaptics::Vector3(x - length / 2.f, y, z),
                                    Ultrahaptics::Vector3(x + length / 2.f, y, z));
    }

    else if (geometryName == POINT_LIST_NAME) {
        int nb;
        float x, y, z;
        std::vector<Ultrahaptics::Vector3> pts;
        in >> nb;
        for (int i = 0 ; i < nb ; i++) {
            in >> x >> y >> z;
            pts.push_back(Ultrahaptics::Vector3(x, y, z));
        }
        g = new Geometries::PointList(pts);
    }

    else if (geometryName == ARC_NAME) {
        double length, radius;
        in >> length >> radius
           >> x >> y >> z;
        g = new Geometries::Arc(radius, length, Ultrahaptics::Vector3(x, y, z));
    }

    return std::make_pair(geometryName, g);
}

//////////////////////////////////
/// Geometry + sampling
//////////////////////////////////

void ImportExport::exportGeometryAndStrategy(SamplingStrategy* strategy, std::string strategyName,
                                                    Geometries::Geometry* g, std::string geometryName,
                                                    std::string path, int sampleRate) {
    std::string stratExport = exportStrategy(strategy, strategyName, path);
    std::string geoExport = exportGeometry(g, geometryName, path);
    std::string res = std::to_string(sampleRate) + "\n" + geoExport + '\n' + stratExport;

    std::string pathPointList = path.substr(0, path.find_last_of('.')) + ".ptlist";
    std::string ptsString = pointList2String(strategy, g, sampleRate);

    if (geometryName != "...") {
        std::ofstream out(path);
        out << res;
        out.close();

        std::ofstream outPtList(pathPointList);
        outPtList << ptsString;
        outPtList.close();
    }
}

std::tuple<Geometries::Geometry*, std::string,
           SamplingStrategy*, std::string, int>
ImportExport::importGeometryAndStrategy(std::string path) {
    std::pair<std::string, Geometries::Geometry*> pairGeo;
    std::pair<std::string, SamplingStrategy*> pairSamp;

    std::ifstream in(path);
    std::stringstream ss;
    ss << in.rdbuf();
    in.close();

    // Recover each lines of the original file
    std::string sampleRateLine, geomLine, geomComment, stratLine, stratComment;
    std::getline(ss, sampleRateLine, '\n');
    std::getline(ss, geomLine, '\n');
    std::getline(ss, geomComment, '\n');
    std::getline(ss, stratLine, '\n');
    std::getline(ss, stratComment, '\n');

    // Compute the sample rate from the first line
    int sampleRate = std::stoi(sampleRateLine);

    // Create the corresponding geom file
    std::ofstream out(path);
    out << geomLine << '\n' << geomComment;
    out.close();

    pairGeo = importGeometry(path);

    // Create the corresponding samp file
    out.open(path);
    out << stratLine << '\n' << stratComment;
    out.close();

    pairSamp = importStrategy(path);

    out.open(path);
    out << sampleRateLine << '\n' << geomLine << '\n' << geomComment
        << '\n' << stratLine << '\n' << stratComment;
    out.close();

    return std::make_tuple(pairGeo.second, pairGeo.first,
                           pairSamp.second, pairSamp.first, sampleRate);
}

std::string ImportExport::pointList2String(SamplingStrategy* strategy, Geometries::Geometry* geometry, int sampleRate) {
    QVector<Ultrahaptics::Vector3> pts = strategy->get3DPoints(geometry);
    QVector<double> intensities = strategy->getIntensities(sampleRate);
    std::stringstream ss;
    ss << strategy->getNbPts() << " " << sampleRate;

    for (int i = 0 ; i < strategy->getNbPts() ; i++) {
        Ultrahaptics::Vector3 pt = pts[i] * Ultrahaptics::Units::centimetres;
        ss << " " << pt.x << " " << pt.y << " " << pt.z << " " << intensities[i];
    }

    return ss.str();
}
