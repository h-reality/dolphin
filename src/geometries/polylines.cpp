/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "polylines.h"

using namespace Geometries;
//////////////////////////
//    Segment class     //
//////////////////////////
Segment::Segment()
    : Segment::Segment(Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f),
                       Ultrahaptics::Vector3(1.0f, 0.0f, 0.0f)) {}


Segment::Segment(Ultrahaptics::Vector3 first_point, Ultrahaptics::Vector3 second_point)
{
    tag = 1;
    segment = std::pair<Ultrahaptics::Vector3, Ultrahaptics::Vector3>(first_point, second_point);
    cycle = false;
    float x_center, y_center, z_center;
    x_center = (first_point.x + second_point.x) / 2.f;
    y_center = (first_point.y + second_point.y) / 2.f;
    z_center = (first_point.z + second_point.z) / 2.f;
    rot_center = Ultrahaptics::Vector3(x_center, y_center, z_center);
    r_x = 0.0f;
    r_y = 0.0f;
    r_z = 0.0f;
}

float Segment::length()
{
    Ultrahaptics::Vector3 temp = (std::get<1>(segment) - std::get<0>(segment));
    return temp.length();
}

Ultrahaptics::Vector3 Segment::evaluate_at(float phi, float psi, float teta)
{
    Ultrahaptics::Vector3 vect = std::get<1>(segment) - std::get<0>(segment);
    return std::get<0>(segment) + phi * vect;
}

double Segment::perimeter() {
    return length();
}

//////////////////////////
//    Polyline class    //
//////////////////////////


PolyLines::PolyLines()
{
    tag = 1;
    cycle = false;
    rot_center = Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f);
    r_x = 0.0f;
    r_y = 0.0f;
    r_z = 0.0f;
    line = std::vector<Segment>(1, Segment());
    cut = std::vector<float>(2, 0.0f);
    cut.at(1) = 1.0f;
}


PolyLines::PolyLines(std::vector<Ultrahaptics::Vector3> list_points)
{
    tag = 1;
    cycle = false;
    rot_center = Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f);
    r_x = 0.0f;
    r_y = 0.0f;
    r_z = 0.0f;
    size_t size = list_points.size();
    line = std::vector<Segment>(size - 1, Segment());
    cut = std::vector<float>(size, 0.0f);
    for (size_t i = 0 ; i < size - 1 ; ++i)
    {
        line.at(i) = Segment(list_points.at(i), list_points.at(i + 1));
        cut.at(i + 1) = cut.at(i) + line.at(i).length();
    };
}

PolyLines::PolyLines(std::vector<Segment> list_segments)
    : line(list_segments)
{
    tag = 1;
    cycle = false;
    rot_center = Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f);
    r_x = 0.0f;
    r_y = 0.0f;
    r_z = 0.0f;
    size_t size = list_segments.size();
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size ; ++i)
    {
        cut.at(i + 1) = cut.at(i) + line.at(i).length();
    };
}

 Ultrahaptics::Vector3 PolyLines::evaluate_at(float phi, float psi, float teta)
 {
    size_t size = line.size();
    for (size_t i = 1 ; i < size ; ++ i)
    {
        if (phi * cut.at(size) < cut.at(i))
        {
            float frac = (phi * cut.at(size) - cut.at(i - 1)) / (cut.at(i) - cut.at(i - 1));
            return rotate((line.at(i - 1)).evaluate_at(frac));
        };
    };
    float frac = (phi * cut.at(size) - cut.at(size - 1)) / (cut.at(size) - cut.at(size - 1));
    return rotate((line.at(size - 1)).evaluate_at(frac));
 }

PolyLines::~PolyLines()
{
    line.~vector();
    cut.~vector();
}


 //////////////////////////
 //ClosedPolylines class //
 //////////////////////////

 ClosedPolylines::ClosedPolylines()
 {
     Ultrahaptics::Vector3 p1(1.0f, 1.0f, 20.0f);
     Ultrahaptics::Vector3 p2(1.0f, -1.0f, 20.0f);
     Ultrahaptics::Vector3 p3(-1.0f, -1.0f, 20.0f);
     Ultrahaptics::Vector3 p4(-1.0f, 1.0f, 20.0f);
     std::vector<Ultrahaptics::Vector3> sq({p1, p2, p3, p4});
     tag = 1;
     cycle = true;
     rot_center = Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f);
     r_x = 0.0f;
     r_y = 0.0f;
     r_z = 0.0f;
     size_t size = sq.size();
     line = std::vector<Segment>(size - 1, Segment());
     cut = std::vector<float>(size, 0.0f);
     for (size_t i = 0 ; i < size - 1 ; ++i)
     {
         line.at(i) = Segment(sq.at(i), sq.at(i + 1));
         cut.at(i + 1) = cut.at(i) + line.at(i).length();
     };

 }

ClosedPolylines::ClosedPolylines(std::vector<Ultrahaptics::Vector3> list_points)
{
    tag = 1;
    cycle = true;
    rot_center = Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f);
    r_x = 0.0f;
    r_y = 0.0f;
    r_z = 0.0f;
    size_t size = list_points.size();
    line = std::vector<Segment>(size, Segment());
    cut = std::vector<float>(size + 1, 0.0f);
    for (size_t i = 0 ; i < size - 1 ; ++i)
    {
        line.at(i) = Segment(list_points.at(i), list_points.at(i + 1));
        cut.at(i + 1) = cut.at(i) + line.at(i).length();
    };
    line.at(size - 1) = Segment(list_points.at(size - 1), list_points.at(0));
    cut.at(size) = cut.at(size - 1) + line.at(size - 1).length();
}
