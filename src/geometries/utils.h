/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include "Ultrahaptics.hpp"

float exp_rep(float x, unsigned int n);
std::vector<float> binomial(int n);
float fact(int n);
std::vector<float> cox_de_boor(std::vector<float> nodes, size_t n, float t);
Ultrahaptics::Vector3 rot_vect(Ultrahaptics::Vector3 point, float rot, Ultrahaptics::Vector3 axis);
Ultrahaptics::Vector3 rotation(Ultrahaptics::Vector3 point, Ultrahaptics::Vector3 rot, Ultrahaptics::Vector3 rot_center);
Ultrahaptics::Vector3 chgt_base(Ultrahaptics::Vector3 point, Ultrahaptics::Matrix3x3 mat);

//template<typename point_t>
std::vector<Ultrahaptics::Vector3*> fusion_sort(std::vector<Ultrahaptics::Vector3> pts, bool (*inf)(Ultrahaptics::Vector3, Ultrahaptics::Vector3));
#endif // UTILS_H

/*def deBoor(k, x, t, c, p):
    """
    Evaluates S(x).

    Args
    ----
    k: index of knot interval that contains x
    x: position
    t: array of knot positions, needs to be padded as described above
    c: array of control points
    p: degree of B-spline
    """
    d = [c[j + k - p] for j in range(0, p+1)]

    for r in range(1, p+1):
        for j in range(p, r-1, -1):
            alpha = (x - t[j+k-p]) / (t[j+1+k-r] - t[j+k-p])
            d[j] = (1.0 - alpha) * d[j-1] + alpha * d[j]

    return d[p]*/
