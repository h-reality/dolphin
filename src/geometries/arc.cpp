/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "arc.h"

#ifndef M_PI
#define M_PI 3.14159265358979323
#endif

using namespace Geometries;

Arc::Arc(double radius, double length):
        Arc(radius, length, Ultrahaptics::Vector3(0.f, 0.f, 0.f)) {}

Arc::Arc(double radius, double length, Ultrahaptics::Vector3 center):
        radius(radius), length(length) {
    tag = 1;
    cycle = false;
    rot_center = center;
}

Ultrahaptics::Vector3 Arc::evaluate_at(float phi, float psi, float teta) {
    // The length should be between 0 and the perimeter of the circle
    double l = std::max(0.f, (float)std::min(length, 2.f * M_PI * radius));

    // The angle between the tw extremities and the center
    double angle = l / radius;

    // Offset to center the arc
    double min_y = rot_center.y + radius * cos(angle / 2.f);
    double max_y = rot_center.y + radius;
    double offset = radius - (max_y - min_y) / 2.f;

    double x, y, z;
    x = rot_center.x + radius * sin(angle * (phi - 1.f / 2.f));
    y = rot_center.y - offset + radius * cos(angle * (phi - 1.f / 2.f));
    z = rot_center.z;
    Ultrahaptics::Vector3 pt(x, y, z);

    return pt;
}

double Arc::perimeter() {
    return std::max(0.f, (float)std::min(length, 2.f * M_PI * radius));
}

double Arc::getLength() {
    return length;
}

double Arc::getRadius() {
    return radius;
}
