/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "polylines.h"
#include "regularpolygon.h"
#include "polygon.h"
#include "utils.h"
#include <vector>
#include <cmath>

namespace Geometries {
class Triangle : public Polygon
{
public:
    Triangle();
    Triangle(Ultrahaptics::Vector3 point1, Ultrahaptics::Vector3 point2, Ultrahaptics::Vector3 point3);
};


class RectangleTriangle : public Triangle
{
public:
    RectangleTriangle();
    RectangleTriangle(Ultrahaptics::Vector3 right_angle, float first_side_length, float second_side_length, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
};

class IsocelesTriangle : public Triangle
{
public:
    IsocelesTriangle();
    IsocelesTriangle(Ultrahaptics::Vector3 summit, float length, float teta, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);
};

class EquilateralTriangle : public Triangle, RegularPolygon
{
public:
    EquilateralTriangle();
    EquilateralTriangle(Ultrahaptics::Vector3 center, float side_length, float rot_x = 0.0f, float rot_y = 0.0f, float rot_z = 0.0f);

};
}
#endif // TRIANGLE_H
