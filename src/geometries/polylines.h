/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef POLYLINES_H
#define POLYLINES_H

#include "geometry.h"
#include <vector>
#include <utility>

namespace Geometries {
class Segment : public Geometry
{
private:
    std::pair<Ultrahaptics::Vector3, Ultrahaptics::Vector3> segment;

public:
    Segment();
    Segment(Ultrahaptics::Vector3 first_point, Ultrahaptics::Vector3 second_point);
    float length();
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
    double perimeter() override;
};


class PolyLines : public Geometry
{
protected:
    std::vector<Segment> line;
    std::vector<float> cut;

public:
    PolyLines();
    PolyLines(std::vector<Ultrahaptics::Vector3> list_points);
    PolyLines(std::vector<Segment> list_segments);
    ~PolyLines();
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
    std::vector<Segment> get_line() {return line;}
    std::vector<float> get_cut() {return cut;}
};

class ClosedPolylines : public PolyLines
{
public:
    ClosedPolylines();
    ClosedPolylines(std::vector<Ultrahaptics::Vector3> list_points);
};
}
#endif // POLYLINES_H
