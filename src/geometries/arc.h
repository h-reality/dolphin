/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#include "geometry.h"

#ifndef ARC_H
#define ARC_H

namespace Geometries {
class Arc: public Geometry
{
private:
    double radius;
    double length;

public:
    Arc(double radius, double length);
    Arc(double radius, double length, Ultrahaptics::Vector3 center);
    Ultrahaptics::Vector3 evaluate_at(float phi, float psi = 0.0f, float teta = 0.0f) override;
    double perimeter() override;
    double getRadius();
    double getLength();
};
}

#endif // ARC_H
