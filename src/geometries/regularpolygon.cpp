/**
    04/07/2021 | Authors :
    - Quentin Zanini, ENS Rennes - quentin.zanini@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
**/

#include "regularpolygon.h"

using namespace Geometries;
RegularPolygon::RegularPolygon()
    : RegularPolygon::RegularPolygon(Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f), 3, 1.0f, 0.0f) {}

RegularPolygon::RegularPolygon(Ultrahaptics::Vector3 center, unsigned int side_number)
    : RegularPolygon::RegularPolygon(center, side_number, 1.0f, 0.0f) {}

RegularPolygon::RegularPolygon(Ultrahaptics::Vector3 center, unsigned int side_number, float side_length, float rot_x, float rot_y, float rot_z)
{
    rot_center = center;
    r_x = rot_x * M_PI / 180.0f;
    r_y = rot_y * M_PI / 180.0f;
    r_z = rot_z * M_PI / 180.0f;
    this->side_length = side_length;
    this->side_number = side_number;


    Ultrahaptics::Vector3 x(1, 0, 0);
    Ultrahaptics::Vector3 y(0, 1, 0);
    Ultrahaptics::Vector3 z(0, 0, 1);
    x = rotate(x + rot_center) - rot_center;
    y = rotate(y + rot_center) - rot_center;
    z = rotate(z + rot_center) - rot_center;

    mat = Ultrahaptics::Matrix3x3(x.x, y.x, z.x, x.y, y.y, z.y, x.z, y.z, z.z);

    float teta = 2.0f * M_PI / side_number;
    float radius = side_length / (2 * std::sin(teta / 2));
    std::vector<Ultrahaptics::Vector3> list_points(side_number, Ultrahaptics::Vector3(0.0f, 0.0f, 0.0f));
    points2D = std::vector<Vector2>(side_number + 1);
    for (size_t i = 0 ; i < side_number ; ++i)
    {

        list_points.at(i) = center + radius * Ultrahaptics::Vector3(std::cos(M_PI / 2 - i * teta),
                                                                    std::sin(M_PI / 2 - i * teta),
                                                                    0.0f);
        points2D.at(i) = Vector2(list_points.at(i).x - center.x, list_points.at(i).y - center.y);
    };
    points2D.at(side_number) = points2D.at(0);
    line = std::vector<Segment>(side_number, Segment());
    cut = std::vector<float>(side_number + 1, 0.0f);
    for (size_t i = 0 ; i < side_number - 1 ; ++i)
    {
        line.at(i) = Segment(list_points.at(i), list_points.at(i + 1));
        cut.at(i + 1) = cut.at(i) + line.at(i).length();
    };
    line.at(side_number - 1) = Segment(list_points.at(side_number - 1), list_points.at(0));
    cut.at(side_number) = cut.at(side_number - 1) + line.at(side_number - 1).length();
}

float RegularPolygon::get_side_length() {
    return side_length;
}

double RegularPolygon::perimeter() {
    return side_length * side_number;
}
