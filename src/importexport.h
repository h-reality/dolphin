/**
    04/07/2021 | Authors :
    - Lendy Mulot, ENS Rennes - lendy.mulot@ens-rennes.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    This file is part of DOLPHIN.
    DOLPHIN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    DOLPHIN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with DOLPHIN.  If not, see <https://www.gnu.org/licenses/>.
    This script is attached to a SteamVR controller and handles the commands for saving action.
**/

#ifndef IMPORTEXPORT_H
#define IMPORTEXPORT_H

#include <tuple>

#include "sampling_strategies/importallsampling.h"
#include "geometries/ellipse.h"
#include "geometries/rectangles.h"
#include "geometries/regularpolygon.h"
#include "geometries/pointlist.h"
#include "geometries/arc.h"

class ImportExport
{
public:
    static std::string exportStrategy(SamplingStrategy* strategy, std::string strategyName, std::string path);
    static std::pair<std::string, SamplingStrategy*> importStrategy(std::string path);

    static std::string exportGeometry(Geometries::Geometry* g, std::string geometryName, std::string path);
    static std::pair<std::string, Geometries::Geometry*> importGeometry(std::string path);

    static void exportGeometryAndStrategy(SamplingStrategy* strategy, std::string strategyName,
                                                 Geometries::Geometry* g, std::string geometryName,
                                                 std::string path, int sampleRate);
    static std::tuple<Geometries::Geometry*, std::string, SamplingStrategy*, std::string, int> importGeometryAndStrategy(std::string path);
private:
    ImportExport(); // all methods are static
    static std::string computeExportStrategy(StepSampling* strategy);
    static std::string computeExportStrategy(CustomSampling* strategy);
    static std::string computeExportStrategy(RandomSampling* strategy);
    static std::string computeExportStrategy(CustomTimeStepSampling* strategy);

    static std::string computeExportGeometry(Geometries::Circle* g);
    static std::string computeExportGeometry(Geometries::Ellipse* g);
    static std::string computeExportGeometry(Geometries::Square* g);
    static std::string computeExportGeometry(Geometries::Rectangle* g);
    static std::string computeExportGeometry(Geometries::RegularPolygon* g);
    static std::string computeExportGeometry(Geometries::Segment* g);
    static std::string computeExportGeometry(Geometries::PointList* g);
    static std::string computeExportGeometry(Geometries::Arc* g);

    static std::string pointList2String(SamplingStrategy* strategy, Geometries::Geometry* geometry, int sampleRate);
};

#endif // IMPORTEXPORT_H
